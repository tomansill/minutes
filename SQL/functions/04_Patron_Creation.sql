/** Creates a patron in the system with password and hash
 *  @param email_address - email address of patron - required
 *  @param in_password - password - required
 *  @param title - title of patron - required
 *  @param first_name - first name of patron - required
 *  @param middle_name - middle name of patron - optional (NULL)
 *  @param last_name - last name of patron - required
 *  @param nick_name - nickname of patron - optional (NULL)
 *  @param suffix - suffix of patron - optional (NULL)
 *  @param birth_date - patron's birthday - required
 *  @return true if successful, otherwise false if not successful
 */
CREATE OR REPLACE FUNCTION create_patron(
    email_address VARCHAR(128),
    in_password VARCHAR(128),
    title VARCHAR(10),
    first_name VARCHAR(150),
    middle_name VARCHAR(150),
    last_name VARCHAR(150),
    nick_name VARCHAR(100),
    suffix VARCHAR(10),
    birth_date DATE
) RETURNS BOOLEAN AS $$
BEGIN

    /* Check if email is not null */
    IF email_address IS NULL THEN RAISE EXCEPTION '{"code":"ERR_INVALID_EMAIL_ADDRESS", "message":"email_address is NULL!"}'; END IF;

    /* Check if email is valid */
    IF NOT is_email_address_valid(RTRIM(email_address)) THEN RAISE EXCEPTION '{"code":"ERR_INVALID_EMAIL_ADDRESS", "message":"email_address is invalid!"}'; END IF;

    /* Check if password is not null */
    IF in_password IS NULL THEN RAISE EXCEPTION '{"code":"ERR_INVALID_PASSWORD", "message":"password is NULL!"}'; END IF;

    /* Check if password is long enough */
    IF NOT is_password_valid(RTRIM(in_password)) THEN RAISE EXCEPTION '{"code":"ERR_PASSWORD_NOT_ACCEPTED", "message":"Password is too short!"}'; END IF;

    /* Check if title is not null */
    IF title IS NULL THEN RAISE EXCEPTION '{"code":"ERR_INVALID_TITLE", "message":"title is NULL!"}'; END IF;

    /* Check if title is not null */
    IF NOT is_title_valid(title) THEN RAISE EXCEPTION '{"code":"ERR_INVALID_TITLE", "message":"title is invalid!"}'; END IF;

    /* Check if first_name is not null */
    IF first_name IS NULL THEN RAISE EXCEPTION '{"code":"ERR_INVALID_FIRST_NAME", "message":"first_name is NULL!"}'; END IF;

    /* Check if first_name is valid */
    IF NOT is_name_valid(first_name) THEN RAISE EXCEPTION '{"code":"ERR_INVALID_FIRST_NAME", "message":"first_name is invalid!"}'; END IF;

    /* Check if last_name is not null */
    IF last_name IS NULL THEN RAISE EXCEPTION '{"code":"ERR_INVALID_LAST_NAME", "message":"last_name is NULL!"}'; END IF;

    /* Check if last_name is valid */
    IF NOT is_name_valid(last_name) THEN RAISE EXCEPTION '{"code":"ERR_INVALID_LAST_NAME", "message":"last_name is invalid!"}'; END IF;

    /* Check if birth_date is not null */
    IF birth_date IS NULL THEN RAISE EXCEPTION '{"code":"ERR_INVALID_BIRTH_DATE", "message":"birth_date is NULL!"}'; END IF;

    /* Check if birth date is reasonable */
    IF NOT is_birth_date_reasonable(birth_date) THEN RAISE EXCEPTION '{"code":"ERR_BIRTH_TOO_YOUNG", "message":"User must be at least 18 years old to register!"}'; END IF;

    /* Check if suffix is not null */
    IF suffix IS NOT NULL AND NOT is_suffix_valid(suffix) THEN RAISE EXCEPTION '{"code":"ERR_INVALID_SUFFIX", "message":"suffix is invalid!"}'; END IF;

    /* If nick_name is empty, set it to NULL */
    IF nick_name IS NOT NULL THEN

        /* Check if nick_name is valid */
        IF NOT is_name_valid_null(nick_name) THEN RAISE EXCEPTION '{"code":"ERR_INVALID_NICK_NAME", "message":"nick_name is invalid!"}'; END IF;

        /** If nickname is empty, set it to null */
        IF RTRIM(nick_name) LIKE '' THEN nick_name := NULL; END IF;

    END IF;

    /* If middle_name is empty, set it to NULL */
    IF middle_name IS NOT NULL THEN

        /* Check if middle_name is valid */
        IF NOT is_name_valid_null(middle_name) THEN RAISE EXCEPTION '{"code":"ERR_INVALID_MIDDLE_NAME", "message":"middle_name is invalid!"}'; END IF;

        /** If nickname is empty, set it to null */
        IF RTRIM(middle_name) LIKE '' THEN middle_name := NULL; END IF;

    END IF;

    IF LENGTH(CONCAT(RTRIM(first_name), CONCAT(RTRIM(middle_name), RTRIM(last_name)))) < 6 THEN
        RAISE EXCEPTION '{"code":"ERR_INVALID_NAME", "message":"overall name is invalid!"}';
    END IF;

    /* Check if registration is allowed */
    IF NOT access_control_check_access('register') THEN
        RETURN FALSE;
    END IF;

    DECLARE

        /* Generate a salt */
        salty VARCHAR(16) := encode(gen_random_bytes(8), 'hex');

        /* id of freshly created patron */
        curr_id INT;

    BEGIN
        /* Check if patron does not exist */
        IF NOT patron_exists(email_address::EMAIL_ADDRESS_TYPE) THEN

            /* OK, insert now */
            INSERT INTO Patron (password, hash, salt) VALUES (password_hash(in_password, salty), '.', salty) RETURNING Patron.id INTO curr_id;

            /* Update the patron's hash */
            UPDATE Patron SET hash = SHA256(CONCAT(SHA256(CAST(curr_id AS TEXT)), CONCAT(curr_id,salty))) WHERE Patron.id = curr_id;

            /* Insert into Identification - mark entry_author value as the patron him/herself */
            INSERT INTO Identification (patron_id, title, first_name, middle_name, last_name, nick_name, suffix, birth_date, entry_author)
                VALUES (curr_id, title::TITLE, RTRIM(first_name), RTRIM(middle_name), RTRIM(last_name), RTRIM(nick_name), suffix::SUFFIX, birth_date, curr_id);

            /* Insert email into Email Address */
            INSERT INTO Email_Address (patron_id, address, status, entry_author, login) VALUES (curr_id, email_address::EMAIL_ADDRESS_TYPE, 'EITHER', curr_id, TRUE);

            /* Return the query containing new patron's id and hash */
            RETURN TRUE;
        ELSE
            RAISE EXCEPTION '{"code":"ERR_DUPLICATE_ACCOUNT", "message":"Duplicate email_address : The email_address ''%'' already exists!"}', email_address;
        END IF;
    END;
END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;
