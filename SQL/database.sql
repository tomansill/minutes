/** Patron Hash type */
DROP DOMAIN IF EXISTS PATRON_HASH_TYPE;
CREATE DOMAIN PATRON_HASH_TYPE VARCHAR(64) CONSTRAINT PATRON_HASH NOT NULL CHECK( VALUE ~ '^((([a-f]|[A-F]|[0-9]){64})|.)$');

/**	Patron table - holds all users' essential information ("User" is reserved in Postgresql so we use "Patron" instead)
 *	id - sequence number
 *	password - password - required to use with password_hash function with salt - removed
 *	hash - identification hash
 *	salt - salt consisting of random number sequence
 *	password_date - date of last password change
 *	status - Enum of user's status
 *	date_joined - date of account creation
 */
CREATE TABLE Patron(
	id SERIAL NOT NULL,
	password VARCHAR(128) NOT NULL,
	hash PATRON_HASH_TYPE NOT NULL,
	salt VARCHAR(16) NOT NULL DEFAULT encode(gen_random_bytes(8), 'hex'),
	password_date TIMESTAMP DEFAULT NOW(),
	active BOOLEAN NOT NULL DEFAULT TRUE,
	date_joined DATE DEFAULT NOW(),
	totp_secret VARCHAR(64) DEFAULT NULL,
	PRIMARY KEY(id)
);

/** Session Hash type */
DROP DOMAIN IF EXISTS SESSION_TYPE;
CREATE DOMAIN SESSION_TYPE VARCHAR(128) CONSTRAINT SESSION_HASH CHECK( VALUE ~ '^([a-f]|[A-F]|[0-9]){128}$');

/** Session table - holds users' session information
 *	id - sequence number
 *	patron_id - user id associated with the session
 *	hash - hash of the session that will be sent to the user
 *	date_created - date of creation
 *	last_updated - date of last activity
 */
CREATE TABLE Session(
	id SERIAL NOT NULL,
	patron_id INT NOT NULL,
	hash SESSION_TYPE NOT NULL,
	date_created TIMESTAMP DEFAULT NOW(),
	last_updated TIMESTAMP DEFAULT NOW(),
	require_2fa BOOLEAN DEFAULT FALSE,
	PRIMARY KEY(id, hash),
	FOREIGN KEY(patron_id) REFERENCES Patron(id) ON DELETE CASCADE
);


CREATE TABLE Two_Factor_Authenication_Confirmation(
	patron_id INT NOT NULL,
	secret VARCHAR(64) NOT NULL,
	date_created TIMESTAMP DEFAULT NOW(),
	FOREIGN KEY(patron_id) REFERENCES Patron(id) ON DELETE CASCADE
);

/** Gender Enum */
DROP TYPE IF EXISTS GENDER;
CREATE TYPE GENDER AS ENUM('MALE', 'FEMALE', 'N/A');

/** Title Enum */
DROP TYPE IF EXISTS TITLE;
CREATE TYPE TITLE AS ENUM('Mr.','Dr.', 'Ms.', 'Mrs.');

/** Suffix Enum */
DROP TYPE IF EXISTS SUFFIX;
CREATE TYPE SUFFIX AS ENUM('', 'Jr.','Sr.', 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII', 'XIII');

/** Identification table - holds users' identification
 *  patron_id - user's id
 *  title - user's title
 *  first_name - user's first name
 *  middle_name - user's middle name (not required)
 *  last_name - user's last name
 *  nick_name - user's nick name (not fraternity nick name) (not required)
 *  suffix - user's suffix (not required)
 *  gender - user's gender
 *  birth_date - user's birth date
 *  deceased - user's deceased status (true if deceased)
 *  death_date - user's death date (not required)
 *	private_name - true to hide full name (ex. middle name) from public. name will be only initials
 *	private_middle_name - true to hide middle_name from public
 *	private_birth_date - true to hide birth date
 *  entry_start_time - date on this information update (inclusive)
 *  entry_end_time - date on this information update expiration (exclusive)
 *	entry_author - The id of patron who authored this entry
 */
CREATE TABLE Identification(
	patron_id INT NOT NULL,
	title TITLE NOT NULL,
	first_name VARCHAR(150) NOT NULL,
	middle_name VARCHAR(150),
	last_name VARCHAR(150) NOT NULL,
	nick_name VARCHAR(100),
	suffix SUFFIX,
    /* gender GENDER NOT NULL,*/
	birth_date DATE,
	deceased BOOLEAN NOT NULL DEFAULT FALSE,
	death_date DATE DEFAULT NULL,
	private_name BOOLEAN NOT NULL DEFAULT FALSE, /* opt out */
	private_middle_name BOOLEAN NOT NULL DEFAULT TRUE, /* opt in */
	private_birth_date BOOLEAN NOT NULL DEFAULT TRUE, /* opt in */
	entry_start_time TIMESTAMP NOT NULL DEFAULT NOW(),
	entry_end_time TIMESTAMP DEFAULT NULL,
	entry_author INT NOT NULL,
	FOREIGN KEY(patron_id) REFERENCES Patron(id) ON DELETE CASCADE,
	FOREIGN KEY(entry_author) REFERENCES Patron(id) ON DELETE CASCADE
);


/** Role Type Enum */
DROP TYPE IF EXISTS ROLE_TYPE;
CREATE TYPE ROLE_TYPE AS ENUM('no_login','login','external_advisor','candidate','member', 'alumni', 'officer','e_board', 'admin');

/** PII Access Enum
 *	NEVER - never permit
 *	OPT - optional, allows user to decide
 *	ALWAYS - always permit
 */
DROP TYPE IF EXISTS PII_ACCESS_ENUM;
CREATE TYPE PII_ACCESS_ENUM AS ENUM('NEVER', 'OPT', 'ALWAYS');

/** Default PII Access Matrix
 *	accessor - role of person who is attempting to access
 *	target - role of person who is being accessed
 *	method - method name of PII asset
 *	access - signal to allow, deny, or let decide using PII_ACCESS_ENUM enum
 */
CREATE TABLE Default_PII_Access_Matrix(
	accessor ROLE_TYPE NOT NULL,
	target ROLE_TYPE NOT NULL,
	method VARCHAR(100) NOT NULL,
	access PII_ACCESS_ENUM NOT NULL,
	PRIMARY KEY(accessor, target, method)
);

/**	Executive Board Position - Record of all executive board positions
 *	id - id of position record
 *	title - title of position
 *	patron_id - id of patron who assumed the positon
 *  time_stamp - date of effective assumption of position
 *	successor - id of succeeding patron, null if this record is current
 */
CREATE TABLE Executive_Board_Position(
	id SERIAL NOT NULL,
	title VARCHAR(60) NOT NULL,
	patron_id INT,
	time_stamp TIMESTAMP NOT NULL DEFAULT NOW(),
	successor INT,
	PRIMARY KEY(id),
	FOREIGN KEY(patron_id) REFERENCES Patron(id) ON DELETE CASCADE,
	FOREIGN KEY(successor) REFERENCES Executive_Board_Position(id)
);

/** Role Type Enum */
DROP TYPE IF EXISTS AE_ROLE_TYPE;
CREATE TYPE AE_ROLE_TYPE AS ENUM('patron','position','eb_members','eb_voting_power','members','non_members','everyone','no_login');

/** Access Matrix - Matrix that white or black lists accesses
 *	id - id of this rule
 *	active - activation of this rule - true if active otherwise false
 *	black_list - true for black, false for white
 *	type - type of role to blanket this rule over - this dicates what name value contains.
 *	name - name to describe entity for rule to be applied on
 *			if type is set to patron, name is expected to be patron's id
 *			if type is set to position, name is expected to be a title that exists in Executive_Board_Position table
 *			if type is set to eb_members, name can be null
 *			if type is set to eb_voting_power, name can be null
 *			if type is set to members, name can be null
 *			if type is set to non_members, name can be null
 *			if type is set to everyone, name can be null
 *			if type is set to no_login, name can be null
 *	method - describes what rule prohibits or allows
 *	time_stamp - stamp of this rule
 */
CREATE TABLE Access_Matrix(
	id SERIAL NOT NULL,
	active BOOLEAN NOT NULL,
	black_list BOOLEAN NOT NULL,
	type AE_ROLE_TYPE NOT NULL,
	name VARCHAR(128),
	method VARCHAR(200) NOT NULL,
	entry_start_time TIMESTAMP NOT NULL DEFAULT NOW(),
	entry_end_time TIMESTAMP DEFAULT NULL,
	entry_author INT NOT NULL,
	FOREIGN KEY(entry_author) REFERENCES Patron(id)
);

/** Email datatype */
DROP DOMAIN IF EXISTS EMAIL_ADDRESS_TYPE;
CREATE DOMAIN EMAIL_ADDRESS_TYPE VARCHAR(100) CONSTRAINT EMAIL_ADDRESS NOT NULL CHECK( VALUE ~ '(^\S+@\S+\.\S+$)');

/** Email Status Enum
 *	EITHER - Either Personal or Work
 *  PERSONAL - Personal
 *  WORK - Work
 *  DELETED - Indicate that information is expired
 */
DROP TYPE IF EXISTS EMAIL_ADDRESS_STATUS;
CREATE TYPE EMAIL_ADDRESS_STATUS AS ENUM('EITHER', 'PERSONAL', 'WORK', 'DELETED');

/** Email Address hash type */ /* another hash, yay!! */
DROP DOMAIN IF EXISTS EMAIL_ADDRESS_HASH_TYPE;
CREATE DOMAIN EMAIL_ADDRESS_HASH_TYPE VARCHAR(64) CONSTRAINT EMAIL_ADDRESS_HASH NOT NULL CHECK( VALUE ~ '^((([a-f]|[A-F]|[0-9]){64})|.)$');

/** Email Address Table - Holds all users' email addresses
 *  id - id of this email
 *  patron_id - user's id
 *	login - true if this email is used for login
 *  address - Email Address
 *  status - status of Email Address
 *	private - true to keep this email address private
 *  time_stamp - date on this information update
 *  successor - id of next updated information, NULL if this information is up to date
 *	entry_author - The id of patron who authored this entry
 */
CREATE TABLE Email_Address(
    id SERIAL NOT NULL,
	patron_id INT NOT NULL,
	hash EMAIL_ADDRESS_HASH_TYPE NOT NULL DEFAULT sha256(CONCAT(CONCAT(encode(gen_random_bytes(8), 'hex'), (currval(pg_get_serial_sequence('email_address', 'id')))::text),NOW())),
	login BOOLEAN NOT NULL DEFAULT FALSE,
	address EMAIL_ADDRESS_TYPE NOT NULL,
    status EMAIL_ADDRESS_STATUS NOT NULL DEFAULT 'EITHER',
	private BOOLEAN NOT NULL DEFAULT TRUE,
	time_stamp TIMESTAMP NOT NULL DEFAULT NOW(),
    successor INT DEFAULT NULL,
	entry_author INT NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(patron_id) REFERENCES Patron(id) ON DELETE CASCADE,
    FOREIGN KEY(successor) REFERENCES Email_Address(id) ON DELETE CASCADE,
	FOREIGN KEY(entry_author) REFERENCES Patron(id)
);

/** Initiation Table - Connects users to fraternity membership
 *  patron_id - user's id
 *  id - initiation id
 */
CREATE TABLE Initiation(
    patron_id INT NOT NULL,
    id INT NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(patron_id) REFERENCES Patron(id) ON DELETE CASCADE
);

/** Membership status */
 DROP TYPE IF EXISTS MEMBERSHIP_STATUS;
 CREATE TYPE MEMBERSHIP_STATUS AS ENUM('not_member','candidate','alumni','active','left_school','eternal','expelled','internship','suspended','study_abroad');

/** Membership table - Holds all members' membership history
 *  patron_id - user's id
 *  status - status of member's membership
 *  time_stamp - date on this information update
 */
CREATE TABLE Membership(
 	initiation_id INT NOT NULL,
 	status MEMBERSHIP_STATUS NOT NULL,
 	time_stamp TIMESTAMP NOT NULL DEFAULT NOW(),
 	FOREIGN KEY(initiation_id) REFERENCES Initiation(id) ON DELETE CASCADE
);

/** Fraternity Nickname - Table containing members' nickname
 *  initiation_id - member's initiation id
 *  nickname - nickname
 *  date - date of given nickname
 *  time_stamp - date on this information update
 */
CREATE TABLE Fraternity_Nickname(
    initiation_id INT NOT NULL,
	nickname VARCHAR(100),
	date DATE NOT NULL,
	time_stamp TIMESTAMP NOT NULL DEFAULT NOW(),
	FOREIGN KEY(initiation_id) REFERENCES Initiation(id) ON DELETE CASCADE
);

/** Document Type Enum */
DROP TYPE IF EXISTS DOCUMENT_TYPE;
CREATE TYPE DOCUMENT_TYPE AS ENUM('URL','local');

/** Document Status Enum */
DROP TYPE IF EXISTS DOCUMENT_STATUS;
CREATE TYPE DOCUMENT_STATUS AS ENUM('active','hidden','inactive');

/** Document Table - information on documents
 *  id - id of document
 *  name - name of document
 *  type - type of document
 *  status - status of document
 *  location - location of document on server if it's a local, URL if not local
 *  hash - hash of document based on name and id
 *  time_stamp - date on this information update
 *	entry_author - The id of patron who authored this entry
 */
CREATE TABLE Document(
	id SERIAL NOT NULL,
	name VARCHAR(100) NOT NULL,
	type DOCUMENT_TYPE NOT NULL,
    status DOCUMENT_STATUS NOT NULL DEFAULT 'active',
	location VARCHAR(300) NOT NULL,
	hash VARCHAR(128),
	time_stamp TIMESTAMP NOT NULL DEFAULT NOW(),
	entry_author INT NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(entry_author) REFERENCES Patron(id) ON DELETE CASCADE
);

/** Read Activity Table - information on read activity on each document
 *  patron_id - user's id
 *  document_id - id of document
 *  read_date - date of read
 */
CREATE TABLE Read_Activity(
	patron_id INT NOT NULL,
	document_id INT NOT NULL,
	read_date TIMESTAMP DEFAULT NOW(),
	FOREIGN KEY(patron_id) REFERENCES Patron(id), ON DELETE CASCADE
	FOREIGN KEY(document_id) REFERENCES Document(id) ON DELETE CASCADE
);

/** Subscription Type Enum */
DROP TYPE IF EXISTS SUBSCRIPTION_TYPE;
CREATE TYPE SUBSCRIPTION_TYPE AS ENUM('public','private','inactive');

/** Subscription Table - subscription information
 *  id - id of subscription
 *  name - name of subscription
 *  description - description of subscription
 *  status - status of subscription
 */
CREATE TABLE Subscription(
    id SERIAL NOT NULL,
    name VARCHAR(128),
    description text NOT NULL DEFAULT '',
    status SUBSCRIPTION_TYPE NOT NULL,
    PRIMARY KEY(id)
);

/** Subscription List Table - Mailing List
 *  patron_id - id of user
 *  subscription_id - id of subscription
 */
CREATE TABLE Subscription_List(
    patron_id INT NOT NULL,
    subscription_id INT NOT NULL,
    FOREIGN KEY(patron_id) REFERENCES Patron(id) ON DELETE CASCADE,
    FOREIGN KEY(subscription_id) REFERENCES Subscription(id) ON DELETE CASCADE
);

/** Email Message Table - Messages
 *  id - id of message
 *  composed_by - id of composer
 *  subject - subject on email
 *  message - message on email
 *  created_date - date of creation
 *	entry_author - The id of patron who authored this entry
 */
CREATE TABLE Email_Message(
    id SERIAL NOT NULL,
    composed_by INT NOT NULL,
    subject VARCHAR(256) NOT NULL,
    message TEXT NOT NULL,
    created_date TIMESTAMP NOT NULL DEFAULT NOW(),
	entry_author INT NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(composed_by) REFERENCES Patron(id) ON DELETE CASCADE,
	FOREIGN KEY(entry_author) REFERENCES Patron(id) ON DELETE CASCADE
);

/** Subscription to Email Message Table - Many-to-one relationship between two tables
 *  subscription_id - id of subscription
 *  email_message_id - id of email message
 */
CREATE TABLE Subscription_To_Email_Message(
    subscription_id INT NOT NULL,
    email_message_id INT NOT NULL,
    FOREIGN KEY(subscription_id) REFERENCES Subscription(id) ON DELETE CASCADE,
    FOREIGN KEY(email_message_id) REFERENCES Email_Message(id) ON DELETE CASCADE
);

/** Document to Email Message Table - Many-to-one relationship between two tables
 *  document_id - id of document
 *  email_message_id - id of email message
 */
CREATE TABLE Document_To_Email_Message(
    document_id INT NOT NULL,
    email_message_id INT NOT NULL,
    FOREIGN KEY(document_id) REFERENCES Document(id) ON DELETE CASCADE,
    FOREIGN KEY(email_message_id) REFERENCES Email_Message(id) ON DELETE CASCADE
);

/** Email Receipt - Receipts of Email
 *  patron_id - id of user
 *  document_id - id of document
 *  email_address_id - id of email address used to send
 *  status_code - status code of email (200 - OK, and etc)
 *  time_stamp - date on this receipt
 */
CREATE TABLE Email_Receipt(
    patron_id INT NOT NULL,
    email_message_id INT NOT NULL,
    email_address_id INT NOT NULL,
    status_code INT NOT NULL,
    time_stamp TIMESTAMP NOT NULL DEFAULT NOW(),
    FOREIGN KEY(patron_id) REFERENCES Patron(id) ON DELETE CASCADE,
    FOREIGN KEY(email_message_id) REFERENCES Email_Message(id) ON DELETE CASCADE,
    FOREIGN KEY(email_address_id) REFERENCES Email_Address(id) ON DELETE CASCADE
);
