package tomansill.minutes;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.nio.file.Path;
import java.io.File;

import javax.servlet.http.Part;

import java.util.Map;
import java.util.Map.Entry;

import java.sql.SQLException;

import java.util.logging.Level;

/** Class that handles connections and routes the requests
 *  @author Thomas Ansill
 */
public class API extends HttpServlet{

    /** Server config */
    private Config server_config;
    private Database db;

    /** Constuctor */
    public API(Config server_config) throws InstantiationException{
        this.server_config = server_config;
        this.db = Database.getInstance();
    }

    /** Function that services the HTTP request
     *  @param request HTTP request
     *  @param response HTTP response to respond to the user
     */
    public void service(HttpServletRequest request, HttpServletResponse response){

        // Prep the default response
        response.setContentType("text/text; charset=utf-8");
        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

        // Get the URI - use Path to access individual directory names separated by delimiter in the URI
        Path path1 = new File(request.getRequestURI().toLowerCase()).toPath();
        PathList path = new PathList(request.getRequestURI());

        // Pop the 'api' in the path
        path.tryPop();

        int access_log_id = -1;
        try{

            // Log the request
            access_log_id = LoggerUtil.logAccess(request);

            // Check session - delete cookie it if it's expired
            String session_hash = this.db.session.getSessionHashFromCookies(request.getCookies());
            if(session_hash != null && !this.db.session.isSessionAlive(session_hash)){
                response.addCookie(this.db.session.destroyCookie());
            }

            // "Switch case" for Strings
            if(path.popOnFirstEquals("user")){
                new UserAPI(server_config).processRequest(path, request, response);
            }else if(path.popOnFirstEquals("cause_error")){
                throw new Exception("Fucking some shit up!");
            }

            // Add message to the error message
            if(response.getStatus() == HttpServletResponse.SC_BAD_REQUEST){
                response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Unknown API endpoint");
            }

            // Update the access log
            LoggerUtil.updateAccessLog(access_log_id, response.getStatus());

        }catch(Exception exception){
            // Catching all catastrophic failures and log it
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

            // Update the log
            LoggerUtil.updateAccessLog(access_log_id, response.getStatus());

            // Log the failure
            LoggerUtil.logError(Level.SEVERE, exception, access_log_id);
        }
    }
}
