package tomansill.minutes;

import org.eclipse.jetty.util.log.AbstractLogger;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.eclipse.jetty.util.log.Logger;

public class MinutesLogger extends AbstractLogger{

    private boolean debug = false;
    private int level;
    private String name;

    public MinutesLogger(){
        super();
        level = AbstractLogger.LEVEL_WARN;
    }

    public MinutesLogger(String name){
        super();
        level = AbstractLogger.LEVEL_WARN;
        this.name = name;
    }

    private void format(StringBuffer sb, String msg, Object... args){
        JSONObject ret = new JSONObject();
        ret.put("message", msg);
        JSONArray arr = new JSONArray();
        for(Object obj : args){
            arr.add(obj.toString());
        }
        ret.put("list", arr);
        try{
            sb.append(Utility.encodeJSON(ret));
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    public void debug(String msg, long value){
        if(debug){
            StringBuffer sb = new StringBuffer();
            sb.append("Debug value: ");
            sb.append(value);
            LoggerUtil.logError("DEBUG", "Debug value: ", value + "");
        }
    }

    public void debug(String msg, Object... args){
        if(debug){
            StringBuffer sb = new StringBuffer();
            format(sb, msg, args);
            LoggerUtil.logError("DEBUG", "debug(Obj... args)", sb.toString());
        }
    }

    public void debug(String msg, Throwable thrown){
        if(debug){
            LoggerUtil.logError("DEBUG", msg, thrown);
        }
    }

    public void debug(Throwable thrown){
        if(debug){
            LoggerUtil.logError("DEBUG", thrown);
        }
    }

    public void info(String msg, Object... args){
        if(level <= AbstractLogger.LEVEL_INFO){
            StringBuffer sb = new StringBuffer();
            format(sb, msg, args);
            LoggerUtil.logError("INFO", "info(Obj... args)", sb.toString());
        }
    }

    public void info(String msg, Throwable thrown){
        if(level <= AbstractLogger.LEVEL_INFO){
            LoggerUtil.logError("DEBUG", msg, thrown);
        }
    }

    public void info(Throwable thrown){
        if(level <= AbstractLogger.LEVEL_INFO){
            LoggerUtil.logError("DEBUG", thrown);
        }
    }

    public void warn(String msg, Object... args){
        if(level <= AbstractLogger.LEVEL_WARN){
            StringBuffer sb = new StringBuffer();
            format(sb, msg, args);
            LoggerUtil.logError("INFO", "warn(Obj... args)", sb.toString());
        }
    }

    public void warn(String msg, Throwable thrown){
        if(level <= AbstractLogger.LEVEL_WARN){
            LoggerUtil.logError("DEBUG", msg, thrown);
        }
    }

    public void warn(Throwable thrown){
        if(level <= AbstractLogger.LEVEL_WARN){
            LoggerUtil.logError("DEBUG", thrown);
        }
    }

    public String getName(){
        return name;
    }

    public boolean isDebugEnabled(){
        return debug;
    }

    public void setDebugEnabled(boolean debug){
        // do nothing
    }

    public void ignore(Throwable ignored){
        // do nothing
    }

    public Logger newLogger(String name){
        MinutesLogger log = new MinutesLogger(name);
        return log;
    }

}
