package tomansill.minutes;

import junit.framework.Assert;
import org.junit.Test;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

/**
* Unit test for simple App.
*/
public class TestUtility{

    /**
    * Rigourous Test :-)
    */
    @Test
    public void jsonTest(){
        try{
            JSONArray ja = new JSONArray();
            ja.add("ab");
            ja.add("cd");
            JSONObject jo = new JSONObject();
            jo.put("int", 123);
            jo.put("string", "abc");
            jo.put("bool", true);
            jo.put("obj", ja);
            String str1 = Utility.encodeJSON(ja);
            Assert.assertTrue(str1.equals("[\"ab\",\"cd\"]"));

            String str2 = Utility.encodeJSON(jo);
            Assert.assertTrue(str2.equals("{\"string\":\"abc\",\"bool\":true,\"obj\":[\"ab\",\"cd\"],\"int\":123}"));

            JSONObject jo1 = Utility.decodeJSON(str2);

            //Assert.assertTrue((int)jo1.get("int") == (int)jo.get("int"));

        }catch(Exception e){
            System.err.println(e.getMessage());
            Assert.assertTrue(false);
        }

    }

    @Test
    public void isNameValidTest(){
        Assert.assertTrue(Utility.isNameValid("Harold"));
        Assert.assertTrue(Utility.isNameValid("Sa"));
        Assert.assertTrue(Utility.isNameValid("Wolfeschlegelsteinhausenbergerdorff"));
        Assert.assertTrue(Utility.isNameValid(" Alan "));
        Assert.assertTrue(Utility.isNameValid("Sinbad Omar"));
        Assert.assertFalse(Utility.isNameValid("robot88"));
        Assert.assertFalse(Utility.isNameValid(""));
        Assert.assertFalse(Utility.isNameValid(null));
    }
}
