package tomansill.minutes;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;

/** Class that overrides HttpServletResponse's sendError method to output cleaner error message without HTML markup
 *  @author Thomas Ansill
 */
public class CleanHttpServletResponse extends HttpServletResponseWrapper{

    /** Constructor - does nothing except pass the response to parent class
    *   @param response HttpServletResponse object
    */
    public CleanHttpServletResponse(HttpServletResponse response){
        super(response);
    }

    /** sendError method - writes error message in the response - does not write with HTML markup unlike other sendError
     *  @param sc status code
     *  @param message message to be written
     *  @throws IOException thrown when there is a problem with writing to the response
     */
    @Override
    public void sendError(int sc, String message) throws IOException{
        this.setContentType("text/text; charset=utf-8");
        this.setStatus(sc);
        this.getWriter().print(message); // Default one use a full HTML markup, we don't want that. We just want a regular text
    }
}
