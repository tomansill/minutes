package tomansill.minutes;

public class Database{
    private static Database instance = null;
    public UserDB user;
    public SessionDB session;
    public GlobalDB global;
    public LoggingDB logger;

    /** Server config */
    private Config server_config;

    protected Database(Config server_config){
        this.user = new UserDB(server_config);
        this.session = new SessionDB(server_config);
        this.global = new GlobalDB(server_config);
        this.logger = new LoggingDB(server_config);
    }

    public static Database getInstance() throws InstantiationException{
        if(Database.instance == null){
            Config config = Config.getInstance();
            Database.instance = new Database(config);
        }
        return Database.instance;
    }
}
