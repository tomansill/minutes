/** Gets role of current logged-in patron
 *  @param session current login's session
 *  @param in_email_address email address of this patron
 *  @return A table describing the access privileges
 */
CREATE OR REPLACE FUNCTION get_patron_role(session SESSION_TYPE) RETURNS ROLE_TYPE AS $$
BEGIN

	/* if session is null, then no login */
	IF session IS NULL THEN RETURN 'no_login'; END IF;

	/* if session is expired or invalid, then no login */
	IF NOT is_session_alive(session, FALSE) THEN RETURN 'no_login'; END IF;

	/* Return the answer */
	RETURN get_patron_role((SELECT hash FROM identify_patron_from_session(session)));

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

/** Gets role of current logged-in patron
 *  @param session current login's session
 *  @param in_email_address email address of this patron
 *  @return A table describing the access privileges
 */
CREATE OR REPLACE FUNCTION get_patron_role(patron_hash PATRON_HASH_TYPE) RETURNS ROLE_TYPE AS $$
BEGIN

	/* if session is null, then no login */
	IF NOT patron_exists(patron_hash) THEN RAISE EXCEPTION '{"code":"ERR_USER_HASH_NOT_FOUND", "message":"User hash ''%'' does not exist!"}', patron_hash; END IF;

	/* check if admin */
	IF (SELECT id FROM get_patron_internal(patron_hash)) = 0 THEN
		RETURN 'admin';
	END IF;

	/* check executive board */

	/* check membership status */

	/* check candidate */

	/* check external_advisor */

	/* catch-all */
	RETURN 'login';

END; $$
LANGUAGE PLPGSQL STABLE SECURITY DEFINER;

CREATE OR REPLACE FUNCTION get_patron_information_write(session SESSION_TYPE, in_email_address EMAIL_ADDRESS_TYPE, method VARCHAR(100)) RETURNS BOOLEAN AS $$
BEGIN

    RETURN (SELECT get_patron_information_write(session, (SELECT hash FROM get_patron_by_email_address(in_email_address)), method));

END; $$
LANGUAGE PLPGSQL STABLE SECURITY DEFINER;

CREATE OR REPLACE FUNCTION get_patron_information_write(session SESSION_TYPE, patron_hash PATRON_HASH_TYPE, in_method VARCHAR(100)) RETURNS BOOLEAN AS $$
BEGIN

	/* Verify input */
	IF NOT patron_exists(patron_hash) THEN RAISE EXCEPTION '{"code":"ERR_USER_HASH_NOT_FOUND", "message":"User hash ''%'' does not exist!"}', patron_hash; END IF;
	IF NOT EXISTS (SELECT * FROM Default_PII_Access_Matrix WHERE method = RTRIM(method)) THEN
		RAISE EXCEPTION '{"code":"ERR_METHOD_NOT_APPROVED", "message":"in_method is not found on the approved methods on the list!"}';
	END IF;

	DECLARE
		session_role ROLE_TYPE := (SELECT get_patron_role(session));
		patron_role ROLE_TYPE := (SELECT get_patron_role(patron_hash));
		session_id INT := (SELECT id FROM identify_patron_from_session_internal(session));
		patron_id INT := (SELECT id FROM get_patron_internal(patron_hash));
	BEGIN

		/* Check if session is requesting the info on him/herself */
		IF session_id = patron_id THEN RETURN TRUE; END IF; /* of course, it's your info, you should access it (: */

		/* Check if session is an admin */
		IF session_role = 'admin' AND patron_role != 'admin' THEN RETURN TRUE; END IF;

		/* Check the other matrix */
		IF access_control_check_access(session, 'PII_write') THEN RETURN TRUE; END IF;

		/* Return false */
		RETURN FALSE;

	END;

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

/** Gets information about information access to certain patron
 *  @param session current login's session
 *  @param in_email_address email address of this patron
 *  @return A table describing the access privileges
 */
CREATE OR REPLACE FUNCTION get_patron_information_access(session SESSION_TYPE, in_email_address EMAIL_ADDRESS_TYPE, method VARCHAR(100)) RETURNS PII_ACCESS_ENUM AS $$
BEGIN

    RETURN (SELECT get_patron_information_access(session, (SELECT hash FROM get_patron_by_email_address(in_email_address)), method));

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

/** Gets information about information access to certain patron
 *  @param session current login's session
 *  @param patron_hash hash of this patron
 *  @return A table describing the access privileges
 */
CREATE OR REPLACE FUNCTION get_patron_information_access(session SESSION_TYPE, patron_hash PATRON_HASH_TYPE, in_method VARCHAR(100)) RETURNS PII_ACCESS_ENUM AS $$
BEGIN

	/* Verify input */
	IF NOT patron_exists(patron_hash) THEN RAISE EXCEPTION '{"code":"ERR_USER_HASH_NOT_FOUND", "message":"User hash ''%'' does not exist!"}', patron_hash; END IF;
	IF NOT EXISTS (SELECT * FROM Default_PII_Access_Matrix WHERE method = RTRIM(method)) THEN
		RAISE EXCEPTION '{"code":"ERR_METHOD_NOT_APPROVED", "message":"in_method is not found on the approved methods on the list!"}';
	END IF;

	DECLARE
		session_role ROLE_TYPE := (SELECT get_patron_role(session));
		patron_role ROLE_TYPE := (SELECT get_patron_role(patron_hash));
		session_id INT := (SELECT id FROM identify_patron_from_session_internal(session));
		patron_id INT := (SELECT id FROM get_patron_internal(patron_hash));
		access PII_ACCESS_ENUM := (SELECT access FROM Default_PII_Access_Matrix WHERE accessor = session_role AND target = patron_role AND method = RTRIM(in_method));
	BEGIN

		/* Check if session is requesting the info on him/herself */
		IF session_id IS NOT NULL AND session_id = patron_id THEN RETURN 'ALWAYS'; END IF; /* of course, it's your info, you should access it (: */

		/* Check if session is an admin */
		IF session_role = 'admin' THEN RETURN 'ALWAYS'; END IF;

		/* Check the other matrix */
		IF access_control_check_access(session, 'PII_*') OR /* all PII information */
		access_control_check_access(session, in_method) THEN RETURN 'ALWAYS'; END IF;

		/* Return what default matrix says */
		IF access IS NOT NULL THEN RETURN access; END IF;

		/* fail-safe */
		RETURN 'NEVER';

	END;
END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

/** Function to return name of specified patron
 *  @param session session hash of current logged-in patron
 *  @param patron_hash hash of patron to access
 *  @return null if not permitted, otherwise return name
 */
CREATE OR REPLACE FUNCTION get_patron_information_middle_name(session SESSION_TYPE, email_address EMAIL_ADDRESS_TYPE) RETURNS VARCHAR(150) AS $$
BEGIN
    RETURN (SELECT get_patron_information_middle_name(session, (SELECT hash FROM get_patron(email_address))));
END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

/** Function to return name of specified patron
 *  @param session session hash of current logged-in patron
 *  @param patron_hash hash of patron to access
 *  @return null if not permitted, otherwise return name
 */
CREATE OR REPLACE FUNCTION get_patron_information_middle_name(session SESSION_TYPE, patron_hash PATRON_HASH_TYPE) RETURNS VARCHAR(150) AS $$
BEGIN

	/* Verify input */
	IF NOT patron_exists(patron_hash) THEN RAISE EXCEPTION '{"code":"ERR_USER_HASH_NOT_FOUND", "message":"User hash ''%'' does not exist!"}', patron_hash; END IF;

	DECLARE
		access PII_ACCESS_ENUM := (SELECT get_patron_information_access(session, patron_hash, 'PII_middle_name'::VARCHAR));
		in_patron_id INT := (SELECT id FROM get_patron_internal(patron_hash));
	BEGIN

		/* If never, then return empty set */
		IF access = 'NEVER' THEN RETURN (SELECT middle_name FROM Identification WHERE 1 = 0); END IF; /*always fail - returns empty set */

		/* If always, return everything */
		IF access = 'ALWAYS' THEN RETURN (SELECT middle_name FROM Identification WHERE patron_id = in_patron_id AND entry_end_time IS NULL); END IF;

		/* return what we got */
		RETURN (SELECT middle_name FROM Identification WHERE patron_id = in_patron_id AND entry_end_time IS NULL AND NOT private_middle_name);

	END;

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

/** Function to return name of specified patron
 *  @param session session hash of current logged-in patron
 *  @param patron_hash hash of patron to access
 *  @return null if not permitted, otherwise return name
 */
CREATE OR REPLACE FUNCTION get_patron_information_name(session SESSION_TYPE, email_address EMAIL_ADDRESS_TYPE) RETURNS TABLE(
    title TITLE,
    first_name VARCHAR(150),
    middle_name VARCHAR(150),
    last_name VARCHAR(150),
    nick_name VARCHAR(100),
    suffix SUFFIX
) AS $$
BEGIN
    RETURN QUERY (SELECT * FROM get_patron_information_name(session, (SELECT hash FROM get_patron(email_address))));
END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

/** Function to return name of specified patron
 *  @param session session hash of current logged-in patron
 *  @param patron_hash hash of patron to access
 *  @return null if not permitted, otherwise return name
 */
CREATE OR REPLACE FUNCTION get_patron_information_name(session SESSION_TYPE, patron_hash PATRON_HASH_TYPE) RETURNS TABLE(
    title TITLE,
    first_name VARCHAR(150),
    middle_name VARCHAR(150),
    last_name VARCHAR(150),
    nick_name VARCHAR(100),
    suffix SUFFIX
) AS $$
BEGIN

    /* Verify input */
	IF NOT patron_exists(patron_hash) THEN RAISE EXCEPTION '{"code":"ERR_USER_HASH_NOT_FOUND", "message":"User hash ''%'' does not exist!"}', patron_hash; END IF;

    DECLARE
		access PII_ACCESS_ENUM := (SELECT get_patron_information_access(session, patron_hash, 'PII_name'::VARCHAR));
		in_patron_id INT := (SELECT id FROM get_patron_internal(patron_hash));
    BEGIN

		/* If never, then return empty set */
		IF access = 'NEVER' THEN RETURN QUERY (
				SELECT
					id.title,
					LEFT(id.first_name, 1)::VARCHAR AS first_name,
					NULL::VARCHAR AS middle_name,
					LEFT(id.last_name, 1)::VARCHAR AS last_name,
					NULL::VARCHAR AS nick_name,
					NULL::SUFFIX AS suffix
				FROM Identification id
				WHERE
					id.entry_end_time IS NULL AND
					id.patron_id = in_patron_id
			);


		/* If always, return everything */
		ELSIF access = 'ALWAYS' THEN
			RETURN QUERY (
				SELECT
					id.title,
					id.first_name,
					(SELECT get_patron_information_middle_name(session, patron_hash)) AS middle_name,
					id.last_name,
					id.nick_name,
					id.suffix
				FROM Identification id
				WHERE
					id.entry_end_time IS NULL AND
					id.patron_id = in_patron_id
			);
		ELSE

			/* Return what we have */
			RETURN QUERY (
				SELECT
					id.title,
					id.first_name,
					(SELECT get_patron_information_middle_name(session, patron_hash)) AS middle_name,
					id.last_name,
					id.nick_name,
					id.suffix
				FROM Identification id
				WHERE
					id.entry_end_time IS NULL AND
					id.patron_id = in_patron_id AND
					NOT id.private_name
			);
		END IF;
    END;

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

/** Function to return name of specified patron
 *  @param session session hash of current logged-in patron
 *  @param patron_hash hash of patron to access
 *  @return null if not permitted, otherwise return name
 */
CREATE OR REPLACE FUNCTION get_patron_information_birth_date(session SESSION_TYPE, email_address EMAIL_ADDRESS_TYPE) RETURNS DATE AS $$
BEGIN
    RETURN (SELECT get_patron_information_birth_date(session, (SELECT hash FROM get_patron(email_address))));
END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

/** Function to return name of specified patron
 *  @param session session hash of current logged-in patron
 *  @param patron_hash hash of patron to access
 *  @return null if not permitted, otherwise return name
 */
CREATE OR REPLACE FUNCTION get_patron_information_birth_date(session SESSION_TYPE, patron_hash PATRON_HASH_TYPE) RETURNS DATE AS $$
BEGIN

    /* Verify input */
	IF NOT patron_exists(patron_hash) THEN RAISE EXCEPTION '{"code":"ERR_USER_HASH_NOT_FOUND", "message":"User hash ''%'' does not exist!"}', patron_hash; END IF;

	DECLARE
		access PII_ACCESS_ENUM := (SELECT get_patron_information_access(session, patron_hash, 'PII_birth_date'::VARCHAR));
		in_patron_id INT := (SELECT id FROM get_patron_internal(patron_hash));
	BEGIN

		/* If never, then return empty set */
		IF access = 'NEVER' THEN RETURN (SELECT birth_date FROM Identification WHERE 1 = 0); END IF; /*always fail - returns empty set */

		/* If always, return everything */
		IF access = 'ALWAYS' THEN RETURN (SELECT birth_date FROM Identification WHERE patron_id = in_patron_id AND entry_end_time IS NULL); END IF;

		/* Return what we got */
		RETURN (SELECT birth_date FROM Identification WHERE patron_id = in_patron_id AND entry_end_time IS NULL AND NOT private_birth_date);

	END;

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

/** Function to return name of specified patron
 *  @param session session hash of current logged-in patron
 *  @param patron_hash hash of patron to access
 *  @return null if not permitted, otherwise return name
 */
CREATE OR REPLACE FUNCTION get_patron_information_privacy(session SESSION_TYPE, email_address EMAIL_ADDRESS_TYPE) RETURNS DATE AS $$
BEGIN
    RETURN (SELECT get_patron_information_birth_date(session, (SELECT hash FROM get_patron(email_address))));
END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;


/** Function to return name of specified patron
 *  @param session session hash of current logged-in patron
 *  @param patron_hash hash of patron to access
 *  @return null if not permitted, otherwise return name
 */
CREATE OR REPLACE FUNCTION get_patron_information_privacy(session SESSION_TYPE, patron_hash PATRON_HASH_TYPE) RETURNS TABLE (
	private_name BOOLEAN,
	private_middle_name BOOLEAN,
	private_birth_date BOOLEAN
) AS $$
BEGIN
	/* Verify input */
	IF NOT patron_exists(patron_hash) THEN RAISE EXCEPTION '{"code":"ERR_USER_HASH_NOT_FOUND", "message":"User hash ''%'' does not exist!"}', patron_hash; END IF;

DECLARE
	in_patron_id INT := (SELECT id FROM get_patron_internal(patron_hash));
BEGIN
	RETURN QUERY (SELECT id.private_name, id.private_middle_name, id.private_birth_date FROM Identification id WHERE id.patron_id = in_patron_id AND id.entry_end_time IS NULL);
END;

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;
