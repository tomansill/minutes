/** Function that returns the Global value of session time out interval */
CREATE OR REPLACE FUNCTION GLOBAL_session_timeout_interval() RETURNS INTERVAL AS $$
BEGIN
    RETURN INTERVAL '30 hours';
END; $$
LANGUAGE PLPGSQL IMMUTABLE SECURITY DEFINER;

/** Function that returns the Global value of unverified two factor authenication time out interval */
CREATE OR REPLACE FUNCTION GLOBAL_2FA_timeout_interval() RETURNS INTERVAL AS $$
BEGIN
    RETURN INTERVAL '15 minutes';
END; $$
LANGUAGE PLPGSQL IMMUTABLE SECURITY DEFINER;

/** Function that returns the Global value of element limit on access matrix */
CREATE OR REPLACE FUNCTION GLOBAL_access_matrix_element_limit() RETURNS INT AS $$
BEGIN
    RETURN 20;
END; $$
LANGUAGE PLPGSQL IMMUTABLE SECURITY DEFINER;

/** Function that returns the Global value of interval to expire never-logged-in accounts*/
CREATE OR REPLACE FUNCTION GLOBAL_fresh_account_expiration_time() RETURNS INTERVAL AS $$
BEGIN
    RETURN INTERVAL '48 hours';
END; $$
LANGUAGE PLPGSQL IMMUTABLE SECURITY DEFINER;

/** Function that returns the Global value of flag that permits or deny automatic session renewal upon password change */
CREATE OR REPLACE FUNCTION GLOBAL_password_change_renew_session() RETURNS BOOLEAN AS $$
BEGIN
    RETURN TRUE;
END; $$
LANGUAGE PLPGSQL IMMUTABLE SECURITY DEFINER;
