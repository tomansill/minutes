package tomansill.minutes;

import java.util.Map;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.net.URL;
import java.net.URLDecoder;
import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


import java.io.Reader;
import java.io.StringWriter;
import java.io.StringReader;
import java.io.IOException;

import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Date;

import java.util.regex.Pattern;

/** Utility functions
 *  Bunch of functions to make my life bit easier
 *  @author Thomas Ansill
 */
public class Utility{

    /** Prevents instantiation of this utility */
    private Utility(){}

    /** function to convert the query string to a manageable Key to Pair Map object
     *  Thanks to Pr0gr4mm3r at 'https://stackoverflow.com/questions/13592236/parse-a-uri-string-into-name-value-collection#13592567'!
     *  @param queryString query string
     *  @return map of query
     */
    public static Map<String, List<String>> splitURLQueryString(String queryString){

        // Create map
        final Map<String, List<String>> query_pairs = new LinkedHashMap<String, List<String>>();

        // Return empty map if query string is null or empty
        if(queryString == null || queryString.trim().equals("")){
            return query_pairs;
        }

        // Split the query string by '&'
        final String[] pairs = queryString.split("&");

        try{
            // Go through each pair
            for(String pair : pairs){
                // Get index of first '='
                final int index = pair.indexOf("=");

                // Get key from string before index
                final String key = index > 0 ? URLDecoder.decode(pair.substring(0, index), "UTF-8") : pair;

                // Create a key in the map
                if(!query_pairs.containsKey(key)){
                    query_pairs.put(key, new LinkedList<String>());
                }

                // Get value from string after index
                final String value = index > 0 && pair.length() > index + 1 ? URLDecoder.decode(pair.substring(index + 1), "UTF-8") : null;

                // Insert into the map
                query_pairs.get(key).add(value);
            }
        }catch(UnsupportedEncodingException exception){
            // Just return empty query
            return query_pairs;
        }

        return query_pairs;
    }

    /** Function to encode JSON array to JSON encoded string
     *  @param array JSON array
     *  @return string encoded in JSON
     *  @throws IOException thrown if there's any problem with conversion
     */
    public static String encodeJSON(JSONArray array) throws IOException{
        StringWriter out = new StringWriter();
        array.writeJSONString(out);

        return out.toString();
    }

    /** Function to encode JSON object to JSON encoded string
     *  @param object JSON object
     *  @return string encoded in JSON
     *  @throws IOException thrown if there's any problem with conversion
     */
    public static String encodeJSON(JSONObject object) throws IOException{
        StringWriter out = new StringWriter();
        object.writeJSONString(out);

        return out.toString();
    }

    /** Function to decode JSON encoded string to JSON object
     *  @param in Reader object
     *  @return JSON object
     *  @throws IOException thrown if there's any problem with reading
     */
    public static JSONObject decodeJSON(Reader in) throws IOException{
        try{
            return (JSONObject) new JSONParser().parse(in);
        }catch(ParseException exception){
            throw new IOException(exception.getMessage());
        }
    }

    /** Function to decode JSON encoded string to JSON object
     *  @param in string to be decoded
     *  @return JSON object
     *  @throws IOException thrown if there's any problem with reading
     */
    public static JSONObject decodeJSON(String in) throws IOException{
        return decodeJSON(new StringReader(in));
    }

    /** Checks if name in String is a valid name
     *  @param name name
     *  @return true if valid, otherwise false
     */
    public static boolean isNameValid(String name){
        if(name == null) return false;
        return Pattern.matches("^([a-z]|[A-Z]|[^0-9]|\\s)+$", name);
    }

    /** Checks if date in string format is valid
     *  String is required to be in YYYYMMDD format otherwise it won't be understood
     *  @param date date
     *  @return true if valid, otherwise false
     */
    public static boolean isDateValid(String date){
        try{
            DateFormat format = new SimpleDateFormat("yyyyMMdd");
            format.setLenient(false);
            format.parse(date);
        }catch(java.text.ParseException e){
            return false;
        }
        return true;
    }

    /** Casts the date in string to Date
     *  String is required to be in YYYYMMDD format otherwise it won't be understood
     *  @param date date
     *  @return date if successful, otherwise null if unsuccessful
     */
    public static Date convertToDate(String date){
        try{
            DateFormat format = new SimpleDateFormat("yyyyMMdd");
            format.setLenient(false);
            return format.parse(date);
        }catch(java.text.ParseException e){
            return null;
        }
    }

    /** Convenient function to write error message as JSON string
     *  @param response ServletResponse object to write to
     *  @param status_code Status code to affix to the response
     *  @param error_code specific error code to affix to the message
     *  @param message error message to affix to the message
     *  @throws IOException thrown when there is a problem with writing to response
     */
    public static void sendError(HttpServletResponse response, int status_code, String error_code, String message) throws IOException{

        // Set the status
        response.setStatus(status_code);

        // Create and define the JSON object
        JSONObject answer = new JSONObject();
        answer.put("status", error_code);
        answer.put("description", message);

        // Set the content type
        response.setContentType("text/json; charset=utf-8");

        // Write the response
        response.getWriter().print(Utility.encodeJSON(answer));
    }
}
