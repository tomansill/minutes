/** Retrieves patron's information, using email address as input
 *  @param email_address patron's email address
 *  @return a table containing patron's name and hash
 *  @throws an exception if email address is NULL
 */
CREATE OR REPLACE FUNCTION get_patron(email_address VARCHAR(128)) RETURNS TABLE(
    address EMAIL_ADDRESS_TYPE,
    hash PATRON_HASH_TYPE
) AS $$
BEGIN

    /* Check if email is not null */
    IF email_address IS NULL THEN RAISE EXCEPTION '{"code":"ERR_INVALID_EMAIL_ADDRESS", "message":"email_address is NULL!"}'; END IF;


    /* Check if email is not null */
    IF NOT is_email_address_valid(RTRIM(email_address)) THEN RAISE EXCEPTION '{"code":"ERR_INVALID_EMAIL_ADDRESS", "message":"email_address is invalid!"}'; END IF;


    RETURN QUERY (SELECT res.address, res.hash FROM get_patron_internal(email_address::EMAIL_ADDRESS_TYPE) AS res);

END; $$
LANGUAGE PLPGSQL STABLE SECURITY DEFINER;

/** Retrieves patron's information, using email address as input
 *  @param email_address patron's email address
 *  @return a table containing patron's name and hash
 *  @throws an exception if email address is NULL
 */
CREATE OR REPLACE FUNCTION get_patron(email_address EMAIL_ADDRESS_TYPE) RETURNS TABLE(
    address EMAIL_ADDRESS_TYPE,
    hash PATRON_HASH_TYPE
) AS $$
BEGIN

    RETURN QUERY (SELECT res.address, res.hash FROM get_patron_internal(email_address) AS res);

END; $$
LANGUAGE PLPGSQL STABLE SECURITY DEFINER;

/** Retrieves patron's information, using hash as input
 *  @param hash_string patron's hash
 *  @return a table containing patron's name and hash
 *  @throws an exception if hash is NULL
 */
CREATE OR REPLACE FUNCTION get_patron(patron_hash PATRON_HASH_TYPE) RETURNS TABLE(
    address EMAIL_ADDRESS_TYPE,
    hash PATRON_HASH_TYPE
) AS $$
BEGIN

    RETURN QUERY (SELECT res.address, res.hash FROM get_patron_internal(patron_hash) AS res);

END; $$
LANGUAGE PLPGSQL STABLE SECURITY DEFINER;

/** Internal function that retrieves patron's information, using email address as input
 *  @param email_address patron's email address
 *  @return a table containing patron's id, name, and hash
 *  @throws an exception if email address is NULL
 */
CREATE OR REPLACE FUNCTION get_patron_internal(in_email_address EMAIL_ADDRESS_TYPE) RETURNS TABLE(
    id INT,
    address EMAIL_ADDRESS_TYPE,
    hash PATRON_HASH_TYPE
) AS $$
BEGIN

    IF patron_exists(in_email_address) THEN
        RETURN QUERY (SELECT Patron.id, Email_Address.address, Patron.hash FROM Patron, Email_Address
            WHERE Patron.id = Email_Address.patron_id AND Email_Address.address = in_email_address AND Email_Address.login AND Email_Address.successor IS NULL);

    ELSE
        RAISE EXCEPTION '{"code":"ERR_USER_HASH_NOT_FOUND", "message":"No Record : The in_email_address ''%'' does not exist in the Patron table!"}', in_email_address;
    END IF;

END; $$
LANGUAGE PLPGSQL STABLE SECURITY INVOKER;

/** Internal function that retrieves patron's information, using hash as input
 *  @param patron_hash patron's hash
 *  @return a table containing patron's id, name, and hash
 *  @throws an exception if hash is NULL
 */
CREATE OR REPLACE FUNCTION get_patron_internal(patron_hash PATRON_HASH_TYPE) RETURNS TABLE(
    id INT,
    address EMAIL_ADDRESS_TYPE,
    hash PATRON_HASH_TYPE
) AS $$
BEGIN

    IF patron_exists(patron_hash) THEN
        RETURN QUERY (SELECT Patron.id, Email_Address.address, Patron.hash FROM Patron, Email_Address
            WHERE Patron.hash = patron_hash AND Patron.id = Email_Address.patron_id AND Email_Address.login AND Email_Address.successor IS NULL);
    ELSE
        RAISE EXCEPTION '{"code":"ERR_USER_HASH_NOT_FOUND", "message":"No Record : The patron hash ''%'' does not exist in the Patron table!"}', patron_hash;
    END IF;

END; $$
LANGUAGE PLPGSQL STABLE SECURITY INVOKER;

/** Internal function that retrieves patron's information, using id as input
 *  @param id_number patron's id
 *  @return a table containing patron's id, name, and hash
 *  @throws an exception if id is NULL
 */
CREATE OR REPLACE FUNCTION get_patron(id_number INT) RETURNS TABLE(
    address EMAIL_ADDRESS_TYPE,
    hash PATRON_HASH_TYPE
) AS $$
BEGIN

    RETURN QUERY (SELECT res.address, res.hash FROM get_patron(id_number) AS res);

END; $$
LANGUAGE PLPGSQL STABLE SECURITY INVOKER;

/** Internal function that retrieves patron's information, using id as input
 *  @param id_number patron's id
 *  @return a table containing patron's id, name, and hash
 *  @throws an exception if id is NULL
 */
CREATE OR REPLACE FUNCTION get_patron_internal(id_number INT) RETURNS TABLE(
    id INT,
    address EMAIL_ADDRESS_TYPE,
    hash PATRON_HASH_TYPE
) AS $$
BEGIN

    IF id_number IS NULL THEN
        RAISE EXCEPTION '{"code":"ERR_INVALID_USER_ID", "message":"id_number is NULL!"}';
    END IF;

    IF patron_exists(id_number) THEN
        RETURN QUERY (SELECT Patron.id, Email_Address.address, Patron.hash FROM Patron, Email_Address
            WHERE id_number = Patron.id AND Email_Address.patron_id = Patron.id AND Email_Address.login AND Email_Address.successor IS NULL);
    ELSE
        RAISE EXCEPTION '{"code":"ERR_USER_ID_NOT_FOUND", "message":"No Record : The id ''%'' does not exist in the Patron table!"}', id_number;
    END IF;

END; $$
LANGUAGE PLPGSQL STABLE SECURITY INVOKER;

/** Retrieves patron's date joined, using email address as input
 *  @param email_address patron's email address
 *  @return date that patron joined
 *  @throws an exception if hash is NULL
 */
CREATE OR REPLACE FUNCTION get_patron_date_joined(email_address EMAIL_ADDRESS_TYPE) RETURNS DATE AS $$
BEGIN

    RETURN (SELECT get_patron_date_joined((SELECT hash FROM get_patron(email_address))));

END; $$
LANGUAGE PLPGSQL STABLE SECURITY DEFINER;

/** Retrieves patron's date joined, using hash as input
 *  @param hash_string patron's hash
 *  @return date that patron joined
 *  @throws an exception if hash is NULL
 */
CREATE OR REPLACE FUNCTION get_patron_date_joined(patron_hash PATRON_HASH_TYPE) RETURNS DATE AS $$
BEGIN

    RETURN (SELECT pat.date_joined FROM Patron pat WHERE pat.hash = patron_hash);

END; $$
LANGUAGE PLPGSQL STABLE SECURITY DEFINER;
