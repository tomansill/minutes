/** Function to check if the patron is in the system using primary email address as input
 *  @param in_email_address patron's email address
 *  @return result in boolean, true if exists, otherwise false
 */
CREATE OR REPLACE FUNCTION patron_exists(in_email_address EMAIL_ADDRESS_TYPE) RETURNS BOOLEAN AS $$
BEGIN

    IF RTRIM(in_email_address) IN (SELECT address FROM Email_Address WHERE login AND successor IS NULL) THEN RETURN TRUE;
    ELSE RETURN FALSE;
    END IF;

    RETURN (EXISTS (SELECT Email_Address.address FROM Email_Address WHERE Email_Address.address = in_email_address AND Email_Address.login AND Email_Address.successor IS NULL));

END; $$
LANGUAGE PLPGSQL STABLE SECURITY DEFINER;

/** Function to check if the patron is in the system using hash as input
 *  @param in_hash patron's hash
 *  @return result in boolean, true if exists, otherwise false
 */
CREATE OR REPLACE FUNCTION patron_exists(in_hash PATRON_HASH_TYPE) RETURNS BOOLEAN AS $$
BEGIN

    /* Patrons with hash consisting only '.' are uninitialized, therefore techinically does not exist in the system just yet */
    IF in_hash = '.' THEN RETURN FALSE; END IF;

    RETURN (EXISTS (SELECT Patron.hash FROM Patron WHERE Patron.hash = in_hash));

END; $$
LANGUAGE PLPGSQL STABLE SECURITY DEFINER;

/** Internal function to check if the patron is in the system using id number as input
 *  @param in_id patron's id
 *  returns result in boolean, true if exists, otherwise false
 */
CREATE OR REPLACE FUNCTION patron_exists(in_id INT) RETURNS BOOLEAN AS $$
BEGIN

    /* Verify that input is not NULL */
    IF in_id IS NULL THEN RETURN FALSE; END IF;

    RETURN (EXISTS (SELECT Patron.id FROM Patron WHERE Patron.id = in_id));

END; $$
LANGUAGE PLPGSQL STABLE SECURITY INVOKER;
