/** Authenicates the patron and assign session
 *  Return session if authenication is successful - return "disabled" for special cases like logins are disabled
 *  Raises an exception if patron does not exist or in_password is NULL/Empty
 */
CREATE OR REPLACE FUNCTION authenicate_patron(email_address VARCHAR(128), in_password VARCHAR(128)) RETURNS TABLE(
    session SESSION_TYPE,
    two_factor_authenication BOOLEAN
) AS $$
BEGIN

    /* Check if email is not null */
    IF email_address IS NULL THEN RAISE EXCEPTION '{"code":"ERR_INVALID_EMAIL_ADDRESS", "message":"email_address is NULL!"}'; END IF;


    /* Check if email is not null */
    IF NOT is_email_address_valid(RTRIM(email_address)) THEN RAISE EXCEPTION '{"code":"ERR_INVALID_EMAIL_ADDRESS", "message":"email_address is invalid!"}'; END IF;

    /* Execute the query */
    RETURN QUERY (SELECT * FROM authenicate_patron((SELECT hash FROM get_patron(email_address::EMAIL_ADDRESS_TYPE)), in_password));

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

/** Authenicates the patron and assign session
 *  Return session if authenication is successful - return "disabled" for special cases like logins are disabled
 *  Raises an exception if patron does not exist or in_password is NULL/Empty
 */
CREATE OR REPLACE FUNCTION authenicate_patron(email_address EMAIL_ADDRESS_TYPE, in_password VARCHAR(128)) RETURNS TABLE(
    session SESSION_TYPE,
    two_factor_authenication BOOLEAN
) AS $$
BEGIN

    RETURN QUERY (SELECT * FROM authenicate_patron((SELECT hash FROM get_patron(email_address)), in_password));

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

/** Authenicates the patron and assign session
 *  Return session if authenication is successful - return "disabled" for special cases like logins are disabled
 *  Raises an exception if patron does not exist or in_password is NULL/Empty
 */
CREATE OR REPLACE FUNCTION authenicate_patron(patron_hash PATRON_HASH_TYPE, in_password VARCHAR(128)) RETURNS TABLE(
    session SESSION_TYPE,
    two_factor_authenication BOOLEAN
) AS $$
BEGIN

    IF in_password IS NULL THEN RAISE EXCEPTION '{"code":"ERR_INVALID_PASSWORD", "message":"in_password is NULL!"}'; END IF;
    IF RTRIM(in_password) LIKE '' THEN RAISE EXCEPTION '{"code":"ERR_INVALID_PASSWORD", "message":"in_password is empty!"}'; END IF;

    IF access_control_check_access('login') OR patron_hash = (SELECT hash FROM get_patron(0)) THEN
        IF patron_exists(patron_hash) THEN
            IF check_patron_password(patron_hash::PATRON_HASH_TYPE, in_password) THEN
                DECLARE
                    in_user_id INT := (SELECT id FROM get_patron_internal(patron_hash));
                    in_session SESSION_TYPE;
                    require_2fa BOOLEAN := FALSE;
                BEGIN
                    /* Deletes all old sessions */
                    DELETE FROM Session WHERE AGE(NOW(), last_updated) > GLOBAL_session_timeout_interval();

                    /* Check if 2FA is needed */
                    IF (SELECT p.totp_secret FROM Patron p WHERE p.id = in_user_id) IS NOT NULL THEN
                        require_2fa := TRUE;
                    END IF;

                    /* Inserts new session */
                    INSERT INTO Session (
                        patron_id, hash,
                        date_created,
                        last_updated,
                        require_2fa
                    ) VALUES (
                        in_user_id,
                        SHA512(CONCAT(SHA512(CAST(RTRIM(patron_hash) AS TEXT)),
                        CONCAT(encode(gen_random_bytes(8), 'hex'), NOW()))), NOW(), NOW(),
                        require_2fa
                    ) RETURNING hash INTO in_session;

                    RETURN QUERY (SELECT in_session, require_2fa);
                END;
            ELSE
                RETURN QUERY (SELECT hash, TRUE AS two_factor_authenication FROM Session WHERE 1 = 0); /* Return empty set */
                /* RAISE EXCEPTION '#3 Invalid password'; */
            END IF;
        ELSE
            RAISE EXCEPTION '{"code":"ERR_USER_HASH_NOT_FOUND", "message":"No Record : The patron_hash address ''%'' does not exist in the Patron table!"}', patron_hash;
        END IF;
    ELSE
        RAISE EXCEPTION '{"code":"ERR_LOGIN_DISABLED", "message":"Logins are disabled"}';
    END IF;

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;


CREATE OR REPLACE FUNCTION authenicate_patron_2FA(in_session SESSION_TYPE, code VARCHAR(6)) RETURNS BOOLEAN AS $$
BEGIN

    /* Since we're not using is_session_alive function, we're verifying the sessions here */
    /* Check if session is not null */
    IF in_session = NULL THEN RAISE EXCEPTION '{"code":"ERR_INVALID_SESSION", "message":"in_session is NULL!"}'; END IF;

    /* Check if session is not empty */
    IF in_session LIKE '' THEN RAISE EXCEPTION '{"code":"ERR_INVALID_SESSION", "message":"in_session is empty!"}'; END IF;

    /* Check if code is not null */
    IF code = NULL THEN RAISE EXCEPTION '{"code":"ERR_INVALID_TWO_FACTOR_AUTH_CODE", "message":"code is NULL!"}'; END IF;

    /* Check if code is not empty */
    IF code LIKE '' THEN RAISE EXCEPTION '{"code":"ERR_INVALID_TWO_FACTOR_AUTH_CODE", "message":"code is empty!"}'; END IF;

    /* Clean up old session attempts */
    DELETE FROM Session WHERE require_2fa AND AGE(NOW(), last_updated) > GLOBAL_2FA_timeout_interval();

    /* Check if session exists */
    IF EXISTS (SELECT * FROM Session WHERE hash = in_session AND require_2fa) THEN

        /* Get totp secret */
        DECLARE
            totp_secret VARCHAR(64) := (SELECT totp_secret FROM Patron WHERE id = (SELECT id FROM identify_patron_from_session_internal(in_session)));
        BEGIN
            /* Check if patron has a 2FA set up */
            IF totp_secret IS NULL THEN
                RAISE EXCEPTION '{"code":"ERR_NO_TWO_FACTOR_AUTH", "message":"User does not have 2FA set up!"}';
            END IF;

            /* Check if code is correct */
            IF get_TOTP(totp_secret) = code THEN

                /* Update the session to be fully authenicated */
                UPDATE Session SET require_2fa = FALSE WHERE hash = in_session;

                /* Inform the operation was successful */
                RETURN TRUE;
            ELSE RETURN FALSE; END IF;
        END;
    ELSE
        RAISE EXCEPTION '{"code":"ERR_NO_LOGIN", "message":"User is not logged in!"}';
    END IF;

    /* fail safe */
    RETURN FALSE;

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;


/** Queries and see if session has been expired - also deletes all old sessions
 *  Return true if session is alive otherwise false
 */
CREATE OR REPLACE FUNCTION is_session_alive(in_session SESSION_TYPE, update_session BOOLEAN) RETURNS BOOLEAN AS $$
BEGIN

    /* False if session is null */
    IF in_session IS NULL THEN RETURN FALSE; END IF;

    DECLARE
        /* Check if session is alive */
        result BOOLEAN := in_session IN (SELECT hash FROM Session WHERE Session.hash = in_session AND AGE(NOW(), last_updated) < GLOBAL_session_timeout_interval() AND NOT require_2fa);
    BEGIN

        /* If session is alive and the update flag is true, update to delay the session expiration */
        IF update_session AND result THEN
            UPDATE Session SET last_updated = NOW() WHERE hash = in_session;
        END IF;

        /* If session is dead, then remove it */
        IF NOT result THEN
            DELETE FROM Session WHERE hash = in_session;
        END IF;

        /* Return the result */
        RETURN result;
    END;

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

/** Provides information about the session
 *  Return 'NO_SESSION', 'EXPIRED, 'TWO_FACTOR_AUTHENICATION', and 'LOGIN'
 */
CREATE OR REPLACE FUNCTION get_login_information(in_session SESSION_TYPE) RETURNS VARCHAR(24) AS $$
BEGIN

    /* False if session is null */
    IF in_session IS NULL THEN RETURN 'NO_SESSION' ; END IF;

    /* Check if session exists at all */
    IF NOT EXISTS (SELECT * FROM Session WHERE Session.hash = in_session) THEN RETURN 'NO_SESSION'; END IF;

    /* Check if session is expired */
    IF AGE(NOW(), (SELECT last_updated FROM Session WHERE Session.hash = in_session)) >= GLOBAL_session_timeout_interval() THEN RETURN 'EXPIRED'; END IF;

    /* Check if session requires 2FA*/
    IF (SELECT require_2fa FROM Session WHERE Session.hash = in_session) THEN RETURN 'TWO_FACTOR_AUTHENICATION'; END IF;

    RETURN 'LOGIN';

END; $$
LANGUAGE PLPGSQL STABLE SECURITY DEFINER;

/** Kills the specified session
 *  Return true if session killing is successful
 */
CREATE OR REPLACE FUNCTION kill_session(in_session SESSION_TYPE) RETURNS BOOLEAN AS $$
BEGIN

    /* False if session is null */
    IF in_session IS NULL THEN RETURN FALSE; END IF;

    /* Attempt to kill the session */
    IF EXISTS (SELECT * FROM Session WHERE hash = in_session AND NOT require_2fa) THEN
        DELETE FROM Session WHERE hash = in_session;
        RETURN TRUE;
    ELSE
        RETURN FALSE;
    END IF;
END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

/** Function to identify the patron behind a session
 *  @param session session to identify the patron from
 *  @return a table of patron's id, email address, and hash
 */
CREATE OR REPLACE FUNCTION identify_patron_from_session_internal(in_session SESSION_TYPE) RETURNS TABLE(
    id INT,
    address EMAIL_ADDRESS_TYPE,
    hash PATRON_HASH_TYPE
) AS $$
BEGIN

    /* Return info */
    RETURN QUERY (SELECT Patron.id, Email_Address.address, Patron.hash FROM Patron, Session, Email_Address
        WHERE Patron.id = Email_Address.patron_id AND Patron.id = Session.patron_id AND Session.hash = in_session
        AND Email_Address.login AND Email_Address.successor IS NULL);

END; $$
LANGUAGE PLPGSQL STABLE SECURITY INVOKER;

/** Function to identify the patron behind a session
 *  @param session session to identify the patron from
 *  @return a table of patron's id, email address, and hash
 */
CREATE OR REPLACE FUNCTION identify_patron_from_session(in_session SESSION_TYPE) RETURNS TABLE(
    address EMAIL_ADDRESS_TYPE,
    hash PATRON_HASH_TYPE
) AS $$
BEGIN

    /* Return info */
    RETURN QUERY (SELECT res.address, res.hash FROM identify_patron_from_session_internal(in_session) AS res);

END; $$
LANGUAGE PLPGSQL STABLE SECURITY DEFINER;
