package tomansill.minutes;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.Cookie;

/** A bundle of functions that handles database functions to Session functions
 *  @author Thomas Ansill
 */
public class SessionDB{

    /** Server config */
    private Config server_config;
    private UserDB udb;

    /** Constuctor */
    public SessionDB(Config server_config){
        this.server_config = server_config;
        this.udb = new UserDB(server_config);
    }

    /** Function to create a fresh session cookie
     *  @param session_hash session hash
     *  @param remember_me true to set cookie's max age to proper value, false
     *      to set cookie to expire when browser closes
     *  @return Cookie
     */
    private static Cookie createCookie(String session_hash, boolean remember_me){

        // Define the cookies
        Cookie response_cookie = new Cookie("session", session_hash);
        if(remember_me) response_cookie.setMaxAge(30 * 60 * 60); //30 hours - need more sophsicated method to get value directly from database
        else response_cookie.setMaxAge(-1);

        response_cookie.setPath("/");
        //response_cookie.setSecure(true);

        return response_cookie;
    }

    // Do we need this one?
    private static Cookie updateCookie(Cookie cookie, boolean remember_me){

        if(remember_me) cookie.setMaxAge(30 * 60 * 60); //30 hours - need more sophsicated method to get value directly from database
        else cookie.setMaxAge(-1);

        cookie.setPath("/");

        return cookie;
    }

    /** Function to read session hash from cookie
     *  @param cookie Cookie
     *  @return session hash if found in cookie, otherwise null if no session hash is found
     */
    public static String getSessionHashFromCookie(Cookie cookie){
        // Get cookies from request to get hashes if any
        if(cookie == null) return null;

        // Check if cookie is correctly labeled
        if(!cookie.getName().equals("session")) return null;

        // Check if cookie is not null or empty
        if(cookie.getValue() == null || cookie.getValue().trim().equals("")) return null;

        // Cleared to return
        return cookie.getValue();
    }

    /** Function to read session hash from cookies
     *  @param cookie array of cookies
     *  @return session hash if found in cookie, otherwise null if no session hash is found
     */
    public static String getSessionHashFromCookies(Cookie[] cookies){
        // Get cookies from request to get hashes if any
        if(cookies == null) return null;

        // Search through cookies for user and session hashes
        for(int i = 0; i < cookies.length; i++){

            // Only look for a cookie named session
            if(cookies[i].getName().equals("session")){

                // Look for active cookie, cookies with null or empty values dont count
                if(cookies[i].getValue() != null && !cookies[i].getValue().trim().equals("")){
                    return cookies[i].getValue();
                }
            }
        }

        // Return null if all fail
        return null;
    }

    /** Creates new cookie with no session hash to wipe out the older cookie
     *  @return new cookie that will destroy old cookie when returned to user
     */
    public static Cookie destroyCookie(){

        // Define the cookies
        Cookie response_cookie = new Cookie("session", null);
        response_cookie.setMaxAge(0);
        response_cookie.setPath("/");

        return response_cookie;
    }

    /** Remove session hash from cookie
     *  @return updated cookie
     */
    public static Cookie destroyCookie(Cookie cookie){

        cookie.setValue(null);
        cookie.setMaxAge(0);

        return cookie;
    }

    /** Function to make a database connection and destroy the session, then return the cookie
     *  @param request_cookies array of cookies that may have session
     *  @return Updated cookie if successful, otherwise null if unsuccessful
     *  @throws SQLException thrown if there's a problem with database connection
     */
    public Cookie destroySession(Cookie[] request_cookies) throws SQLException{
        return destroySession(getSessionHashFromCookies(request_cookies));
    }

    /** Function to make a database connection and destroy the session, then return the cookie
     *  @param session_hash session hash
     *  @return Updated cookie if successful, otherwise null if unsuccessful
     *  @throws SQLException thrown if there's a problem with database
     */
    public Cookie destroySession(String session_hash) throws SQLException{

        // Check if Server config is initialized. It must be initialized to use the database
        if(server_config == null){
            System.err.println("ERROR: The server config is not initialized.");
            throw new SQLException("ERROR: The server config is not initialized. Meaning there's no database connection!");
        }

        // Verify session hash
        if(session_hash == null) return null;

        // Execute the SQL query
        PreparedStatement stmt = server_config.db.connection.prepareStatement("SELECT kill_session(?::SESSION_TYPE)");
        stmt.setString(1, session_hash);
        ResultSet rs = stmt.executeQuery();

        // Get the result - if exception is thrown for empty rs, then database has issues, not this
        rs.next();
        boolean answer = rs.getBoolean(1);

        // Clean up
        rs.close();
        stmt.close();

        // If session was actually killed, update the cookies to prompt the browser to delete it ASAP
        if(answer) return destroyCookie();
        else return null;
    }

    /** Function to check if the session is not expired

     *  @param request_cookies array of cookies that may contain session hash
     *  @return true if alive, otherwise false
     *  @throws SQLException thrown if there's a problem with database
     */
    public boolean isSessionAlive(Cookie[] request_cookies) throws SQLException{
        return isSessionAlive(getSessionHashFromCookies(request_cookies));
    }

    /** Function to check if the session is not expired
     *  @param session_hash session hash
     *  @return true if alive, otherwise false
     *  @throws SQLException thrown if there's a problem with database
     */
    public boolean isSessionAlive(String session_hash) throws SQLException{

        // Check if Server config is initialized. It must be initialized to use the database
        if(server_config == null){
            System.err.println("ERROR: The server config is not initialized.");
            throw new SQLException("ERROR: The server config is not initialized. Meaning there's no database connection!");
        }

        // Check if cookie is properly formed
        if(session_hash == null || session_hash.trim().equals("")) return false;

        // Answer
        boolean answer = false;

        // Execute SQL query
        PreparedStatement stmt = server_config.db.connection.prepareStatement("SELECT is_session_alive(?::SESSION_TYPE,?)");
        stmt.setString(1, session_hash);
        stmt.setBoolean(2, false);
        ResultSet rs = stmt.executeQuery();

        // Get the result - if exception is thrown for empty rs, then database has issues, not this
        rs.next();
        answer = rs.getBoolean(1);

        // Clean up
        rs.close();
        stmt.close();

        return answer;
    }

    /** Function to check if the session is not expired and update it
     *  @param request_cookies array of cookies that may have session hash
     *  @return Cookie if session is good and updated, null if session is expired
     *  @throws SQLException thrown if there's a problem with database
     */
    public Cookie updateSessionIfAlive(Cookie[] request_cookies, boolean remember_me) throws SQLException{
        return updateSessionIfAlive(getSessionHashFromCookies(request_cookies), remember_me);
    }

    /** Function to check if the session is not expired and update it
     *  @param session_hash session hash
     *  @return Cookie if session is good and updated, null if session is expired
     *  @throws SQLException thrown if there's a problem with database
     */
    public Cookie updateSessionIfAlive(String session_hash, boolean remember_me) throws SQLException{

        // Check if Server config is initialized. It must be initialized to use the database
        if(server_config == null){
            System.err.println("ERROR: The server config is not initialized.");
            throw new SQLException("ERROR: The server config is not initialized. Meaning there's no database connection!");
        }

        // Check if cookie is properly formed
        if(session_hash == null || session_hash.trim().equals("")) return null;

        boolean answer = false;

        // Execute SQL query
        PreparedStatement stmt = server_config.db.connection.prepareStatement("SELECT is_session_alive(?::SESSION_TYPE,?)");
        stmt.setString(1, session_hash);
        stmt.setBoolean(2, true);
        ResultSet rs = stmt.executeQuery();

        // Get the result - if exception is thrown for empty rs, then database has issues, not this
        rs.next();
        answer = rs.getBoolean(1);

        // Clean up
        rs.close();
        stmt.close();

        if(!answer) return null;

        // Create new cookie
        return createCookie(session_hash, remember_me);
    }

    /** Function to authenicate an user
     *  @param email_address user's email_address
     *  @param password password
     *  @param remember_me true to set a proper session expiration date, otherwise expire as soon as user closes the browser
     *  @return Authenication_Result cookie is null if unsuccessful, status describes the authenication status
     *  @throws Exception thrown if there's more than one row in SQL result - should call for Internal Server Error
     *  @throws SQLException thrown if there's a problem with the database
     */
    public Authenication_Result authenicate(Cookie[] request_cookies, String email_address, String password, boolean remember_me) throws SQLException{

        // Check if Server config is initialized. It must be initialized to use the database
        if(server_config == null){
            System.err.println("ERROR: The server config is not initialized.");
            throw new SQLException("ERROR: The server config is not initialized. Meaning there's no database connection!");
        }

        // Create the return object
        Authenication_Result ar = new Authenication_Result(null, Authenication_Status.FAILURE);

        // If session is alive, keep it (and update the session)
        ar.cookie = updateSessionIfAlive(request_cookies, remember_me);
        if(ar.cookie != null){
            ar.status = Authenication_Status.SUCCESS;
            return ar;
        }

        // Authenicate the cookie
        PreparedStatement stmt = server_config.db.connection.prepareStatement("SELECT * FROM authenicate_patron(?::EMAIL_ADDRESS_TYPE,?)");
        stmt.setString(1, email_address);
        stmt.setString(2, password);
        ResultSet rs = stmt.executeQuery();

        // Get the query, if nothing in it, then the authenication has failed
        if(rs.next()){
            // Make sure result are not empty
            if(rs.getString(1) == null && rs.getString(1).equals("")){
                return ar;
            }

            // Build cookie and put it in return object
            ar.cookie = createCookie(rs.getString(1), remember_me);
            ar.status = rs.getBoolean(2) ? Authenication_Status.TWO_FACTOR_AUTHENICATION : Authenication_Status.SUCCESS;

        }else{
            ar.cookie = null;
        }

        // Clean up
        rs.close();
        stmt.close();

        return ar;
    }

    /** Function to find out who's behind the session hash
     *  @param request_cookies array of cookies that may contain session hash
     *  @return email address if successful otherwise null
     *  @throws SQLException thrown if there's a problem with database
     */
    public String getEmailAddressFromSession(Cookie[] request_cookies) throws Exception, SQLException{
        return getEmailAddressFromSession(getSessionHashFromCookies(request_cookies));
    }

    /** Function to find out who's behind the session hash

     *  @param session_hash session hash
     *  @return email address if successful otherwise null
     *  @throws SQLException thrown if there's a problem with database
     */
    public String getEmailAddressFromSession(String session_hash) throws SQLException{

        // Check if Server config is initialized. It must be initialized to use the database
        if(server_config == null){
            System.err.println("ERROR: The server config is not initialized.");
            throw new SQLException("ERROR: The server config is not initialized. Meaning there's no database connection!");
        }

        // Verify session
        if(session_hash == null) return null;

        // Check if session is alive
        if(session_hash == null && !isSessionAlive(session_hash)){
            return null;
        }

        String email_address = null;

        // Query the email address
        PreparedStatement stmt = server_config.db.connection.prepareStatement("SELECT address FROM identify_patron_from_session(?::SESSION_TYPE)");
        stmt.setString(1, session_hash);
        ResultSet rs = stmt.executeQuery();

        // Get the result
        if(rs.next()){
            email_address = rs.getString(1);
        }

        // Clean up
        rs.close();
        stmt.close();

        return email_address;
    }
    /** Function to find out who's behind the session hash
     *  @param request_cookies array of cookies that may contain session hash
     *  @return user hash if successful otherwise null
     *  @throws SQLException thrown if there's a problem with database
     */
    public String getUserHashFromSession(Cookie[] request_cookies) throws Exception, SQLException{
        return getUserHashFromSession(getSessionHashFromCookies(request_cookies));
    }

    /** Function to find out who's behind the session hash
     *  @param session_hash session hash
     *  @return user hash if successful otherwise null
     *  @throws SQLException thrown if there's a problem with database
     */
    public String getUserHashFromSession(String session_hash) throws SQLException{

        // Check if Server config is initialized. It must be initialized to use the database
        if(server_config == null){
            System.err.println("ERROR: The server config is not initialized.");
            throw new SQLException("ERROR: The server config is not initialized. Meaning there's no database connection!");
        }

        // Verify session
        if(session_hash == null) return null;

        // Check if session is alive
        if(session_hash == null && !isSessionAlive(session_hash)){
            return null;
        }

        String user_hash = null;

        // Query the user hash
        PreparedStatement stmt = server_config.db.connection.prepareStatement("SELECT hash FROM identify_patron_from_session(?::SESSION_TYPE)");
        stmt.setString(1, session_hash);
        ResultSet rs = stmt.executeQuery();

        // Get the result
        if(rs.next()){
            user_hash = rs.getString(1);
        }

        // Clean up
        rs.close();
        stmt.close();

        return user_hash;
    }

    /** Function to find the information about the session
     *  @param session_hash session hash
     *  @return a string containing information. Possible values: 'NO_LOGIN', 'LOGIN', 'TWO_FACTOR_AUTHENICATION', and 'EXPIRED'
     *  @throws SQLException thrown if there's a problem with database
     */
    public String getLoginInformation(String session_hash) throws SQLException{

        // Check if Server config is initialized. It must be initialized to use the database
        if(server_config == null){
            System.err.println("ERROR: The server config is not initialized.");
            throw new SQLException("ERROR: The server config is not initialized. Meaning there's no database connection!");
        }

        String answer = null;

        // Query the user hash
        PreparedStatement stmt = server_config.db.connection.prepareStatement("SELECT get_login_information(?::SESSION_TYPE)");
        stmt.setString(1, session_hash);
        ResultSet rs = stmt.executeQuery();

        // Get the result
        if(rs.next()){
            answer = rs.getString(1);
        }

        // Clean up
        rs.close();
        stmt.close();

        // If answer is null, then there's a problem with the database
        if(answer == null) throw new SQLException("The function 'get_login_information(SESSION_TYPE)' in the database returned nothing!");

        return answer;
    }

    enum Authenication_Status{
        SUCCESS, TWO_FACTOR_AUTHENICATION, FAILURE, DISABLED
    }

    class Authenication_Result{
        public Cookie cookie;
        public Authenication_Status status;
        public Authenication_Result(Cookie cookie, Authenication_Status status){
            this.cookie = cookie;
            this.status = status;
        }
    }
}
