CREATE TABLE Properties(
    id SERIAL NOT NULL,
    key VARCHAR(200) NOT NULL,
    value TEXT DEFAULT NULL,
    PRIMARY KEY(key)
);

CREATE TABLE Statistics(
    id SERIAL NOT NULL,
    key VARCHAR(200) NOT NULL,
    value BIGINT DEFAULT 0,
    PRIMARY KEY(key)
);

CREATE TABLE Access_Agent(
    id SERIAL NOT NULL,
    ip_address VARCHAR(39) NOT NULL,
    user_agent TEXT NOT NULL,
    count_requests INT NOT NULL DEFAULT 1,
    PRIMARY KEY(id)
);

CREATE TABLE Known_Association(
    id SERIAL NOT NULL,
    agent_id INT NOT NULL,
    patron_id INT NOT NULL,
    FOREIGN KEY(agent_id) REFERENCES Access_Agent(id),
    FOREIGN KEY(patron_id) REFERENCES Patron(id)
);

CREATE TABLE Access_Resource(
    id SERIAL NOT NULL,
    url TEXT NOT NULL,
    method VARCHAR(10) NOT NULL,
    count_accessed INT NOT NULL DEFAULT 1,
    PRIMARY KEY(id)
);

CREATE TABLE Access_Log(
    id SERIAL NOT NULL,
    agent_id INT NOT NULL,
    resource_id INT NOT NULL,
    patron_id INT DEFAULT NULL,
    date_logged TIMESTAMP NOT NULL DEFAULT NOW(),
    status_code INT NOT NULL DEFAULT -1,
    content TEXT DEFAULT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(agent_id) REFERENCES Access_Agent(id),
    FOREIGN KEY(resource_id) REFERENCES Access_Resource(id),
    FOREIGN KEY(patron_id) REFERENCES Patron(id)
);

/** Error Severity Enum */
DROP TYPE IF EXISTS ERROR_SEVERITY;
CREATE TYPE ERROR_SEVERITY AS ENUM('INFO', 'WARNING', 'SEVERE');

CREATE TABLE Error_Log(
	id SERIAL NOT NULL,
    level ERROR_SEVERITY NOT NULL,
    date_logged TIMESTAMP NOT NULL DEFAULT NOW(),
    caused_by INT DEFAULT NULL,
    message VARCHAR(200) NOT NULL,
    description TEXT,
	PRIMARY KEY(id),
    FOREIGN KEY(caused_by) REFERENCES Access_Log(id)
);
