package tomansill.minutes;

import java.io.StringReader;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import java.sql.SQLException;

/** Class that describes an exception coming from SQL functions */
public class SQLExceptionResponse{

    /** Error code */
    private String code;

    /** Error message */
    private String message;

    /** The SQL function the exception was thrown from */
    private String function;

    /** the line number where the exception was thrown */
    private int lineNumber;

    /** The stack trace */
    private StackTraceElement[] stackTrace;

    /** Constructor for response object
     *  @param code Error code
     *  @param message Error message
     *  @param function SQL function where the exception was thrown from
     *  @param stackTrace The stack trace
     */
    private SQLExceptionResponse(String code, String message, String function, int lineNumber, StackTraceElement[] stackTrace){
        this.code = code;
        this.message = message;
        this.function = function;
        this.lineNumber = lineNumber;
        this.stackTrace = stackTrace;
    }

    /** Gets the code
     *  @return code
     */
    public String getCode(){ return this.code; }

    /** Gets the message
     *  @return message
     */
    public String getMessage(){ return this.message; }

    /** Gets the function name
     *  @return function name
     */
    public String getFunctionName(){ return this.function; }

    /** Gets the stack trace
     *  @return stack trace
     */
    public StackTraceElement[] getStackTrace(){ return this.stackTrace; }

    /** SQLExceptionResponse's toString method
     *  @return string representation of SQLExceptionResponse without stackTrace
     */
    public String toString(){
        StringBuffer sb = new StringBuffer();
        sb.append("SQLExceptionResponse(code=\"");
        sb.append(this.code);
        sb.append("\", message=\"");
        sb.append(this.message);
        sb.append("\", function=\"");
        sb.append(this.function);
        sb.append("\", lineNumber=");
        sb.append(this.lineNumber);
        sb.append(")");
        return sb.toString();
    }

    /** Attempts to interpret the SQL exception. If it fails to interpret, then it will return null. If interpretation is successful, then it will return a response object
     *  @param exception SQL exception
     *  @return response object if the interpretation was successful, if it is not successful, then it will return null
     */
    public static SQLExceptionResponse interpret(SQLException exception){
        SQLExceptionResponse res = null;

        // Remove 'ERROR: ' string
        String message = exception.getMessage().replace("ERROR: ", "");
        int pos_msg = message.indexOf("Where: PL/pgSQL function "); //25 character

        // Extract some of function_name
        String function_name = message.substring(pos_msg+25);

        // Extract some of line number
        int pos = function_name.indexOf(") line "); //7 character
        String line_number = function_name.substring(pos+7);

        // Clean the remainder of function_name
        function_name = function_name.substring(0, pos+1); // +1 to include the last ')'

        // Clean the remainder of line number
        pos = line_number.indexOf(" at RAISE"); //9 character
        line_number = line_number.substring(0, pos);

        // Clean the message
        message = message.substring(0, pos_msg);

        // Convert the line number to integer
        int func_line_number = -1;
        try{
            func_line_number = Integer.parseInt(line_number);
        }catch(Exception e){} // Do nothing

        try{
            StringReader sr = new StringReader(message);
            JSONObject obj = (JSONObject) new JSONParser().parse(sr);
            sr.close();
            res = new SQLExceptionResponse((String)obj.get("code"), (String)obj.get("message"), function_name, func_line_number, exception.getStackTrace());
        }catch(Exception e){} // Doesn't matter what exception says - interpretation has failed

        return res;
    }
}
