#!/bin/bash

cd back_end/minutes/target
jarname=$(ls *.jar)
cd ../../..
sudo java -cp "back_end/minutes/target/$jarname:back_end/minutes/target/dependency/*" tomansill/minutes/MainServer
