/** No-Login's Function to check if current user has the access to specific method
 *  @param in_method method name to be invoked
 *  @return true if allowed, otherwise false
 */
CREATE OR REPLACE FUNCTION access_control_check_access(in_method VARCHAR(200)) RETURNS BOOLEAN AS $$
BEGIN

    /* Check if method is not null and empty*/
    IF in_method IS NULL THEN RAISE EXCEPTION '{"code":"ERR_INVALID_ACCESS_METHOD", "message":"in_method is NULL!"}'; END IF;
    IF RTRIM(in_method) LIKE '' THEN RAISE EXCEPTION '{"code":"ERR_INVALID_ACCESS_METHOD", "message":"in_method is empty!"}'; END IF;

    /* Check no_login */
    RETURN EXISTS (SELECT * FROM Access_Matrix WHERE active AND black_list AND type = 'no_login' AND method = in_method AND entry_end_time IS NULL);

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

/** Function to check if current user has the access to specific method
 *  @param session_hash hash of current session
 *  @param in_method method name to be invoked
 *  @return true if allowed, otherwise false
 */
CREATE OR REPLACE FUNCTION access_control_check_access(session SESSION_TYPE, in_method VARCHAR(200)) RETURNS BOOLEAN AS $$
BEGIN

    /* Check if method is not null and empty*/
    IF in_method IS NULL THEN RAISE EXCEPTION '{"code":"ERR_INVALID_ACCESS_METHOD", "message":"in_method is NULL!"}'; END IF;
    IF RTRIM(in_method) LIKE '' THEN RAISE EXCEPTION '{"code":"ERR_INVALID_ACCESS_METHOD", "message":"in_method is empty!"}'; END IF;

    /* Check if session exists */
    IF is_session_alive(session, FALSE) THEN

        DECLARE
            in_patron_id INT := (SELECT id FROM identify_patron_from_session_internal(session));
            hash PATRON_HASH_TYPE := (SELECT hash FROM identify_patron_from_session(session));
        BEGIN

            /** If patron is admin, then always true, admin have literally all access */
            IF in_patron_id = 0 THEN RETURN TRUE; END IF;

            /* Check patron type first */
            IF EXISTS
                (SELECT * FROM Access_Matrix
                    WHERE type = 'patron'
                    AND name = in_patron_id::text
                    AND method = in_method
                    AND active
                    AND black_list
                    AND entry_end_time IS NULL
                ) THEN RETURN TRUE;
            END IF;

            /* Check position type, if patron doesn't have any current position, skip, otherwise check if position is allowed in the matrix */
            IF EXISTS (SELECT * FROM Executive_Board_Position WHERE patron_id = in_patron_id AND successor IS NULL) THEN
                DECLARE
                    position_title VARCHAR(60) := (SELECT title FROM Executive_Board_Position WHERE patron_id = in_patron_id AND successor IS NULL);
                BEGIN
                    IF EXISTS
                        (SELECT * FROM Access_Matrix
                            WHERE type = 'position'
                            AND name = position_title
                            AND method = in_method
                            AND active
                            AND black_list
                            AND entry_end_time IS NULL
                        ) THEN RETURN TRUE;
                    END IF;
                END;
            END IF;

            /* Check membership */
            /* todo */

            /* Check if non-member */
            /* todo */

            /* Check if everyone */
            /* todo */

        END;
    END IF;

    /* Default to false */
    RETURN FALSE;

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

/** Function to insert a new access rule
 *  @param session of current session
 *  @param in_active true to make this rule active as soon as it is created
 *  @param in_black_list true if blacklist otherwise whitelist
 *  @param type role type
 *  @param in_name name of role
 *  @param in_method method name to be invoked
 *  @return true if allowed, otherwise false
 */
CREATE OR REPLACE FUNCTION access_control_insert_rule(
    session SESSION_TYPE,
    in_active BOOLEAN,
    in_black_list BOOLEAN,
    in_type AE_ROLE_TYPE,
    in_name VARCHAR(128),
    in_method VARCHAR(200)
) RETURNS BOOLEAN AS $$
BEGIN

    /* Check session */
    IF NOT is_session_alive(session, FALSE) THEN RAISE EXCEPTION '{"code":"ERR_INVALID_SESSION", "message":"Session is invalid!"}'; END IF;

    /* Verify input */
    IF in_active IS NULL THEN RAISE EXCEPTION '{"code":"ERR_INVALID_ACCESS_ACTIVE", "message":"in_active is NULL!"}'; END IF;
    IF in_black_list IS NULL THEN RAISE EXCEPTION '{"code":"ERR_INVALID_ACCESS_BLACKLIST", "message":"in_black_list is NULL"}!'; END IF;
    IF in_type IS NULL THEN RAISE EXCEPTION '{"code":"ERR_INVALID_ACCESS_TYPE", "message":"in_type is NULL!"}'; END IF;
    IF in_method IS NULL THEN RAISE EXCEPTION '{"code":"ERR_INVALID_ACCESS_METHOD", "message":"in_method is NULL!"}'; END IF;
    IF RTRIM(in_method) LIKE '' THEN RAISE EXCEPTION '{"code":"ERR_INVALID_ACCESS_METHOD", "message":"in_method is EMPTY!"}'; END IF;
    IF in_name IS NOT NULL THEN
        IF RTRIM(in_name) LIKE '' THEN RAISE EXCEPTION '{"code":"ERR_INVALID_ACCESS_NAME", "message":"in_name is EMPTY!"}'; END IF;
    END IF;

    /* Get patron id from session */
    DECLARE
        patron_id INT := (SELECT id FROM identify_patron_from_session_internal(session));
        patron_hash PATRON_HASH_TYPE := (SELECT hash FROM identify_patron_from_session(session));
        name_patron_id INT := 0;
    BEGIN

        /* Check if user is allowed to insert a rule */
        IF NOT access_control_check_access(session, 'access_control_modify') THEN
            RETURN FALSE;
        END IF;

        /* Check if this rule does not already exist */
        IF in_name IS NULL THEN
            IF EXISTS (SELECT * FROM Access_Matrix WHERE black_list = in_black_list AND type = in_type AND method = RTRIM(in_method) AND entry_end_time IS NULL) THEN
                RAISE EXCEPTION '{"code":"ERR_ACCESS_RULE_EXISTS", "message":"Rule already exists!"}';
            END IF;
        ELSE
            IF in_type = 'patron' THEN
                IF EXISTS (SELECT * FROM Access_Matrix WHERE black_list = in_black_list AND type = in_type AND name = (SELECT id FROM get_patron_internal(RTRIM(in_name)::PATRON_HASH_TYPE))::VARCHAR AND method = RTRIM(in_method) AND entry_end_time IS NULL) THEN
                    RAISE EXCEPTION '{"code":"ERR_ACCESS_RULE_EXISTS", "message":"Rule already exists!"}';
                END IF;
            ELSE
                IF EXISTS (SELECT * FROM Access_Matrix WHERE black_list = in_black_list AND type = in_type AND name = RTRIM(in_name) AND method = RTRIM(in_method) AND entry_end_time IS NULL) THEN
                    RAISE EXCEPTION '{"code":"ERR_ACCESS_RULE_EXISTS", "message":"Rule already exists!"}';
                END IF;
            END IF;
        END IF;

        /* Check if type and name relation is valid */
        IF in_type = 'patron' THEN

            /* name cannot be NULL and id in the name must exist in Patron */
            IF in_name IS NULL THEN RAISE EXCEPTION '{"code":"ERR_INVALID_ACCESS_NAME_USE", "Message":"The patron in role type requires patron id in name value!"}'; END IF;

            IF NOT patron_exists((RTRIM(in_name))::PATRON_HASH_TYPE) THEN
                RAISE EXCEPTION '{"code":"ERR_USER_HASH_NOT_FOUND", "message":"Patron hash in the name value does not exist!"}';
            ELSE

                name_patron_id := (SELECT id FROM get_patron_internal((RTRIM(in_name))::PATRON_HASH_TYPE));

                IF name_patron_id = 1 THEN
                    RAISE EXCEPTION '{"code":"ERR_ACCESS_ADMIN_MOD", "message":"Not allowed to set rules regarding admin!"}';
                END IF;

            END IF;

        ELSIF in_type = 'position' THEN

            /* name cannot be NULL and hash in the name must exist in Executive_Board_Position */
           IF in_name IS NULL THEN RAISE EXCEPTION '{"code":"ERR_INVALID_ACCESS_NAME_USE", "message":"the position role type requires position name in name value!"}'; END IF;

           IF NOT EXISTS (SELECT * FROM Executive_Board_Position WHERE title = RTRIM(in_name)) THEN
               RAISE EXCEPTION '{"code":"ERR_INVALID_ACCESS_NAME_USE", "message":"position name in the name value does not exist!"}';
           END IF;

        ELSIF in_type = 'eb_members' THEN

            /* name needs to be NULL */
            IF in_name IS NOT NULL THEN
                RAISE EXCEPTION '{"code":"ERR_INVALID_ACCESS_NAME", "message":"the eb_members role type requires name value to be NULL!"}';
            END IF;

        ELSIF in_type = 'members' THEN

            /* name needs to be NULL */
            IF in_name IS NOT NULL THEN
                RAISE EXCEPTION '{"code":"ERR_INVALID_ACCESS_NAME", "message":"the members role type requires name value to be NULL!"}';
            END IF;

        ELSIF in_type = 'non_members' THEN

            /* name needs to be NULL */
            IF in_name IS NOT NULL THEN
                RAISE EXCEPTION '{"code":"ERR_INVALID_ACCESS_NAME", "message":"the non_members role type requires name value to be NULL!"}';
            END IF;

        ELSIF in_type = 'everyone' THEN

            /* name needs to be NULL */
            IF in_name IS NOT NULL THEN
                RAISE EXCEPTION '{"code":"ERR_INVALID_ACCESS_NAME", "message":"the everyone role type requires name value to be NULL!"}';
            END IF;

        ELSIF in_type = 'no_login' THEN

            /* name needs to be NULL */
            IF in_name IS NOT NULL THEN
                RAISE EXCEPTION '{"code":"ERR_INVALID_ACCESS_NAME", "message":"the no_login role type requires name value to be NULL!"}';
            END IF;

        ELSE

            /* Catch-all */
            RAISE EXCEPTION '{"code":"ERR_INVALID_ACCESS_TYPE", "message":"Incorrect role_type!"}';

        END IF;

        /* Insert the rule */
        IF name_patron_id > 0 THEN
            INSERT INTO Access_Matrix (active, black_list, type, name, method, entry_author)
                VALUES (in_active, in_black_list, in_type, name_patron_id::text, RTRIM(in_method), patron_id);
        ELSE
            INSERT INTO Access_Matrix (active, black_list, type, name, method, entry_author)
                VALUES (in_active, in_black_list, in_type, RTRIM(in_name), RTRIM(in_method), patron_id);
        END IF;

    END;

    /* Refresh the session expiration date */
    IF is_session_alive(session, TRUE) THEN END IF; /* do nothing */

    RETURN TRUE;

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

/** Function to toggle a rule
 *  @param session Current login session
 *  @param in_id rule to be toggled
 *  @return true if allowed, otherwise false
 */
CREATE OR REPLACE FUNCTION access_control_toggle_rule(
    session SESSION_TYPE,
    in_id INT
) RETURNS BOOLEAN AS $$
BEGIN

    /* Check if user is allowed to modify a rule */
    IF NOT access_control_check_access(session, 'access_control_modify') THEN
        RETURN FALSE;
    END IF;

    /* Check if id and in_active are not NULL */
    IF in_id IS NULL THEN RAISE EXCEPTION '{"code":"ERR_INVALID_ACCESS_ID", "message":"in_id is NULL!"}'; END IF;

    /* Check if rule exists */
    IF NOT EXISTS (SELECT * FROM Access_Matrix WHERE id = in_id AND entry_end_time IS NULL AND NOT (type = 'patron' AND name = '1')) THEN
        RAISE EXCEPTION '{"code":"ERR_ACCESS_ID_NOT_EXIST", "message":"rule is either not toggleable or does not exist!"}';
    END IF;

    /* Get patron id from session */
    DECLARE
        patron_id INT := (SELECT id FROM identify_patron_from_session_internal(session));
        patron_hash PATRON_HASH_TYPE := (SELECT hash FROM identify_patron_from_session(session));
        right_now TIMESTAMP = NOW();
    BEGIN

        /* Update the current rule to "expire it" */
        UPDATE Access_Matrix SET entry_end_time = right_now WHERE id = in_id AND entry_end_time IS NULL;

        /* Add new rule */
        INSERT INTO Access_Matrix (id, active, black_list, type, name, method, entry_author)
            WITH helper AS (SELECT * FROM Access_Matrix WHERE id = in_id AND entry_end_time = right_now)
            SELECT helper.id, NOT helper.active, helper.black_list, helper.type, helper.name, helper.method, patron_id FROM helper;
    END;

    /* Refresh the session expiration date */
    IF is_session_alive(session, TRUE) THEN END IF; /* do nothing */

    RETURN TRUE;

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

/** Helper function to indicate if user has a permission to view the list
 *  @param session Current login session
 *  @return true if allowed, otherwise false
 */
CREATE OR REPLACE FUNCTION access_control_list_view_permission(
    session SESSION_TYPE
) RETURNS BOOLEAN AS $$
BEGIN

    IF access_control_check_access(session, 'access_control_modify') THEN RETURN TRUE; END IF;

    /* Refresh the session expiration date */
    IF is_session_alive(session, TRUE) THEN END IF; /* do nothing */

    RETURN access_control_check_access(session, 'access_control_view');
END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

/** Function to return the access matrix
 *  @param session Current login session
 *  @param old true to include old entries, otherwise false to show current entries
 *  @param page page number
 *  @return table
 */
CREATE OR REPLACE FUNCTION access_control_get_list(
    session SESSION_TYPE,
    old BOOLEAN,
    page INT
) RETURNS TABLE(id INT, active BOOLEAN, black_list BOOLEAN, type AE_ROLE_TYPE, hash VARCHAR(64), method VARCHAR(200), entry_start_time TIMESTAMP, entry_end_time TIMESTAMP, entry_author VARCHAR(100)) AS $$
BEGIN

    IF old IS NULL THEN RAISE EXCEPTION '{"code":"ERR_INVALID_ACCESS_PARAM", "message":"old is NULL!"}'; END IF;
    IF page IS NULL THEN RAISE EXCEPTION '{"code":"ERR_INVALID_PAGE", "message":"page is NULL!"}'; END IF;

    /* check if page is valid */
    IF page < 1 THEN
        RAISE EXCEPTION '{"code":"ERR_PAGE_NUMBER_OUT_RANGE", "message":"page is is out of range! Page number: %"}', page;
    END IF;

    /* Check permission */
    IF NOT access_control_list_view_permission(session) THEN RAISE EXCEPTION '{"code":"ERR_NOT_PERMITTED", "message":"User is not allowed to view the list!"}'; END IF;

    /* Refresh the session expiration date */
    SELECT is_session_alive(session, TRUE); /* do nothing */

    /* Return the table with all patron ids replaced to patron names */
    RETURN QUERY
    (
        SELECT
            cleaned.id,
            cleaned.active,
            cleaned.black_list,
            cleaned.type,
            cleaned.hash,
            cleaned.method,
            cleaned.entry_start_time,
            cleaned.entry_end_time,
            (SELECT auth.hash FROM get_patron_by_id(cleaned.entry_author) auth)
        FROM (
            SELECT
                am.id,
                am.active,
                am.black_list,
                am.type,
                (SELECT res.hash FROM get_patron_by_id(am.name::int) res),
                am.method,
                am.entry_start_time,
                am.entry_end_time,
                am.entry_author
            FROM Access_Matrix am WHERE am.type = 'patron' AND (old OR am.entry_end_time IS NULL)
            UNION ALL
            SELECT * FROM Access_Matrix am1 WHERE NOT am1.type = 'patron' AND (old OR am1.entry_end_time IS NOT NULL)
        ) AS cleaned ORDER BY cleaned.entry_start_time DESC LIMIT GLOBAL_access_matrix_element_limit() OFFSET (GLOBAL_access_matrix_element_limit() * (page - 1))
    );
END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;
