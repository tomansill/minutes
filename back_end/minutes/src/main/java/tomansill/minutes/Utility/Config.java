package tomansill.minutes;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import org.json.simple.JSONObject;
import java.io.FileReader;
import java.io.Writer;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;

/** Server Config
 *  Holds configuration information and make themselves available over all modules
 *  @author Thomas Ansill
 */
public class Config{

    public static Config instance = null;

    /** database connection */
    public DatabaseConnection db = null;

    /** debug level */
    public int debug_level = 0; // Perhaps another class just for debugging? placeholder for now

    /** server port number */
    public int port = -1;

    /** web server root directory */
    public File root = null;

    /** Constructor */
    public Config(){}

    /** Creates a default config
     *  @throws IOException thrown if there's any problems with disk writing
     */
    public static void createDefaultConfig() throws IOException{
        // The config json
        JSONObject config_json = new JSONObject();

        // Server config
        JSONObject server_json = new JSONObject();
        server_json.put("port", "8080");
        server_json.put("root", "./");
        config_json.put("server", server_json);

        // Database config
        JSONObject database_json = new JSONObject();
        database_json.put("hostname", "localhost");
        database_json.put("database", ".");
        database_json.put("user", "user");
        database_json.put("password", "password");
        config_json.put("database", database_json);

        // Debug config
        JSONObject debug_json = new JSONObject();
        debug_json.put("debug_level", "0"); // 0 - severe, 1 - warning, 2 - info
        debug_json.put("debug_output", "sql"); //sql, stdout, logger
        config_json.put("debug", debug_json);

        // Logger config
        JSONObject logger_json = new JSONObject();
        logger_json.put("directory", "minutes.log");
        logger_json.put("log_level", "0");
        config_json.put("logger", logger_json);

        // Access Activity
        JSONObject access_json = new JSONObject();
        access_json.put("access_activity_output", "none"); //none, sql, stdout, logger
        config_json.put("access_activity", access_json);

        // Write the config
        Writer writer = null;
        writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File("config.json")), "utf-8"));
        writer.write(Utility.encodeJSON(config_json));
        writer.flush();
    }

    /** Reads config from the file and create the config object
     *  @param  path_to_config file path to config file
     *  @return Config
     *  @throws IOException thrown if there's any problems with disk reading
     */
    public static Config loadConfig(String path_to_config) throws IOException{
        return loadConfig(new File(path_to_config));
    }

    /** Reads config from the file and create the config object
     *  @param  path_to_config file path to config file
     *  @return Config
     *  @throws IOException thrown if there's any problems with disk reading
     */
    public static Config loadConfig(File path_to_config) throws IOException{

        // Read the config
        JSONObject config_json = Utility.decodeJSON(new FileReader(path_to_config));

        // Create new config
        Config cfg = new Config();

        // Create a database connection (does not connect yet)
        JSONObject database_config = (JSONObject) config_json.get("database");
        cfg.db = new DatabaseConnection((String) database_config.get("hostname"),
                                        (String) database_config.get("database"),
                                        (String) database_config.get("user"),
                                        (String) database_config.get("password"));

        // Load entries into the config
        JSONObject server_config = (JSONObject) config_json.get("server");
        cfg.port = Integer.parseInt((String)server_config.get("port"));
        cfg.root = new File((String)server_config.get("root"));
        cfg.debug_level = Integer.parseInt((String)((JSONObject)config_json.get("debug")).get("debug_level"));

        // Return it
        Config.instance = cfg;
        return cfg;
    }

    public static Config getInstance() throws InstantiationException{
        if(Config.instance == null) throw new InstantiationException("Config is not instantiated. Run loadConfig(File) to initialize it!");
        return Config.instance;
    }
}
