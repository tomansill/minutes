package tomansill.minutes;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Date;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

/** A bundle of functions that handles database functions to User functions
 *  @author Thomas Ansill
 */
public class UserDB{

    /** Server config */
    private Config server_config;

    /** Constuctor */
    public UserDB(Config server_config){
        this.server_config = server_config;
    }

    public JSONObject getUserIdentificationPrivacy(String session_hash, String user_hash) throws SQLException{

        // Check if Server config is initialized. It must be initialized to use the database
        if(server_config == null){
            System.err.println("ERROR: The server config is not initialized.");
            throw new SQLException("ERROR: The server config is not initialized. Meaning there's no database connection!");
        }

        // Check input
        if(user_hash == null || user_hash.trim().equals("")) return null;

        // Create the object
        JSONObject output = null;

        // Execute SQL query
        PreparedStatement stmt = server_config.db.connection.prepareStatement("SELECT get_patron_information_privacy(?::SESSION_TYPE,?::PATRON_HASH_TYPE)");
        stmt.setString(1, session_hash);
        stmt.setString(2, user_hash);
        ResultSet rs = stmt.executeQuery();

        // Get the result - if exception is thrown for empty rs, then database has issues, not this
        if(rs.next()){
            output = new JSONObject();
            output.put("private_name", rs.getBoolean(1));
            output.put("private_middle_name", rs.getBoolean(1));
            output.put("private_birth_date", rs.getBoolean(1));
        }

        // Clean up
        rs.close();
        stmt.close();

        // Return the object, initialized or null
        return output;
    }

    public boolean updateUserIdentification(
        String session_hash,
        String user_hash,
        String title,
        String first_name,
        String middle_name,
        String last_name,
        String nick_name,
        String suffix,
        java.util.Date birth_date,
        Boolean private_name,
        Boolean private_middle_name,
        Boolean private_birth_date
    ) throws SQLException{

        // Check if Server config is initialized. It must be initialized to use the database
        if(server_config == null){
            System.err.println("ERROR: The server config is not initialized.");
            throw new SQLException("ERROR: The server config is not initialized. Meaning there's no database connection!");
        }

        // Check input
        if(user_hash == null || user_hash.trim().equals("")) return false;

        // Create the object
        boolean output = false;

        // Execute SQL query
        PreparedStatement stmt = server_config.db.connection.prepareStatement("SELECT update_patron_identification(?::SESSION_TYPE,?::PATRON_HASH_TYPE,"
        + "?::TITLE, ?::VARCHAR, ?::VARCHAR, ?::VARCHAR, ?::VARCHAR, ?::SUFFIX, ?::DATE, ?::BOOLEAN, ?::BOOLEAN, ?::BOOLEAN)");
        stmt.setString(1, session_hash);
        stmt.setString(2, user_hash);
        stmt.setString(3, title);
        stmt.setString(4, first_name);
        stmt.setString(5, middle_name);
        stmt.setString(6, last_name);
        stmt.setString(7, nick_name);
        stmt.setString(8, suffix);
        if(birth_date == null) stmt.setString(9, null);
        else stmt.setString(9, new java.sql.Date(birth_date.getTime()).toString());
        if(private_name == null) stmt.setString(10, null);
        else stmt.setBoolean(10, private_name);
        if(private_middle_name == null) stmt.setString(11, null);
        else stmt.setBoolean(11, private_middle_name);
        if(private_birth_date == null) stmt.setString(12, null);
        else stmt.setBoolean(12, private_birth_date);
        System.out.println(stmt);
        ResultSet rs = stmt.executeQuery();

        // Get the result
        if(rs.next()){
            output = rs.getBoolean(1);
        }

        // Clean up
        rs.close();
        stmt.close();

        // Return the object, initialized or null
        return output;
    }

    public JSONObject getUserIdentificationPrivileges(String session_hash, String user_hash) throws SQLException{

        // Check if Server config is initialized. It must be initialized to use the database
        if(server_config == null){
            System.err.println("ERROR: The server config is not initialized.");
            throw new SQLException("ERROR: The server config is not initialized. Meaning there's no database connection!");
        }

        // Check input
        if(user_hash == null || user_hash.trim().equals("")) return null;

        // Create the object
        JSONObject output = new JSONObject();

        // Execute SQL query
        PreparedStatement stmt = server_config.db.connection.prepareStatement("SELECT get_patron_information_access(?::SESSION_TYPE,?::PATRON_HASH_TYPE, 'PII_name'::VARCHAR)");
        stmt.setString(1, session_hash);
        stmt.setString(2, user_hash);
        ResultSet rs = stmt.executeQuery();

        // Get the result
        if(rs.next()){
            output.put("name", rs.getString(1));
        }

        // Clean up
        rs.close();
        stmt.close();

        // Execute SQL query
        stmt = server_config.db.connection.prepareStatement("SELECT get_patron_information_access(?::SESSION_TYPE,?::PATRON_HASH_TYPE, 'PII_middle_name'::VARCHAR)");
        stmt.setString(1, session_hash);
        stmt.setString(2, user_hash);
        rs = stmt.executeQuery();

        // Get the result
        if(rs.next()){
            output.put("middle_name", rs.getString(1));
        }

        // Clean up
        rs.close();
        stmt.close();

        // Execute SQL query
        stmt = server_config.db.connection.prepareStatement("SELECT get_patron_information_access(?::SESSION_TYPE,?::PATRON_HASH_TYPE, 'PII_birth_date'::VARCHAR)");
        stmt.setString(1, session_hash);
        stmt.setString(2, user_hash);
        rs = stmt.executeQuery();

        // Get the result
        if(rs.next()){
            output.put("birth_date", rs.getString(1));
        }

        // Clean up
        rs.close();
        stmt.close();

        // Execute SQL query
        stmt = server_config.db.connection.prepareStatement("SELECT get_patron_information_write(?::SESSION_TYPE,?::PATRON_HASH_TYPE, 'PII_write'::VARCHAR)");
        stmt.setString(1, session_hash);
        stmt.setString(2, user_hash);
        rs = stmt.executeQuery();

        // Get the result
        if(rs.next()){
            output.put("write", rs.getBoolean(1));
        }

        // Clean up
        rs.close();
        stmt.close();

        // Return the object, initialized or null
        return output;
    }

    public JSONArray getUserEmailAddress(String session_hash, String user_hash) throws SQLException{

        // Check if Server config is initialized. It must be initialized to use the database
        if(server_config == null){
            System.err.println("ERROR: The server config is not initialized.");
            throw new SQLException("ERROR: The server config is not initialized. Meaning there's no database connection!");
        }

        // Check input
        if(user_hash == null || user_hash.trim().equals("")) return null;

        // Create the object
        JSONArray output = null;

        // Execute SQL query
        PreparedStatement stmt = server_config.db.connection.prepareStatement("SELECT * FROM get_patron_information_email_address(?::SESSION_TYPE,?::PATRON_HASH_TYPE)");
        stmt.setString(1, session_hash);
        stmt.setString(2, user_hash);
        ResultSet rs = stmt.executeQuery();

        // Get the result
        while(rs.next()){
            JSONObject obj = new JSONObject();
            obj.put("address", rs.getString(1));
            obj.put("status", rs.getString(2));
            output.add(obj);
        }

        // Clean up
        rs.close();
        stmt.close();

        // Return the object, initialized or null
        return output;
    }

    public Date getUserBirthDate(String session_hash, String user_hash) throws SQLException{

        // Check if Server config is initialized. It must be initialized to use the database
        if(server_config == null){
            System.err.println("ERROR: The server config is not initialized.");
            throw new SQLException("ERROR: The server config is not initialized. Meaning there's no database connection!");
        }

        // Check input
        if(user_hash == null || user_hash.trim().equals("")) return null;

        // Create the object
        Date output = null;

        // Execute SQL query
        PreparedStatement stmt = server_config.db.connection.prepareStatement("SELECT get_patron_information_birth_date(?::SESSION_TYPE,?::PATRON_HASH_TYPE)");
        stmt.setString(1, session_hash);
        stmt.setString(2, user_hash);
        ResultSet rs = stmt.executeQuery();

        // Get the result - if exception is thrown for empty rs, then database has issues, not this
        if(rs.next()){
            output = rs.getDate(1);
        }

        // Clean up
        rs.close();
        stmt.close();

        // Return the object, initialized or null
        return output;
    }

    public Date getUserJoinedDate(String user_hash) throws SQLException{

        // Check if Server config is initialized. It must be initialized to use the database
        if(server_config == null){
            System.err.println("ERROR: The server config is not initialized.");
            throw new SQLException("ERROR: The server config is not initialized. Meaning there's no database connection!");
        }

        // Check input
        if(user_hash == null || user_hash.trim().equals("")) return null;

        // Create the object
        Date output = null;

        // Execute SQL query
        PreparedStatement stmt = server_config.db.connection.prepareStatement("SELECT get_patron_date_joined(?::PATRON_HASH_TYPE)");
        stmt.setString(1, user_hash);
        ResultSet rs = stmt.executeQuery();

        // Get the result - if exception is thrown for empty rs, then database has issues, not this
        if(rs.next()){
            output = rs.getDate(1);
        }

        // Clean up
        rs.close();
        stmt.close();

        // Return the object, initialized or null
        return output;
    }

    public JSONObject getUserNames(String session_hash, String user_hash) throws SQLException{

        // Check if Server config is initialized. It must be initialized to use the database
        if(server_config == null){
            System.err.println("ERROR: The server config is not initialized.");
            throw new SQLException("ERROR: The server config is not initialized. Meaning there's no database connection!");
        }

        // Check input
        if(user_hash == null || user_hash.trim().equals("")) return null;

        // Create the object
        JSONObject output = null;

        // Execute SQL query
        PreparedStatement stmt = server_config.db.connection.prepareStatement("SELECT * FROM get_patron_information_name(?::SESSION_TYPE,?::PATRON_HASH_TYPE)");
        stmt.setString(1, session_hash);
        stmt.setString(2, user_hash);
        ResultSet rs = stmt.executeQuery();

        // Get the result - if exception is thrown for empty rs, then database has issues, not this
        if(rs.next()){
            // Build output object
            output = new JSONObject();
            output.put("title", rs.getString(1));
            output.put("first_name", rs.getString(2));
            output.put("middle_name", rs.getString(3));
            output.put("last_name", rs.getString(4));
            output.put("nick_name", rs.getString(5));
            output.put("suffix", rs.getString(6));
        }

        // Clean up
        rs.close();
        stmt.close();

        // Return the object, initialized or null
        return output;
    }

    /** Function to check if user's password is valid without authenication
     *  @param email_address user's email address
     *  @param password password
     *  @return true if valid, otherwise false
     *  @throws SQLException thrown if there's a problem with the database
     */
    public boolean checkUserPassword(String email_address, String password)  throws SQLException{

        // Check if Server config is initialized. It must be initialized to use the database
        if(server_config == null){
            System.err.println("ERROR: The server config is not initialized.");
            throw new SQLException("ERROR: The server config is not initialized. Meaning there's no database connection!");
        }

        // Verify inputs
        if(email_address == null) return false;
        if(password == null) return false;

        boolean answer = false;

        // Execute SQL query
        PreparedStatement stmt = server_config.db.connection.prepareStatement("SELECT check_patron_password(?::EMAIL_ADDRESS_TYPE,?)");
        stmt.setString(1, email_address);
        stmt.setString(2, password);
        ResultSet rs = stmt.executeQuery();

        // Get the result - if exception is thrown for empty rs, then database has issues, not this
        rs.next();
        answer = rs.getBoolean(1);

        // Clean up
        rs.close();
        stmt.close();

        return answer;
    }

    /** Function to change user's password
     *  @param session_hash session hash
     *  @param email_address user's email_address
     *  @param password password
     *  @return true if valid, otherwise false
     *  @throws SQLException thrown if there's a problem with the database
     */
    public boolean changeUserPassword(String session_hash, String email_address, String password) throws SQLException{

        // Check if Server config is initialized. It must be initialized to use the database
        if(server_config == null){
            System.err.println("ERROR: The server config is not initialized.");
            throw new SQLException("ERROR: The server config is not initialized. Meaning there's no database connection!");
        }

        // Verify inputs
        if(session_hash == null) return false;
        if(email_address == null) return false;
        if(password == null) return false;

        // Execute SQL query
        PreparedStatement stmt = server_config.db.connection.prepareStatement("SELECT change_patron_password(?::SESSION_TYPE,?::EMAIL_ADDRESS_TYPE,?::VARCHAR)");
        stmt.setString(1, session_hash);
        stmt.setString(2, email_address);
        stmt.setString(3, password);
        ResultSet rs = stmt.executeQuery();

        boolean answer = false;

        // Get the result - if exception is thrown for empty rs, then database has issues, not this
        rs.next();
        answer = rs.getBoolean(1);

        // Clean up
        rs.close();
        stmt.close();

        return answer;
    }

    /** Function to check if user with email_address exists in the system
     *  @param email_address user's email_address
     *  @return true if exist, otherwise false
     *  @throws SQLException thrown if there's a problem with the database
     */
    public boolean userExistsEmailAddress(String email_address)  throws SQLException{

        // Check if Server config is initialized. It must be initialized to use the database
        if(server_config == null){
            System.err.println("ERROR: The server config is not initialized.");
            throw new SQLException("ERROR: The server config is not initialized. Meaning there's no database connection!");
        }

        // Verify inputs
        if(email_address == null) return false;
        if(email_address.trim().equals("")) return false;

        boolean answer = false;

        // Execute SQL query
        PreparedStatement stmt = server_config.db.connection.prepareStatement("SELECT patron_exists(?::EMAIL_ADDRESS_TYPE)");
        stmt.setString(1, email_address);
        ResultSet rs = stmt.executeQuery();

        // Get the result - if exception is thrown for empty rs, then database has issues, not this
        rs.next();
        answer = rs.getBoolean(1);

        // Clean up
        rs.close();
        stmt.close();

        return answer;
    }

    /** Function to check if user with user hash exists in the system
     *  @param username user's hash
     *  @return true if exist, otherwise false
     *  @throws SQLException thrown if there's a problem with the database
     */
    public boolean userHashExists(String user_hash) throws SQLException{

        // Check if Server config is initialized. It must be initialized to use the database
        if(server_config == null){
            System.err.println("ERROR: The server config is not initialized.");
            throw new SQLException("ERROR: The server config is not initialized. Meaning there's no database connection!");
        }

        // Verify inputs
        if(user_hash == null) return false;
        if(user_hash.trim().equals("")) return false;

        boolean answer = false;

        // Execute SQL query
        PreparedStatement stmt = server_config.db.connection.prepareStatement("SELECT patron_exists(?::PATRON_HASH_TYPE)");
        stmt.setString(1, user_hash);
        ResultSet rs = stmt.executeQuery();

        // Get the result - if exception is thrown for empty rs, then database has issues, not this
        rs.next();
        answer = rs.getBoolean(1);

        // Clean up
        rs.close();
        stmt.close();

        return answer;
    }

    /** Function to register an user
     *  @param email_address user's email_address
     *  @param password password
     *  @param title user's name title
     *  @param first_name user's first name
     *  @param middle_name user's middle name
     *  @param last_name user's last name
     *  @param nick_name user's nick name
     *  @param suffix user's suffix
     *  @param birth_date user's birthday date
     *  @return true if successful, otherwise false if unsuccessful
     *  @throws SQLException thrown if there's a problem with the database
     */
    public boolean register(String email_address, String password, String title, String first_name, String middle_name, String last_name, String nick_name, String suffix, java.util.Date birth_date)  throws SQLException{

        // Check if Server config is initialized. It must be initialized to use the database
        if(server_config == null){
            System.err.println("ERROR: The server config is not initialized.");
            throw new SQLException("ERROR: The server config is not initialized. Meaning there's no database connection!");
        }

        boolean answer = false;

        // Execute SQL query
        PreparedStatement stmt = server_config.db.connection.prepareStatement("SELECT create_patron(?,?,?,?,?,?,?,?,?::DATE)");
        stmt.setString(1, email_address);
        stmt.setString(2, password);
        stmt.setString(3, title);
        stmt.setString(4, first_name);
        stmt.setString(5, middle_name);
        stmt.setString(6, last_name);
        stmt.setString(7, nick_name);
        stmt.setString(8, suffix);
        stmt.setString(9, new java.sql.Date(birth_date.getTime()).toString());
        System.out.println(stmt);
        ResultSet rs = stmt.executeQuery();

        // Get the result - if exception is thrown for empty rs, then database has issues, not this
        rs.next();
        answer = rs.getBoolean(1);

        // Clean up
        rs.close();
        stmt.close();

        return answer;
    }

    /** Function to get user's hash with username
     *  @param email_address user's email_address
     *  @return hash, null if username is not found
     *  @throws SQLException thrown if there's a problem with the database
     */
    public String getUserHash(String email_address)  throws SQLException{

        // Check if Server config is initialized. It must be initialized to use the database
        if(server_config == null){
            System.err.println("ERROR: The server config is not initialized.");
            throw new SQLException("ERROR: The server config is not initialized. Meaning there's no database connection!");
        }

        // Verify input
        if(email_address == null) return null;

        String answer = null;

        // Execute SQL query
        PreparedStatement stmt = server_config.db.connection.prepareStatement("SELECT hash FROM get_patron(?::EMAIL_ADDRESS_TYPE)");
        stmt.setString(1, email_address);
        ResultSet rs = stmt.executeQuery();

        // Get the result - if exception is thrown for empty rs, then database has issues, not this
        rs.next();
        answer = rs.getString(1);

        // Clean up
        rs.close();
        stmt.close();

        return answer;
    }

    /** Function to further validate user's session with 2nd factor authenication
     *  @param email_address user's email_address
     *  @return hash, null if username is not found
     *  @throws SQLException thrown if there's a problem with the database
     */
    public boolean twoFactorAuthenication(String session, String code)  throws SQLException{

        // Check if Server config is initialized. It must be initialized to use the database
        if(server_config == null){
            System.err.println("ERROR: The server config is not initialized.");
            throw new SQLException("ERROR: The server config is not initialized. Meaning there's no database connection!");
        }

        // Verify input
        if(session == null) return false;
        if(code == null) return false;

        boolean answer = false;

        // Execute SQL query
        PreparedStatement stmt = server_config.db.connection.prepareStatement("SELECT authenicate_patron_2FA(?::SESSION_TYPE, ?::VARCHAR)");
        stmt.setString(1, session);
        stmt.setString(2, code);
        System.out.println(stmt);
        ResultSet rs = stmt.executeQuery();

        // Get the result - if exception is thrown for empty rs, then database has issues, not this
        rs.next();
        answer = rs.getBoolean(1);

        System.out.println("Answer: " + answer);

        // Clean up
        rs.close();
        stmt.close();

        return answer;
    }
}
