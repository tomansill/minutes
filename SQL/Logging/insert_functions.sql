CREATE OR REPLACE FUNCTION get_or_insert_agent(in_ip_address VARCHAR(39), in_user_agent TEXT) RETURNS INT AS $$
BEGIN
    DECLARE
        agent_id INT := (SELECT id FROM Access_Agent WHERE ip_address = RTRIM(in_ip_address) AND user_agent = RTRIM(in_user_agent));
    BEGIN
        IF agent_id IS NULL THEN
            INSERT INTO Access_Agent (ip_address, user_agent) VALUES (RTRIM(in_ip_address), RTRIM(in_user_agent)) RETURNING Access_Agent.id INTO agent_id;

            /* Update Statistics */
            UPDATE Statistics SET value = value + 1 WHERE key = 'Access_Agent_Total_Count';
        ELSE
            UPDATE Access_Agent SET count_requests = count_requests + 1 WHERE id = agent_id;
        END IF;

        RETURN agent_id;
    END;
END; $$
LANGUAGE PLPGSQL SECURITY INVOKER;

CREATE OR REPLACE FUNCTION get_or_insert_resource(in_url TEXT, in_method VARCHAR(10)) RETURNS INT AS $$
BEGIN
    DECLARE
        resource_id INT := (SELECT id FROM Access_Resource WHERE url = LOWER(RTRIM(in_url)) AND method = UPPER(RTRIM(in_method)));
    BEGIN
        IF resource_id IS NULL THEN
            INSERT INTO Access_Resource (url, method) VALUES (LOWER(RTRIM(in_url)), UPPER(RTRIM(in_method))) RETURNING Access_Resource.id INTO resource_id;

            /* Update Statistics */
            UPDATE Statistics SET value = value + 1 WHERE key = 'Access_Resource_Total_Count';
        ELSE
            UPDATE Access_Resource SET count_accessed = count_accessed + 1 WHERE id = resource_id;
        END IF;

        RETURN resource_id;
    END;
END; $$
LANGUAGE PLPGSQL SECURITY INVOKER;

CREATE OR REPLACE FUNCTION get_or_insert_known_associations(in_agent_id INT, in_patron_id INT) RETURNS BOOLEAN AS $$
BEGIN
        IF in_agent_id IS NULL OR in_patron_id IS NULL OR NOT EXISTS (SELECT * FROM Access_Agent WHERE id = in_agent_id) OR NOT EXISTS (SELECT * FROM Patron WHERE id = in_patron_id) THEN
            RETURN FALSE;
        ELSE
            IF NOT EXISTS (SELECT * FROM Known_Association WHERE agent_id = in_agent_id AND patron_id = in_patron_id) THEN
                INSERT INTO Known_Association (agent_id, patron_id) VALUES (in_agent_id, in_patron_id);
            END IF;

            RETURN TRUE;
        END IF;
END; $$
LANGUAGE PLPGSQL SECURITY INVOKER;

CREATE OR REPLACE FUNCTION insert_access_log(ip_address VARCHAR(39), user_agent TEXT, url TEXT, method VARCHAR(10), content TEXT, session SESSION_TYPE) RETURNS INT AS $$
BEGIN

    /* Do some house cleaning */
    DECLARE
        size_limit INT := (SELECT value FROM Properties WHERE key = 'Access_Log_Space_Limit')::INT;
        actual_size INT := 0;
        date_age_off INTERVAL;
    BEGIN
        /* Age off */
        DELETE FROM Access_Log WHERE AGE(NOW(), date_logged) > date_age_off;

        /* Check if read from Properties was successful */
        IF size_limit IS NULL THEN size_limit := 10000; END IF; /* 10 MB */

        /* If that was not good enough, delete more */
        actual_size := (SELECT size FROM get_table_space_usage() WHERE table_name = 'access_log');
        IF size_limit < actual_size THEN
            DELETE FROM Access_Log WHERE id IN (SELECT Access_Log.id FROM Access_Log ORDER BY date_age_off ASC LIMIT 100);
        END IF;
    END;

    /* Insert */
    DECLARE
        in_agent_id INT := (SELECT get_or_insert_agent(ip_address, user_agent));
        in_resource_id INT := (SELECT get_or_insert_resource(url, method));
        known_patron_id INT := (SELECT id FROM identify_patron_from_session_internal(session));
        log_id INT;
    BEGIN
        IF known_patron_id IS NOT NULL AND in_agent_id IS NOT NULL THEN
            IF get_or_insert_known_associations(in_agent_id, known_patron_id) THEN END IF;
        END IF;

        INSERT INTO Access_Log (agent_id, resource_id, content, patron_id) VALUES (in_agent_id, in_resource_id, content, known_patron_id) RETURNING Access_Log.id INTO log_id;

        /* Update Statistics */
        UPDATE Statistics SET value = value + 1 WHERE key = 'Access_Log_Total_Count';

        RETURN log_id;
    END;
END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

CREATE OR REPLACE FUNCTION insert_error_log(in_level ERROR_SEVERITY, in_message TEXT, in_description TEXT, in_caused_by INT) RETURNS INT AS $$
BEGIN

    /* Do some house cleaning */
    DECLARE
        size_limit INT := (SELECT value FROM Properties WHERE key = 'Error_Log_Space_Limit')::INT;
        actual_size INT := 0;
        date_age_off INTERVAL;
    BEGIN
        /* Age off */
        DELETE FROM Error_Log WHERE AGE(NOW(), date_logged) > date_age_off;

        /* Check if read from Properties was successful */
        IF size_limit IS NULL THEN size_limit := 10000; END IF; /* 10 MB */

        /* If that was not good enough, delete more */
        actual_size := (SELECT size FROM get_table_space_usage() WHERE table_name = 'error_log');
        IF size_limit < actual_size THEN
            DELETE FROM Error_Log WHERE id IN (SELECT Error_Log.id FROM Error_Log ORDER BY date_age_off ASC LIMIT 100);
        END IF;
    END;

    DECLARE
        error_id INT;
    BEGIN
        INSERT INTO Error_Log (level, caused_by, message, description) VALUES (in_level, in_caused_by, in_message, in_description) RETURNING Error_Log.id INTO error_id;
        RETURN error_id;
    END;
END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

CREATE OR REPLACE FUNCTION update_access_log(access_log_id INT, in_status_code INT) RETURNS VOID AS $$
BEGIN
    UPDATE Access_Log SET status_code = in_status_code WHERE id = access_log_id;
END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;
