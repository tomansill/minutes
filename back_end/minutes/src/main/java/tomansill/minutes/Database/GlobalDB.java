package tomansill.minutes;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/** A bundle of functions that handles database functions to Global functions
 *  @author Thomas Ansill
 */
public class GlobalDB{

    /** Server config */
    private Config server_config;

    /** Constuctor */
    public GlobalDB(Config server_config){
        this.server_config = server_config;
    }

    /** Function to connect to the database to check if input string is a valid
     *  name title

     *  @param title name title
     *  @return true if valid, otherwise false
     *  @throws SQLException thrown if there's a problem with database
     */
    public boolean isTitleValid(String title) throws SQLException{

        // Check if Server config is initialized. It must be initialized to use the database
        if(server_config == null){
            System.err.println("ERROR: The server config is not initialized.");
            throw new SQLException("ERROR: The server config is not initialized. Meaning there's no database connection!");
        }

        // Verify input
        if(title == null) return false;

        boolean answer = false;

        // Execute SQL query
        PreparedStatement stmt = server_config.db.connection.prepareStatement("SELECT is_title_valid(?)");
        stmt.setString(1, title);
        ResultSet rs = stmt.executeQuery();

        // Get the result - if exception is thrown for empty rs, then database has issues, not this
        rs.next();
        answer = rs.getBoolean(1);

        // Clean up
        rs.close();
        stmt.close();

        return answer;
    }

    /** Function to connect to the database to check if input string is a valid
     *  name suffix

     *  @param title name suffix
     *  @return true if valid, otherwise false
     *  @throws SQLException thrown if there's a problem with database
     */
    public boolean isSuffixValid(String suffix) throws SQLException{

        // Check if Server config is initialized. It must be initialized to use the database
        if(server_config == null){
            System.err.println("ERROR: The server config is not initialized.");
            throw new SQLException("ERROR: The server config is not initialized. Meaning there's no database connection!");
        }

        if(suffix == null) return true; // null is acceptable

        boolean answer = false;

        // Execute SQL query
        PreparedStatement stmt = server_config.db.connection.prepareStatement("SELECT is_suffix_valid(?)");
        stmt.setString(1, suffix);
        ResultSet rs = stmt.executeQuery();

        // Get the result - if exception is thrown for empty rs, then database has issues, not this
        rs.next();
        answer = rs.getBoolean(1);

        System.out.println("suffix: " + answer);

        // Clean up
        rs.close();
        stmt.close();

        return answer;
    }
}
