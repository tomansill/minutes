package tomansill.minutes;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.nio.file.Path;
import java.io.File;

import java.util.Map;
import java.util.List;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

import java.io.IOException;

import java.util.regex.Pattern;

import javax.servlet.http.Cookie;

/** Class that services or route requests that relates to an User
 *  @author Thomas Ansill
 */
public class UserOptionAPI{

    /** Server config */
    private Config server_config;

    /** Database object */
    private Database db;

    /** Constuctor */
    public UserOptionAPI(Config server_config) throws Exception{
        this.server_config = server_config;
        this.db = Database.getInstance();
    }

    /** Function that services the HTTP request and possibly route them further
     *  @param path URL path
     *  @param request HTTP request
     *  @param response HTTP response to respond to the user
     */
    public void processRequest(PathList path, HttpServletRequest request, HttpServletResponse response) throws Exception{

        // Get the hash
        String user_hash = null;

        // If user hash is present in the path...
        if(path.firstMatches("^([a-f]|[A-F]|[0-9]){64}")){

            // Check if that hash exists at all
            if(!this.db.user.userHashExists(path.peek())){
                response.sendError(HttpServletResponse.SC_NOT_FOUND, "The user hash does not exist on the server.");
                return;
            }

            // Get the hash and pop the list
            user_hash = path.pop();
        }

        // Find if path exists
        if(path.size() > 0){
            // "Switch case" for Strings
            if(path.popOnFirstEquals("password")){
                password(path, user_hash, request, response);
            }else if(path.popOnFirstEquals("identification")){
                new UserOptionIdentificationAPI(server_config).processRequest(path, user_hash, request, response);
            }else if(path.popOnFirstEquals("address")){

            }else if(path.popOnFirstEquals("email")){

            }
        }else{
            information(user_hash, request, response);
        }
    }

    private void information(String user_hash, HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{

        // If user_hash is null, use user behind session
        if(user_hash == null){
            // Get and check session
            String session_hash = this.db.session.getSessionHashFromCookies(request.getCookies());
            if(!this.db.session.isSessionAlive(session_hash)){
                // Destroys the session hash in cookie
                response.addCookie(SessionDB.destroyCookie());
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "You are not logged in!");
                return; // Technically the request may be fine
            }

            // Get hash
            user_hash = this.db.session.getUserHashFromSession(session_hash);
        }

        // Accept only GET, deny if not GET
        if(!request.getMethod().equals("GET")){
            response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "Only GET Request Method are allowed on this endpoint.");
            return;
        }

        // Check if user is logged in - login is not required
        String session_hash = this.db.session.getSessionHashFromCookies(request.getCookies());

        // Create JSON object
        JSONObject results = new JSONObject();

        // Put in user hash
        results.put("hash", user_hash);

        // Get joined date
        results.put("date_joined", this.db.user.getUserJoinedDate(user_hash));

        // OK
        response.setStatus(HttpServletResponse.SC_OK);
        response.setContentType("text/json; charset=utf-8");
        response.getWriter().print(Utility.encodeJSON(results));
    }

    /** API endpoint to service requests for password change
     *  - All elements on path should be exhausted when arriving to this
     *  function, send Bad Request (400) if there's still some remaining elements
     *  - All required fields (username, current_password, password,
     *  confirm_password) need to be on the query parameters, send Bad Request
     *  (400) if any of those fields are missing.
     *  - Only PUT method are allowed, send Bad Request (400) if method is not PUT
     *  - User needs to be logged in, send Unauthorized (401) if not logged in
     *  - Current password need to be valid, send Forbidden 403 if invalid
     *  - password and confirm password need to match, send Unprocessable
     *  Entity (422) if they do not match.
     *  - Send Accepted 202 with new cookie if successful, otherwise send Forbidden
     *  (403) if unsuccessful
     *  Send 500s error message if there's a problem with the server
     *  @param path Path string - should be empty, send error if there's still some remaining
     *  @param request HTTP request
     *  @param response HTTP response to respond to the user
     */
    private void password(PathList path, String user_hash, HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{

        // If the path still has some left, then it's a bad request
        if(!path.isEmpty()){
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Unknown API endpoint");
            return;
        }

        // If user_hash is null, use user behind session
        if(user_hash == null){
            // Get and check session
            String session_hash = this.db.session.getSessionHashFromCookies(request.getCookies());
            if(!this.db.session.isSessionAlive(session_hash)){
                // Destroys the session hash in cookie
                response.addCookie(SessionDB.destroyCookie());
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "You are not logged in!");
                return; // Technically the request may be fine
            }

            // Get hash
            user_hash = this.db.session.getUserHashFromSession(session_hash);
        }

        // Accept only PUT, deny if not PUT
        if(!request.getMethod().equals("PUT")){
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Only PUT Request Methods are allowed on this endpoint.");
            return;
        }

        // Check the parameters
        if(request.getParameter("current_password") == null || request.getParameter("password") == null || request.getParameter("confirm_password") == null){
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid Query: current_password, password, or confirm_password are missing");
            return;
        }

        // Check if session is alive
        String session_hash = this.db.session.getSessionHashFromCookies(request.getCookies());
        if(!this.db.session.isSessionAlive(session_hash)){

            // Delete that session
            if(session_hash != null) response.addCookie(this.db.session.destroyCookie());

            // Change status
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }

        // Check if current password is correct
        String email_address = this.db.session.getEmailAddressFromSession(session_hash);
        if(!this.db.user.checkUserPassword(email_address, request.getParameter("current_password").trim())){
            // Send error
            response.sendError(HttpServletResponse.SC_FORBIDDEN, "Invalid password");

            return;
        }

        // Check if both password matches
        if(!request.getParameter("password").trim().equals(request.getParameter("confirm_password").trim())){
            // 422 Unprocessable Entity - request is well-formed but was unable to be followed due to semantic errors.
            response.setStatus(422);

            JSONObject answer = new JSONObject();
            answer.put("status", "E1");
            answer.put("description", "Password and confirm password do not match");

            response.setContentType("text/json; charset=utf-8");
            response.getWriter().print(Utility.encodeJSON(answer));
            return;
        }

        try{
            // Okey dokey, we're all good, let's change the password
            if(!this.db.user.changeUserPassword(session_hash, email_address, request.getParameter("password").trim())){
                response.sendError(HttpServletResponse.SC_FORBIDDEN, "Password change is not permitted");
            }else{
                // Set accepted code
                response.setStatus(HttpServletResponse.SC_ACCEPTED);
            }

            // Check if session is already renewed or another relogin is required
            String session_info = this.db.session.getLoginInformation(this.db.session.getSessionHashFromCookies(request.getCookies()));
            if(session_info.equals("NO_LOGIN")){
                response.addCookie(SessionDB.destroyCookie());
            }

        }catch(SQLException exception){
            // Attempt to interpret the exception
            SQLExceptionResponse res = SQLExceptionResponse.interpret(exception);

            // If cannot be interpreted, throw it upstream
            if(res == null) throw exception;

            // Otherwise, continue
            if(res.getCode().equals("ERR_PASSWORD_NOT_ACCEPTED")){
                Utility.sendError(response, 422, "E1", res.getMessage());
            }else if(res.getCode().equals("ERR_INVALID_PASSWORD")){
                Utility.sendError(response, 422, "E2", "Password format is not valid.");
            }else if(res.getCode().equals("ERR_INVALID_SESSION")){
                // Destroys the session hash in cookie
                response.addCookie(SessionDB.destroyCookie());
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "You are not logged in!");
            }else throw exception; // Throw it upstream
        }
    }
}
