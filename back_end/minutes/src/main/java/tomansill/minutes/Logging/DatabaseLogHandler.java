package tomansill.minutes;

import java.util.logging.Handler;
import java.util.logging.LogRecord;
import org.json.simple.JSONObject;

public class DatabaseLogHandler extends Handler{
    Config config;
    Database db;

    public DatabaseLogHandler(){

    }

    public void publish(LogRecord record){
        try{
            System.out.println("DatabaseLogHandler: " + record);
            if(this.config == null) this.config = Config.getInstance();
            if(this.config != null && this.db == null) this.db = Database.getInstance();
            JSONObject desc = new JSONObject();
            desc.put("class", record.getSourceClassName());
            desc.put("method", record.getSourceMethodName());
            LoggerUtil.logError(record.getLevel(), record.getMessage(), Utility.encodeJSON(desc), -1);
        }catch(Exception e){
            // Do nothing for now
        }
    }

    public void flush(){
        // Do nothing
    }

    public void close(){
        // Do nothing
    }
}
