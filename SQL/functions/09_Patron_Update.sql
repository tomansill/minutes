/** Updates the patron's identification information
 *  @param session current login session
 *  @param email_address patron's email address
 *  @param title title of patron - required
 *  @param first_name first name of patron - required
 *  @param middle_name middle name of patron - optional (NULL)
 *  @param last_name last name of patron - required
 *  @param nick_name nickname of patron - optional (NULL)
 *  @param suffix suffix of patron - optional (NULL)
 *  @param birth_date patron's birthday - required
 *  @return true if the update was successful otherwise false if denied
 */
CREATE OR REPLACE FUNCTION update_patron_identification(
    session SESSION_TYPE,
    email_address VARCHAR(128),
    title VARCHAR(10),
    first_name VARCHAR(150),
    middle_name VARCHAR(150),
    last_name VARCHAR(150),
    nick_name VARCHAR(100),
    suffix VARCHAR(10),
    birth_date DATE,
    private_name BOOLEAN,
    private_middle_name BOOLEAN,
    private_birth_date BOOLEAN
) RETURNS BOOLEAN AS $$
BEGIN

    /* Check if email is not null */
    IF email_address IS NULL THEN RAISE EXCEPTION '{"code":"ERR_INVALID_EMAIL_ADDRESS", "message":"email_address is NULL!"}'; END IF;

    /* Check if email is not null */
    IF NOT is_email_address_valid(RTRIM(email_address)) THEN RAISE EXCEPTION '{"code":"ERR_INVALID_EMAIL_ADDRESS", "message":"email_address is invalid!"}'; END IF;

    /* Check if title is valid */
    IF title IS NOT NULL THEN
        IF NOT is_title_valid(title) THEN RAISE EXCEPTION '{"code":"ERR_INVALID_TITLE", "message":"title is invalid!"}'; END IF;
    END IF;

    /* Check if suffix is valid */
    IF title IS NOT NULL THEN
        IF LENGTH(RTRIM(suffix)) > 0 AND NOT is_title_valid(title) THEN RAISE EXCEPTION '{"code":"ERR_INVALID_SUFFIX", "message":"suffix is invalid!"}'; END IF;
    END IF;

    RETURN (SELECT update_patron_identification(session, (SELECT hash FROM get_patron(email_address)), title::TITLE, first_name, middle_name, last_name, nick_name, suffix::SUFFIX, birth_date, private_name, private_middle_name, private_birth_date));

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

/** Updates the patron's identification information
 *  @param session current login session
 *  @param email_address patron's email address
 *  @param title title of patron - required
 *  @param first_name first name of patron - required
 *  @param middle_name middle name of patron - optional (NULL)
 *  @param last_name last name of patron - required
 *  @param nick_name nickname of patron - optional (NULL)
 *  @param suffix suffix of patron - optional (NULL)
 *  @param birth_date patron's birthday - required
 *  @return true if the update was successful otherwise false if denied
 */
CREATE OR REPLACE FUNCTION update_patron_identification(
    session SESSION_TYPE,
    email_address EMAIL_ADDRESS_TYPE,
    title TITLE,
    first_name VARCHAR(150),
    middle_name VARCHAR(150),
    last_name VARCHAR(150),
    nick_name VARCHAR(100),
    suffix SUFFIX,
    birth_date DATE,
    private_name BOOLEAN,
    private_middle_name BOOLEAN,
    private_birth_date BOOLEAN
) RETURNS BOOLEAN AS $$
BEGIN

    RETURN (SELECT update_patron_identification(session, (SELECT hash FROM get_patron(email_address)), title, first_name, middle_name, last_name, nick_name, suffix, birth_date, private_name, private_middle_name, private_birth_date));

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

/** Updates the patron's identification information
 *  @param session current login session
 *  @param patron_hash patron's hash
 *  @param title title of patron - NULL to use old value
 *  @param first_name first name of patron - NULL to use old value
 *  @param middle_name middle name of patron - NULL to use old value
 *  @param last_name last name of patron - NULL to use old value
 *  @param nick_name nickname of patron - NULL to use old value
 *  @param suffix suffix of patron - NULL to use old value
 *  @param birth_date patron's birthday - NULL to use old value
 *  @return true if the update was successful otherwise false if denied
 */
CREATE OR REPLACE FUNCTION update_patron_identification(
    session SESSION_TYPE,
    patron_hash PATRON_HASH_TYPE,
    in_title TITLE,
    in_first_name VARCHAR(150),
    in_middle_name VARCHAR(150),
    in_last_name VARCHAR(150),
    in_nick_name VARCHAR(100),
    in_suffix SUFFIX,
    in_birth_date DATE,
    in_private_name BOOLEAN,
    in_private_middle_name BOOLEAN,
    in_private_birth_date BOOLEAN
) RETURNS BOOLEAN AS $$
BEGIN

    /* Check session */
    IF NOT is_session_alive(session, FALSE) THEN
        RAISE EXCEPTION '{"code":"ERR_INVALID_SESSION, "message":"Session is not valid!"}';
    END IF;

    /* Check if patron exists at all */
    IF NOT patron_exists(patron_hash) THEN
        RAISE EXCEPTION '{"code":"ERR_USER_HASH_NOT_FOUND", "message":"User hash ''%'' does not exist in the system!"}', patron_hash;
    END IF;

    /* Check if inputs are valid */
    IF in_first_name IS NOT NULL THEN
        IF NOT is_name_valid(in_first_name) THEN
            RAISE EXCEPTION '{"code":"ERR_INVALID_FIRST_NAME", "message":"in_first_name is too short!"}';
        END IF;
    END IF;

    IF in_middle_name IS NOT NULL THEN
        IF LENGTH(RTRIM(in_middle_name)) > 0 AND NOT is_name_valid(in_middle_name) THEN
            RAISE EXCEPTION '{"code":"ERR_INVALID_MIDDLE_NAME", "message":"in_middle_name is too short!"}';
        END IF;
    END IF;

    IF LENGTH(RTRIM(in_nick_name)) > 0 AND in_nick_name IS NOT NULL THEN
        IF NOT is_name_valid(in_middle_name) THEN
            RAISE EXCEPTION '{"code":"ERR_INVALID_NICK_NAME", "message":"in_nick_name is too short!"}';
        END IF;
    END IF;

    IF in_last_name IS NOT NULL THEN
        IF NOT is_name_valid(in_last_name) THEN
            RAISE EXCEPTION '{"code":"ERR_INVALID_LAST_NAME", "message":"in_last_name is too short!"}';
        END IF;
    END IF;

    IF birth_date IS NOT NULL THEN
        IF NOT is_birth_date_reasonable(birth_date) THEN
            RAISE EXCEPTION '{"code":"ERR_BIRTH_TOO_YOUNG", "message":"Age must be 18 years or older!"}';
        END IF;
    END IF;

    /* If nothing is changed, don't waste any time */
    IF  in_title IS NULL AND
        in_first_name IS NULL AND
        in_middle_name IS NULL AND
        in_last_name IS NULL AND
        in_nick_name IS NULL AND
        in_suffix IS NULL AND
        in_birth_date IS NULL AND
        in_private_name IS NULL AND
        in_private_middle_name IS NULL AND
        in_private_birth_date IS NULL
    THEN
        /* nothing is changed! */
        RAISE EXCEPTION '{"code":"ERR_EMPTY_PARAMETERS", "message":"All parameters are NULL!"}';
    END IF;

    DECLARE
        /* Collect data */
        author_id INT := (SELECT id FROM identify_patron_from_session_internal(session));
        patron_hash_from_session PATRON_HASH_TYPE := (SELECT hash FROM identify_patron_from_session(session));
        in_patron_id INT := (SELECT id FROM get_patron_internal(patron_hash));

        /* Collect old information */
        mod_title TITLE := (SELECT title FROM Identification WHERE patron_id = in_patron_id AND entry_end_time IS NULL);
        mod_first_name VARCHAR(150) := (SELECT first_name FROM Identification WHERE patron_id = in_patron_id AND entry_end_time IS NULL);
        mod_middle_name VARCHAR(150) := (SELECT middle_name FROM Identification WHERE patron_id = in_patron_id AND entry_end_time IS NULL);
        mod_last_name VARCHAR(150) := (SELECT last_name FROM Identification WHERE patron_id = in_patron_id AND entry_end_time IS NULL);
        mod_nick_name VARCHAR(100) := (SELECT nick_name FROM Identification WHERE patron_id = in_patron_id AND entry_end_time IS NULL);
        mod_suffix SUFFIX := (SELECT suffix FROM Identification WHERE patron_id = in_patron_id AND entry_end_time IS NULL);
        mod_birth_date DATE := (SELECT birth_date FROM Identification WHERE patron_id = in_patron_id AND entry_end_time IS NULL);
        mod_private_name BOOLEAN := (SELECT private_name FROM Identification WHERE patron_id = in_patron_id AND entry_end_time IS NULL);
        mod_private_middle_name BOOLEAN := (SELECT private_middle_name FROM Identification WHERE patron_id = in_patron_id AND entry_end_time IS NULL);
        mod_private_birth_date BOOLEAN := (SELECT private_birth_date FROM Identification WHERE patron_id = in_patron_id AND entry_end_time IS NULL);

        /* flag to indicate if input data is any different from data on the database */
        dirty BOOLEAN := FALSE;

        /* Store the time to use for start and end times here so we won't have very very tiny time gap */
        right_now TIMESTAMP := NOW();

    BEGIN
        /* Check access */
        IF NOT patron_hash = patron_hash_from_session THEN
            /* Check if this patron have rights */
            IF NOT access_control_check_access(session, 'PII_write') THEN RETURN FALSE; END IF;
        END IF;

        /* Check if input data and data on database differs */
        IF in_title IS NOT NULL AND mod_title != in_title THEN dirty := TRUE; END IF;
        IF in_first_name IS NOT NULL AND mod_first_name != RTRIM(in_first_name) THEN dirty := TRUE; END IF;
        IF in_middle_name IS NOT NULL AND (mod_middle_name IS NULL OR mod_middle_name != RTRIM(in_middle_name)) THEN dirty := TRUE; END IF;
        IF in_last_name IS NOT NULL AND mod_last_name != RTRIM(in_last_name) THEN dirty := TRUE; END IF;
        IF in_nick_name IS NOT NULL AND (mod_nick_name IS NULL OR mod_nick_name != RTRIM(in_nick_name)) THEN dirty := TRUE; END IF;
        IF in_suffix IS NOT NULL AND (mod_suffix IS NULL OR mod_suffix != in_suffix) THEN dirty := TRUE; END IF;
        IF in_birth_date IS NOT NULL AND mod_birth_date != in_birth_date THEN dirty := TRUE; END IF;
        IF in_private_name IS NOT NULL AND mod_private_name != in_private_name THEN dirty := TRUE; END IF;
        IF in_private_middle_name IS NOT NULL AND mod_private_middle_name != in_private_middle_name THEN dirty := TRUE; END IF;
        IF in_private_birth_date IS NOT NULL AND mod_private_birth_date != in_private_birth_date THEN dirty := TRUE; END IF;

        /* Update if there's actual changes in the data - if not, just pretend that we actually updated something */
        IF dirty THEN

            /* Copy in new values */
            IF in_title IS NOT NULL THEN mod_title := in_title; END IF;
            IF in_first_name IS NOT NULL THEN mod_first_name := RTRIM(in_first_name); END IF;
            IF in_middle_name IS NOT NULL THEN mod_middle_name := RTRIM(in_middle_name); END IF;
            IF in_last_name IS NOT NULL THEN mod_last_name := RTRIM(in_last_name); END IF;
            IF in_nick_name IS NOT NULL THEN mod_nick_name := RTRIM(in_nick_name); END IF;
            IF in_suffix IS NOT NULL THEN mod_suffix := in_suffix; END IF;
            IF in_birth_date IS NOT NULL THEN mod_birth_date := in_birth_date; END IF;
            IF in_private_name IS NOT NULL THEN mod_private_name := in_private_name; END IF;
            IF in_private_middle_name IS NOT NULL THEN mod_private_middle_name := in_private_middle_name; END IF;
            IF in_private_birth_date IS NOT NULL THEN mod_private_birth_date := in_private_birth_date; END IF;

            /* Special cases - request to remove optional values */
            IF in_middle_name = '' THEN mod_middle_name := NULL; END IF;
            IF in_nick_name = '' THEN mod_nick_name := NULL; END IF;
            IF in_suffix = '' THEN mod_suffix := NULL; END IF;

            /* Update the older entry to end the entry - if it does not exist, this statement will have no effect */
            UPDATE Identification SET entry_end_time = right_now WHERE Identification.patron_id = in_patron_id AND entry_end_time IS NULL;

            /* Update the information */
            INSERT INTO Identification(
                patron_id,
                title,
                first_name,
                middle_name,
                last_name,
                nick_name,
                suffix,
                birth_date,
                entry_author,
                entry_start_time,
                private_name,
                private_middle_name,
                private_birth_date
            )VALUES(
                in_patron_id,
                mod_title,
                mod_first_name,
                mod_middle_name,
                mod_last_name,
                mod_nick_name,
                mod_suffix,
                mod_birth_date,
                author_id,
                right_now,
                mod_private_name,
                mod_private_middle_name,
                mod_private_birth_date
            );
        END IF;
    END;

    /* Refresh the session expiration date */
    IF is_session_alive(session, TRUE) THEN END IF; /* do nothing */

    RETURN TRUE;

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;
