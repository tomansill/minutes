package tomansill.minutes;

/** Password Utility
 *  Dedicated utility with functions for password to make my life easier
 *  @author Thomas Ansill
 */
public class PasswordUtility{

    /** Maximum length of password */
    public static final int MAXIMUM_LENGTH = 64;

    /** Minimum length of password */
    public static final int MINIMUM_LENGTH = 8;

    /** Prevents instantiation of this utility */
    private PasswordUtility(){}

    /** Function to check if password is valid
     *  @param password password to be checked
     *  @return true if valid otherwise false
     */
    public static boolean isPasswordValid(String password){

        // Make sure password is not empty
        if(password == null) return false;

        // Password must be minimum of 8 characters
        if(password.trim().length() < MINIMUM_LENGTH) return false;

        // Password must be maximum of 64 characters
        if(password.trim().length() > MAXIMUM_LENGTH) return false;

        // All OK here
        return true;
    }
}
