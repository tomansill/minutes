package tomansill.minutes;

import java.util.Arrays;
import java.util.LinkedList;
import java.nio.file.Path;
import java.util.NoSuchElementException;
import java.util.regex.Pattern;
import java.util.ListIterator;

public class PathList{
    private LinkedList<String> list;
    private boolean root;

    public PathList(Path path){
        this(path.toString(), true);
    }

    public PathList(Path path, boolean toLowerCase){
        this(path.toString(), toLowerCase);
    }

    public PathList(String path){
        this(path, true);
    }

    public PathList(String path, boolean toLowerCase){

        // Trim any whitespace
        path = path.trim();

        // Check if path starts from root
        this.root = path.charAt(0) == '/';

        // Convert to lowercase
        if(toLowerCase) path = path.toLowerCase();

        // Split the '/' and convert to list
        this.list = new LinkedList(Arrays.asList(path.split("/")));

        // If root, pop front to correct the list
        if(this.root) this.list.pop();
    }

    public String pop() throws NoSuchElementException{
        if(this.root) this.root = false;
        return this.list.pop();
    }

    public String tryPop(){
        if(this.isEmpty()) return null;
        return this.pop();
    }

    public String peek() throws NoSuchElementException{
        return this.list.getFirst();
    }

    public String tryPeek(){
        return this.list.peekFirst();
    }

    public boolean isEmpty(){
        return this.list.isEmpty();
    }

    public boolean firstEquals(String compare){
        if(compare == null) throw new NullPointerException();
        if(this.list.isEmpty()) return false;
        return this.list.getFirst().equals(compare);
    }

    public boolean firstEquals(Pattern compare){
        if(compare == null) throw new NullPointerException();
        if(this.list.isEmpty()) return false;
        return compare.matcher(this.list.getFirst()).matches();
    }

    public boolean firstMatches(String compare){
        if(compare == null) throw new NullPointerException();
        if(this.list.isEmpty()) return false;
        return this.list.getFirst().matches(compare);
    }

    public boolean popOnFirstEquals(String compare){
        if(compare == null) throw new NullPointerException();
        if(this.list.isEmpty()) return false;
        boolean result = this.list.getFirst().equals(compare);
        if(result) this.pop();
        return result;
    }

    public boolean popOnFirstEquals(Pattern compare){
        if(compare == null) throw new NullPointerException();
        if(this.list.isEmpty()) return false;
        boolean result = compare.matcher(this.list.getFirst()).matches();
        if(result) this.pop();
        return result;
    }

    public boolean popOnFirstMatches(String compare){
        if(compare == null) throw new NullPointerException();
        if(this.list.isEmpty()) return false;
        boolean result = this.list.getFirst().matches(compare);
        if(result) this.pop();
        return result;
    }

    public int size(){
        return this.list.size();
    }

    @Override
    public String toString(){
        StringBuffer sb = new StringBuffer();
        ListIterator<String> iterator = this.list.listIterator();
        if(this.root) sb.append('/');
        while(iterator.hasNext()){
            sb.append(iterator.next());
            if(iterator.hasNext()) sb.append('/');
        }
        return sb.toString();
    }
}
