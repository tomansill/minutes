package tomansill.minutes;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.file.Path;
import java.io.File;
import java.util.Map;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import java.io.IOException;
import java.util.regex.Pattern;
import javax.servlet.http.Cookie;
import java.util.Calendar;
import java.util.Date;

/** Class that services or route requests that relates to Users
 *  @author Thomas Ansill
 */
public class UserAPI{

    /** Server config */
    private Config server_config;

    /** Database object */
    private Database db;

    /** Constuctor */
    public UserAPI(Config server_config) throws Exception{
        this.server_config = server_config;
        this.db = Database.getInstance();
    }

    /** Function that services the HTTP request and possibly route them further
     *  @param path URL path
     *  @param request HTTP request
     *  @param response HTTP response to respond to the user
     */
    public void processRequest(PathList path, HttpServletRequest request, HttpServletResponse response) throws Exception{

        // "Switch case" for Strings
        if(path.popOnFirstEquals("search_user")){
            //searchUser(path, request, response);
        }else if(path.popOnFirstEquals("login")){
            login(path, request, response);
        }else if(path.popOnFirstEquals("auth")){
            auth(path, request, response);
        }else if(path.popOnFirstEquals("register")){
            register(path, request, response);
        }else if(path.popOnFirstEquals("logout")){
            logout(path, request, response);
        }else{
            new UserOptionAPI(server_config).processRequest(path, request, response);
        }
    }

    /** API endpoint to service requests for registration
     *  - All elements on path should be exhausted when arriving to this
     *  function, send Bad Request (400) if there's still some remaining elements
     *  - Only POST method are allowed, send Bad Request (400) if method is not POST
     *  - All required fields (username, password, title, first_name, last_name,
     *  birth_date) need to be on the query parameters, send Bad Request (400)
     *  if any of those fields are missing. Optional fields (nick_name, suffix)
     *  may be present on the query parameters.
     *  - User must not have a login to register If user is already logged in,
     *  send Unprocessable Entity (422) to indicate so.
     *  - Email address in Username must be unclaimed, send Unprocessable Entity
     *  (422) if Email Address is already claimed.
     *  - Username must be a valid email, send Unprocessable Entity (422) if
     *  not valid
     *  - Password must be valid, send Unprocessable Entity (422) if not valid
     *  - Title must be valid, send Unprocessable Entity (422) if not valid
     *  - First Name must be valid, send Unprocessable Entity (422) if not valid
     *  - Middle name must be valid, send Unprocessable Entity (422) if not valid
     *  - Last Name must be valid, send Unprocessable Entity (422) if not valid
     *  - Overall name (first + middle + last) need to be long enough, send
     *  Unprocessable Entity (422) if not valid
     *  - Suffix must be valid, send Unprocessable Entity (422) if not valid
     *  - Birthday date must be valid, send Unprocessable Entity (422) if not
     *  valid
     *  - If registration are unsuccessful, send Forbidden (403)
     *  If successful, a cookie with session hash is returned with Created (201)
     *  Send 500s error message if there's a problem with the server
     *  @param path Path string - should be empty, send error if there's still some remaining
     *  @param request HTTP request
     *  @param response HTTP response to respond to the user
     */
    private void register(PathList path, HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{

        // If the path still has some left, then it's a bad request
        if(!path.isEmpty()){
            return; // Immediately exit this function
        }

        // Accept only POST, deny if not POST
        if(!request.getMethod().equals("POST")){
            response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "Only POST Request Methods are allowed on this endpoint.");
            return; // Immediately exit this function
        }

        // Check the parameters
        if(
            request.getParameter("username") == null ||
            request.getParameter("password") == null ||
            request.getParameter("title") == null ||
            request.getParameter("first_name") == null ||
            request.getParameter("last_name") == null ||
            request.getParameter("birth_date") == null
        ){
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid Query");
            return; // Immediately exit this function
        }

        // Check session
        String session_info = this.db.session.getLoginInformation(this.db.session.getSessionHashFromCookies(request.getCookies()));
        if(session_info.equals("LOGIN") || session_info.equals("TWO_FACTOR_AUTHENICATION")){

            // 422 Unprocessable Entity - request is well-formed but was unable to be followed due to semantic errors.
            Utility.sendError(response, 422, "E0", "The user is logged in. To register, the user must be logged off.");
            return; // Immediately exit this function
        }

        // Submit query
        try{
            if(this.db.user.register(
                request.getParameter("username"),
                request.getParameter("password"),
                request.getParameter("title"),
                request.getParameter("first_name"),
                request.getParameter("middle_name"),
                request.getParameter("last_name"),
                request.getParameter("nick_name"),
                request.getParameter("suffix"),
                Utility.convertToDate(request.getParameter("birth_date"))
            )){
                response.setStatus(HttpServletResponse.SC_CREATED); // Success
            }else{
                response.sendError(HttpServletResponse.SC_FORBIDDEN, "Registration is closed");
            }
        }catch(SQLException exception){

            // Attempt to interpret the exception
            SQLExceptionResponse res = SQLExceptionResponse.interpret(exception);

            // If cannot be interpreted, throw it upstream
            if(res == null) throw exception;

            // Otherwise, continue
            if(res.getCode().equals("ERR_DUPLICATE_ACCOUNT")){
                Utility.sendError(response, 422, "E1", "Email Address is already in use.");
            }else if(res.getCode().equals("ERR_INVALID_EMAIL_ADDRESS")){
                Utility.sendError(response, 422, "E2", "Username is not a valid email address.");
            }else if(res.getCode().equals("ERR_INVALID_PASSWORD")){
                Utility.sendError(response, 422, "E3", "Password is invalid.");
            }else if(res.getCode().equals("ERR_PASSWORD_NOT_ACCEPTED")){
                Utility.sendError(response, 422, "E4", res.getMessage());
            }else if(res.getCode().equals("ERR_INVALID_TITLE")){
                Utility.sendError(response, 422, "E5", "Value used for name title is invalid.");
            }else if(res.getCode().equals("ERR_INVALID_FIRST_NAME")){
                Utility.sendError(response, 422, "E6", "First name is too short.");
            }else if(res.getCode().equals("ERR_INVALID_MIDDLE_NAME")){
                Utility.sendError(response, 422, "E7", "Middle name is too short.");
            }else if(res.getCode().equals("ERR_INVALID_LAST_NAME")){
                Utility.sendError(response, 422, "E8", "Last name is too short.");
            }else if(res.getCode().equals("ERR_INVALID_NICK_NAME")){
                Utility.sendError(response, 422, "E9", "Nick name is too short.");
            }else if(res.getCode().equals("ERR_INVALID_SUFFIX")){
                Utility.sendError(response, 422, "E10", "Value used for name suffix is invalid.");
            }else if(res.getCode().equals("ERR_INVALID_NAME")){
                Utility.sendError(response, 422, "E11", "Overall name is too short.");
            }else if(res.getCode().equals("ERR_INVALID_BIRTH_DATE")){
                Utility.sendError(response, 422, "E12", "Birth date is invalid.");
            }else if(res.getCode().equals("ERR_BIRTH_TOO_YOUNG")){
                Utility.sendError(response, 422, "E12", "Birth date is invalid. User needs to be 18 years or older.");
            }else if(res.getCode().equals("ERR_DUPLICATE_ACCOUNT")){
                response.sendError(HttpServletResponse.SC_CONFLICT, "An account with that name already exist");
            }else throw exception; // Throw it upstream
        }
    }

    /** API endpoint to service requests for log out
     *  - All elements on path should be exhausted when arriving to this
     *  function, send Bad Request (400) if there's still some remaining elements
     *  - Only GET and POST method are allowed, send Bad Request (400) if method
     *  is not GET or POST
     *  - User must have a login to log off If user is not logged in, send
     *  Unauthorized (401)
     *  - If successful, return empty cookie with Accepted (202)
     *  Send 500s error message if there's a problem with the server
     *  @param path Path string - should be empty, send error if there's still some remaining
     *  @param request HTTP request
     *  @param response HTTP response to respond to the user
     */
    private void logout(PathList path, HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{

        // If the path still has some left, then it's a bad request
        if(!path.isEmpty()){
            return;
        }

        // Accept only GET or POST, deny if not either
        if(!request.getMethod().equals("GET") && !request.getMethod().equals("POST")){
            response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "Only GET and POST Request Methods are allowed on this endpoint.");
            return;
        }

        // Check if logged in
        String session_hash = this.db.session.getSessionHashFromCookies(request.getCookies());
        if(!this.db.session.isSessionAlive(session_hash)){
            response.addCookie(SessionDB.destroyCookie());
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "User is not logged in.");
            return;
        }

        // Attempt to destroy
        Cookie cookie = this.db.session.destroySession(request.getCookies());
        if(cookie != null) response.addCookie(cookie);
        response.setStatus(HttpServletResponse.SC_ACCEPTED);
    }

    /**
     *  @param path Path string - should be empty, send error if there's still some remaining
     *  @param request HTTP request
     *  @param response HTTP response to respond to the user
     */
    private void auth(PathList path, HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{

        // If the path still has some left, then it's a bad request
        if(!path.isEmpty()){
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Unknown API endpoint");
            return;
        }

        // Accept only POST, deny if not POST
        if(!request.getMethod().equals("POST")){
            response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "Only POST Request Methods are allowed on this endpoint.");
            return;
        }

        // Check the parameters
        if(request.getParameter("code") == null){
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid Query");
            return;
        }

        try{

            // Check session
            String session_info = this.db.session.getLoginInformation(this.db.session.getSessionHashFromCookies(request.getCookies()));
            if(!session_info.equals("TWO_FACTOR_AUTHENICATION")){
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "User is not logged in! Log in first.");
                return; // Immediately exit this function
            }

            // Check the code
            if(this.db.user.twoFactorAuthenication(this.db.session.getSessionHashFromCookies(request.getCookies()), request.getParameter("code"))){
                response.setStatus(HttpServletResponse.SC_ACCEPTED);
            }else{
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            }
        }catch(SQLException exception){
            // Attempt to interpret the exception
            SQLExceptionResponse res = SQLExceptionResponse.interpret(exception);

            // If cannot be interpreted, throw it upstream
            if(res == null) throw exception;

            // Otherwise, continue
            if(res.getCode().equals("ERR_INVALID_SESSION") || res.getCode().equals("ERR_NO_LOGIN")){
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "User is not logged in! Log in first.");
            }else if(res.getCode().equals("ERR_INVALID_TWO_FACTOR_AUTH_CODE")){
                response.sendError(422, "Authenication code format is not valid.");
            }else throw exception; // Throw it upstream
        }
    }

    /** API endpoint to service requests for login
     *  - All elements on path should be exhausted when arriving to this
     *  function, send Bad Request (400) if there's still some remaining elements
     *  - All required fields (username, password) need to be on the query
     *  parameters, send Bad Request (400) if any of those fields are missing.
     *  Optional fields (remember_me) may be present on the query parameters.
     *  remember_me field needs to be either true of false, send Unprocessable
     *  Entity (400) if not valid
     *  - Only POST method are allowed, send Bad Request (400) if method is not POST
     *  - Username and Password need to be valid, send Forbidden 403 if invalid.
     *  If valid, a cookie with session hash is returned with Accepted (202)
     *  Send 500s error message if there's a problem with the server
     *  @param path Path string - should be empty, send error if there's still some remaining
     *  @param request HTTP request
     *  @param response HTTP response to respond to the user
     */
    private void login(PathList path, HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{

        // If the path still has some left, then it's a bad request
        if(!path.isEmpty()){
            return;
        }

        // Accept only POST, deny if not POST
        if(!request.getMethod().equals("POST")){
            response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "Only POST Request Methods are allowed on this endpoint.");
            return; // Found the endpoint just fine
        }

        // Check the parameters
        if(request.getParameter("username") == null || request.getParameter("password") == null){
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid Query: Missing one or both of 'username' and 'password' parameters.");
            return; // Found the endpoint just fine
        }

        // Check the optional parameter
        boolean remember_me = false;
        if(request.getParameter("remember_me") != null){
            if(request.getParameter("remember_me").equalsIgnoreCase("true") || request.getParameter("remember_me").equalsIgnoreCase("false")){
                remember_me = Boolean.parseBoolean(request.getParameter("remember_me"));
            }else{
                response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid Query: Cannot understand 'remember_me'.");
                return; // Found the endpoint just fine
            }
        }

        // Check if logged in
        String session_hash = this.db.session.getSessionHashFromCookies(request.getCookies());
        if(this.db.session.isSessionAlive(session_hash)){
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "User is already logged in. Log off first");
            return; // Found the endpoint just fine
        }

        try{
            // Authenicate
            SessionDB.Authenication_Result ar = this.db.session.authenicate(request.getCookies(), request.getParameter("username"), request.getParameter("password"), remember_me);

            // Check status and add updated cookie
            if(ar.status == SessionDB.Authenication_Status.FAILURE){
                response.setStatus(HttpServletResponse.SC_FORBIDDEN); // Code to indicate a login fail
            }else if(ar.status == SessionDB.Authenication_Status.DISABLED){
                response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "Account logins are disabled. Try again later.");
            }else{

                // Update cookie
                response.addCookie(ar.cookie);

                // Check if user needs to do two factor authenication
                if(ar.status == SessionDB.Authenication_Status.SUCCESS){
                    response.setStatus(HttpServletResponse.SC_ACCEPTED); // ACCEPTED 202
                }else if(ar.status == SessionDB.Authenication_Status.TWO_FACTOR_AUTHENICATION){
                    response.setStatus(HttpServletResponse.SC_TEMPORARY_REDIRECT); // TEMPORARY REDIRECT 307
                    response.sendRedirect("/api/user/auth"); // Direct the user to use this URL to further authenicate the login
                }
            }
        }catch(SQLException exception){

            // Attempt to interpret the exception
            SQLExceptionResponse res = SQLExceptionResponse.interpret(exception);

            // If cannot be interpreted, throw it upstream
            if(res == null) throw exception;

            // Otherwise, continue
            if(res.getCode().equals("ERR_INVALID_EMAIL_ADDRESS")){
                Utility.sendError(response, 422, "E1", "Username is not a valid email address.");
            }else if(res.getCode().equals("ERR_INVALID_PASSWORD")){
                Utility.sendError(response, 422, "E2", "Password format is not valid.");
            }else if(res.getCode().equals("ERR_LOGIN_DISABLED")){
                response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "Account logins are disabled. Try again later.");
            }else throw exception; // Throw it upstream
        }
    }
}
