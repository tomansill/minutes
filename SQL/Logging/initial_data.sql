INSERT INTO Properties (key, value) VALUES ('Access_Log_Space_Limit', '1000000'); /* 1GB */
INSERT INTO Properties (key, value) VALUES ('Error_Log_Space_Limit', '500000'); /* 500MB */
INSERT INTO Properties (key, value) VALUES ('Access_Agent_Space_Limit', '1000000'); /* 1GB */
INSERT INTO Properties (key, value) VALUES ('Access_Resource_Space_Limit', '1000000'); /* 1GB */
INSERT INTO Statistics (key) VALUES ('Access_Agent_Total_Count');
INSERT INTO Statistics (key) VALUES ('Access_Log_Total_Count');
INSERT INTO Statistics (key) VALUES ('Access_Resource_Total_Count');
