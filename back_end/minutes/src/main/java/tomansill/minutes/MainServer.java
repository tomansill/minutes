package tomansill.minutes;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.ConnectionFactory;

import java.io.File;
import java.nio.file.Path;

import java.io.FileReader;
import java.io.Writer;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.servlet.MultipartConfigElement;
import javax.servlet.DispatcherType;

import org.json.simple.JSONObject;

import java.util.Scanner;
import java.util.EnumSet;

/** Main Server code
 *  Handles all server connections
 *  @author Thomas Ansill
 */
public class MainServer {

    /** Main method
     *  Usage: MainServer [path_to_config.json]
     *  By default, it will find config.json and start automatically, if
     *  config.json is not found, it will create one with preset options.
     *  A path to config.json can be supplied to commandline as an argument
     *  @param args Commandline arguments
     */
    public static void main(String[] args){
        System.out.println("Minutes Application Server");

        // Configure Logger
        //System.setProperty("org.eclipse.jetty.util.log.class", "tomansill.minutes.MinutesLogger");
        //System.setProperty("org.eclipse.jetty.LEVEL", "WARN");

        // Default config file location
        File config_path = new File("config.json");

        // If config file location is supplied , change the location
        if(args.length == 1){
            config_path = new File(args[0]);
        }

        // Attempt to load it
        if(!config_path.exists()){
            if(args.length == 1){
                System.err.println("Configuration file is not found. Please double-check the path");
            }else{

                // Cannot find the config, inform the user that new config is being created
                System.err.println("Configuration file is not found. Creating a new config file...");

                try{
                    // Create the default config
                    Config.createDefaultConfig();

                    // Inform the user that the config is set up
                    System.err.println("New Configuration file is just 'config.json' at the program's working directory.");
                    System.err.println("Please edit the config file accordingly before running the program.");

                }catch(IOException exception){
                    System.err.println(exception.getMessage());
                }
            }
        }

        // Loads the config
        Config server_config = null;
        try{
            server_config = Config.loadConfig(config_path);
        }catch(IOException e){
            System.err.println(e.getMessage());
            System.exit(0);
        }

        //Logger logger = Logger.getLogger();
        try{
            // Connect to database
            server_config.db.open();

            // Turn off logging for now
            //System.setProperty("org.eclipse.jetty.util.log.class", "org.eclipse.jetty.util.log.StdErrLog");
            //System.setProperty("org.eclipse.jetty.LEVEL", "OFF");

            // Set up the server
            Server server = new Server(server_config.port);
            System.out.println("Server root is located at " + server_config.root.getCanonicalPath());

            // Create the context
            ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
            context.setResourceBase(server_config.root.getCanonicalPath());
            context.setContextPath("/");
            server.setHandler(context);

            // Assign holder to API at /api
            ServletHolder holderAPI = new ServletHolder(new API(server_config));
            holderAPI.getRegistration().setMultipartConfig(new MultipartConfigElement("/tmp/web/minutes_server/", 1048576, 1048576, 262144));
            context.addServlet(holderAPI, "/api/*");
            context.addFilter(CleanResponseFilter.class, "/api/*", EnumSet.of(DispatcherType.REQUEST)); // Didn't like jetty's default sendError, this filter replaces that method

            // Assign holder to everything else for static file handling
            ServletHolder holderRoot = new ServletHolder("default", LoggedDefaultServlet.class);
            holderRoot.setInitParameter("dirAllowed", "false"); // Prohibit directory listing
            context.addServlet(holderRoot, "/");

            // Configure the server to not send server version
            // Thanks to Jacob on https://stackoverflow.com/questions/15652902/remove-the-http-server-header-in-jetty-9#15675075
            for(Connector connector : server.getConnectors()) {
                for(ConnectionFactory connection  : connector.getConnectionFactories()) {
                    if(connection instanceof HttpConnectionFactory) {
                        ((HttpConnectionFactory)connection).getHttpConfiguration().setSendServerVersion(false);
                    }
                }
            }

            // Start the server
            server.start();

            // Command Loop
            if(true){

                // Interactive mode
                Scanner in = new Scanner(System.in);

                // Loop until broken
                while(true){

                    // Input
                    System.out.print("> ");
                    String input = in.nextLine();

                    // Quit command
                    if(input.trim().equals("quit")){
                        server.stop();
                        break;
                    }
                }
            }else{
                // Other solutions
            }

            // Wait for server to stop
            server.join();

            // Close the database connection
            server_config.db.close();

            // Good bye
            System.out.println("Server shutting down");

        }catch(Exception exception){
            // Log here and exit
            System.err.println("SERVER ERROR: " + exception.getMessage());
            exception.printStackTrace();
        }
    }
}
