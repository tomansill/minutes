package tomansill.minutes;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import java.io.IOException;

/** A bundle of functions that handles database functions to Global functions
 *  @author Thomas Ansill
 */
public class LoggingDB{

    /** Server config */
    private Config server_config;

    /** Constuctor */
    public LoggingDB(Config server_config){
        this.server_config = server_config;
    }


    public int insertAccessLog(String address, String user_agent, String url, String method, String content, String session) throws SQLException{

        // Check if Server config is initialized. It must be initialized to use the database
        if(server_config == null){
            throw new SQLException("ERROR: The server config is not initialized. Meaning there's no database connection!");
        }

        // Answer
        int answer = -1;

        // Execute SQL query
        PreparedStatement stmt = server_config.db.connection.prepareStatement("SELECT insert_access_log(?,?,?,?,?,?)");
        stmt.setString(1, address);
        stmt.setString(2, user_agent);
        stmt.setString(3, url);
        stmt.setString(4, method);
        stmt.setString(5, content);
        stmt.setString(6, session);
        ResultSet rs = stmt.executeQuery();

        // Get the result - if exception is thrown for empty rs, then database has issues, not this
        rs.next();
        answer = rs.getInt(1);

        // Clean up
        rs.close();
        stmt.close();

        return answer;
    }

    public void updateAccessLog(int access_log_id, int status_code) throws SQLException{

        // Check if Server config is initialized. It must be initialized to use the database
        if(server_config == null){
            throw new SQLException("ERROR: The server config is not initialized. Meaning there's no database connection!");
        }

        // Execute SQL query
        PreparedStatement stmt = server_config.db.connection.prepareStatement("SELECT update_access_log(?,?)");
        stmt.setInt(1, access_log_id);
        stmt.setInt(2, status_code);
        ResultSet rs = stmt.executeQuery();


        // Clean up
        rs.close();
        stmt.close();
    }

    public void insertErrorLog(String severity, Exception exception, int access_log_id, boolean trim) throws SQLException, IOException{

        // Get the stack trace and put it in json array
        JSONArray array = new JSONArray();
        int count = 0;
        for(int i = 0; i < exception.getStackTrace().length; i++){
            StackTraceElement ste = exception.getStackTrace()[i];

            JSONObject stacktrace = new JSONObject();
            stacktrace.put("class_name", ste.getClassName());
            stacktrace.put("method_name", ste.getMethodName());
            stacktrace.put("line_number", ste.getLineNumber());
            array.add(stacktrace);

            // For most cases, the stack logs below HttpServlet class is irrelevant to our debugging process so we don't want those stack logs
            if(count == 1 && ste.getClassName().equals("javax.servlet.http.HttpServlet") && ste.getMethodName().equals("service") && trim){
                break;
            }
            count++;
        }

        insertErrorLog(severity, exception.getMessage(), Utility.encodeJSON(array), access_log_id);
    }

    public void insertErrorLog(String severity, String message, String description, int access_log_id) throws SQLException{

        // Check if Server config is initialized. It must be initialized to use the database
        if(server_config == null){
            throw new SQLException("ERROR: The server config is not initialized. Meaning there's no database connection!");
        }

        // Execute SQL query
        PreparedStatement stmt = server_config.db.connection.prepareStatement("SELECT insert_error_log(?::ERROR_SEVERITY,?::VARCHAR,?::TEXT,?::INT)");
        stmt.setString(1, severity);
        stmt.setString(2, message);
        stmt.setString(3, description);
        if(access_log_id == -1) stmt.setString(4, "NULL");
        else stmt.setInt(4, access_log_id);

        ResultSet rs = stmt.executeQuery();

        rs.next();

        // Clean up
        rs.close();
        stmt.close();
    }
}
