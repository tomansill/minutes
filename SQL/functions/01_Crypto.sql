DROP EXTENSION IF EXISTS pgcrypto;
CREATE EXTENSION pgcrypto;

/** Function to generate a SHA256 digest of the input TEXT
 *  text  - input text
 *  returns digest
 */

CREATE OR REPLACE FUNCTION SHA256(message TEXT) RETURNS VARCHAR(64) AS $$
BEGIN
    RETURN encode(digest(message, 'sha256'), 'hex');
END; $$
LANGUAGE PLPGSQL IMMUTABLE SECURITY DEFINER;

/** Function to generate a SHA512 digest of the input TEXT
 *  text  - input text
 *  returns digest
 */
CREATE OR REPLACE FUNCTION SHA512(message TEXT) RETURNS VARCHAR(128) AS $$
BEGIN
    RETURN encode(digest(message, 'sha512'), 'hex');
END; $$
LANGUAGE PLPGSQL IMMUTABLE SECURITY DEFINER;

/** Function to generate a secure password digest
 *  message - password
 *  salt - random number sequence
 *  returns digest
 */
CREATE OR REPLACE FUNCTION password_hash(message TEXT, salt TEXT) RETURNS VARCHAR(128) AS $$
BEGIN
    /* sha256 ( salt + sha256(message) + message ) */
    RETURN SHA512(CONCAT(salt, CONCAT(SHA512(message), message)));
END; $$
LANGUAGE PLPGSQL IMMUTABLE SECURITY INVOKER;

/** Function to calculate the Time-based One Time Password to be used as two factor authenication
 *  @param key secret key to be used for calculation
 *  @return 6 digit code
 */
CREATE OR REPLACE FUNCTION get_TOTP(key TEXT) RETURNS VARCHAR(6) AS $$
BEGIN
    DECLARE
        elapsed_time INT := (select FLOOR(EXTRACT(epoch FROM NOW())/30));
        elapsed_time_bytea BYTEA := (SELECT decode(LPAD(to_hex(elapsed_time), 16, '0'), 'hex'));
        hash_b BYTEA := (SELECT HMAC(elapsed_time_bytea, key::BYTEA, 'sha1'::TEXT));
        hash VARCHAR := (SELECT encode(hash_b, 'hex'));
        h_offset INT := (SELECT get_byte(decode('0' || SUBSTRING(hash, LENGTH(hash), LENGTH(hash)+1),'hex')::BYTEA, 0));
        hash_part VARCHAR := (SELECT encode(SUBSTRING(hash_b FROM (h_offset + 1) FOR 4), 'hex'));
        hash_part_i INT := (SELECT SET_BIT(('x' || LPAD(hash_part, 8, '0'))::BIT(32), 0, 0)::INT);
    BEGIN
        RETURN LPAD((hash_part_i % 1000000)::TEXT, 6, '0');
    END;

END; $$
LANGUAGE PLPGSQL IMMUTABLE SECURITY DEFINER;
