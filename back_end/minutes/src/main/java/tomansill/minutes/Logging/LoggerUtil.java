package tomansill.minutes;

import java.util.logging.Level;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Part;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import java.time.LocalDateTime;
import java.util.Enumeration;


public class LoggerUtil{

    public static void logError(Level severity, Throwable exception){
        logError(severity.toString(), exception, -1);
    }

    public static void logError(String severity, Throwable exception){
        logError(severity.toString(), null, exception, -1);
    }

    public static void logError(Level severity, String message, Throwable exception){
        logError(severity.toString(), message, exception, -1);
    }

    public static void logError(String severity, String message, Throwable exception){
        logError(severity, message, exception, -1);
    }

    public static void logError(String severity, String message, String description){
        logError(severity, message, description, -1);
    }

    public static void logError(Level severity, String message, String description){
        logError(severity.toString(), message, description, -1);
    }

    public static void logError(Level severity, Throwable exception, int access_log_id){
        logError(severity.toString(), exception, access_log_id);
    }

    public static void logError(String severity, Throwable exception, int access_log_id){
        logError(severity, null, exception, access_log_id);
    }

    public static void logError(Level severity, String message, String description, int access_log_id){
        logError(severity.toString(), message, description, access_log_id);
    }

    public static void logError(String severity, String message, Throwable exception, int access_log_id){

        // Get the stack trace and put it in json array
        JSONArray array = new JSONArray();
        int count = 0;
        for(int i = 0; i < exception.getStackTrace().length; i++){
            StackTraceElement ste = exception.getStackTrace()[i];

            JSONObject stacktrace = new JSONObject();
            stacktrace.put("message", (message == null ? exception.getMessage() : message));
            stacktrace.put("class_name", ste.getClassName());
            stacktrace.put("method_name", ste.getMethodName());
            stacktrace.put("line_number", ste.getLineNumber());
            array.add(stacktrace);

            // For most cases, the stack logs below HttpServlet class is irrelevant to our debugging process so we don't want those stack logs
            if(count == 1 && ste.getClassName().equals("javax.servlet.http.HttpServlet") && ste.getMethodName().equals("service")){
                break;
            }
            count++;
        }

        try{
            // Pass it through
            logError(severity, exception.getMessage(), Utility.encodeJSON(array), access_log_id);
        }catch(IOException exception1){
            // Will probably never happen but handle this just in case
            logError(Level.SEVERE, exception.getMessage(), ("Not available: logError(Level, Exception, int) failed! - " + exception1.getMessage()), access_log_id);
        }
    }

    public static void logError(String severity, String message, String description, int access_log_id){
        try{
            Database db = Database.getInstance();
            db.logger.insertErrorLog(severity, message, description, access_log_id);
        }catch(Exception e){
            e.getMessage();
            // Fail safe - stderr - access log id is not used
            System.err.println("FALLBACK: [" + LocalDateTime.now().toString() + "] " + severity.toString() + " : " + message + " - " + description);
        }
    }

    public static int logAccess(HttpServletRequest request) throws IOException{
        JSONObject content_json = new JSONObject();

        // Parts
        JSONArray parts_json = new JSONArray();
        try{
        for(Part part : request.getParts()){
            JSONObject part_json = new JSONObject();
            part_json.put("name", part.getName());
            part_json.put("content_type", part.getContentType());
            part_json.put("size", part.getSize());
            part_json.put("file_name", part.getSubmittedFileName());
            JSONArray part_headers = new JSONArray();
            for(String header : part.getHeaderNames()){
                JSONObject parts_header_json = new JSONObject();
                JSONArray headers_json = new JSONArray();
                for(String value : part.getHeaders(header)){
                    headers_json.add(value);
                }
                parts_header_json.put(header, headers_json);
            }
            part_json.put("headers", part_headers);
        }
        }catch(Exception e){}
        content_json.put("parts", parts_json);

        // Headers
        JSONArray headers_json = new JSONArray();
        Enumeration headers = request.getHeaderNames();
        while(headers.hasMoreElements()){
            String header = (String) headers.nextElement();
            JSONObject header_json = new JSONObject();
            JSONArray header_values_json = new JSONArray();
            Enumeration header_values = request.getHeaders(header);
            while(header_values.hasMoreElements()){
                String value = (String) header_values.nextElement();
                header_values_json.add(value);
            }
            header_json.put(header, header_values_json);
            headers_json.add(header_json);
        }
        content_json.put("headers", headers_json);

        // Parameters
        JSONArray parameters_json = new JSONArray();
        Enumeration parameters = request.getParameterNames();
        while(parameters.hasMoreElements()){
            String parameter = (String) parameters.nextElement();
            JSONObject parameter_json = new JSONObject();
            JSONArray parameter_values_json = new JSONArray();
            if(parameter.toLowerCase().matches("^.*(password).*$")){
                parameter_values_json.add("REDACTED");
            }else{
                for(int i = 0; i < request.getParameterValues(parameter).length; i++){
                    parameter_values_json.add(request.getParameterValues(parameter)[i]);
                }
            }
            parameter_json.put(parameter, parameter_values_json);
            parameters_json.add(parameter_json);
        }
        content_json.put("parameters", parameters_json);

        //Misc stuff
        content_json.put("content_length", request.getContentLength());
        content_json.put("content_type", request.getContentType());
        content_json.put("protocol", request.getProtocol());

        try{
            Database db = Database.getInstance();
            return db.logger.insertAccessLog(request.getRemoteAddr(), request.getHeader("User-Agent"), request.getRequestURI(), request.getMethod(), Utility.encodeJSON(content_json), SessionDB.getSessionHashFromCookies(request.getCookies()));
        }catch(Exception e){
            e.getMessage();
            // Fail safe - stderr - access log id is not used
            System.err.println("FALLBACK: [" + LocalDateTime.now().toString() + "] "
            + request.getRemoteAddr() + " "
            + request.getHeader("User-Agent") + " "
            + request.getRequestURI() + " "
            + request.getMethod() + " "
            + Utility.encodeJSON(content_json));
        }
        return -1;
    }

    public static void updateAccessLog(int access_log_id, int status_code){
        try{
            if(access_log_id != -1){
                Database db = Database.getInstance();
                db.logger.updateAccessLog(access_log_id, status_code);
            }
        }catch(Exception e){}
    }
}
