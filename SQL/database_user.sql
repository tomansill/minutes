/* Creates user named minutes_db_user */
DROP USER IF EXISTS minutes_db_user;
CREATE USER minutes_db_user WITH ENCRYPTED PASSWORD ''; /* don't forget to set password here */

/* Default permissions - shouldn't be able to do anything except use functions */
REVOKE ALL ON minutes_database FROM minutes_db_user;
