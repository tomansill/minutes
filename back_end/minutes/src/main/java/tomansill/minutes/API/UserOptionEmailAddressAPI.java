package tomansill.minutes;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.nio.file.Path;
import java.io.File;

import java.util.Map;
import java.util.List;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

import java.io.IOException;

import java.util.regex.Pattern;

import javax.servlet.http.Cookie;

import java.util.Date;

/** Class that services or route requests that relates to an User
 *  @author Thomas Ansill
 */
public class UserOptionEmailAddressAPI{


    /** Database object */
    private Database db;

    /** Constuctor */
    public UserOptionEmailAddressAPI() throws Exception{
        this.db = Database.getInstance();
    }

    /** Function that services the HTTP request and possibly route them further
     *  @param user_hash user hash
     *  @param path URL path
     *  @param request HTTP request
     *  @param response HTTP response to respond to the user
     */
    public void processRequest(Path path, String user_hash, HttpServletRequest request, HttpServletResponse response) throws Exception{

        // Pop the hash
        path = (path.getNameCount() > 1 ? path.subpath(1, path.getNameCount()) : new File("").toPath());

        // Check if we have any left on path
        if(path.getNameCount() > 0 && !path.toString().trim().equals("")){
            // This endpoint is deadend, nowhere to go, if attempt to go further, send bad request
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }else{
            // Check request method
            if(request.getMethod().equals("GET")){
                //default_get(user_hash, request, response);
            }else if(request.getMethod().equals("OPTIONS")){
                //default_options(user_hash, request, response);
            }else if(request.getMethod().equals("PUT")){
                //default_put(user_hash, request, response);
            }else{
                response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "Only GET, OPTIONS, and PUT Request Methods are allowed on this endpoint.");
                return;
            }
        }
    }
}
