CREATE OR REPLACE FUNCTION get_error_logs(session SESSION_TYPE, in_level ERROR_SEVERITY, page_limit INT, page_number INT) RETURNS TABLE(
    id INT,
    level ERROR_SEVERITY,
    date_logged TIMESTAMP,
    caused_by INT,
    description TEXT
) AS $$
BEGIN

    RETURN QUERY (SELECT * FROM get_error_logs(session, in_level, page_limit, page_number, NULL, NULL));

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

CREATE OR REPLACE FUNCTION get_error_logs(session SESSION_TYPE, in_level ERROR_SEVERITY, page_limit INT, page_number INT, start_date TIMESTAMP, end_date TIMESTAMP) RETURNS TABLE(
    id INT,
    level ERROR_SEVERITY,
    date_logged TIMESTAMP,
    caused_by INT,
    description TEXT
) AS $$
BEGIN

    /* Validate session */
    IF NOT is_session_alive(session, TRUE) THEN RAISE EXCEPTION '{"code":"ERR_INVALID_SESSION", "message":"session is invalid!"}'; END IF;

    /* Validate page_limit */
    IF page_limit IS NULL OR page_limit < 1 THEN RAISE EXCEPTION '{"code":"ERR_INVALID_PAGE_LIMIT", "message":"page_limit is invalid!"}'; END IF;

    /* Validate page_number */
    IF page_number IS NULL OR page_number < 0  THEN RAISE EXCEPTION '{"code":"ERR_INVALID_PAGE_NUMBER", "message":"page_number is invalid!"}'; END IF;

    /* Validate dates */
    IF start_date IS NOT NULL AND end_date IS NOT NULL AND start_date > end_date THEN
        RAISE EXCEPTION '{"code":"ERR_INVALID_DATES", "message":"The start date is older than end date!"}';
    END IF;

    /* If start date is null, set it to earliest possible */
    IF start_date IS NULL THEN start_date := DATE('1753-1-1'); END IF; /* Earliest allowed by SQL */

    /* If end date is null, set it to latest possible */
    IF end_date IS NULL THEN end_date := NOW() + INTERVAL '10 minutes'; END IF; /* why would there be any future logs? :) */

    /* Check privileges */
    IF access_control_check_access(session, 'view_error_logs') THEN


        RETURN QUERY (
            SELECT
                Error_Log.id,
                Error_Log.level,
                Error_Log.date_logged,
                Error_Log.caused_by,
                Error_Log.description
            FROM Error_Log
            WHERE
                start_date < Error_Log.date_logged AND
                end_date > Error_Log.date_logged AND
                (in_level IS NULL OR Error_Log.level = in_level) /* if specified level is null then this search term don't apply */
            ORDER BY Error_Log.date_logged DESC
            LIMIT page_limit OFFSET (page_limit*page_number)
        );
    ELSE
        RAISE EXCEPTION '{"code":"ERR_NOT_PERMITTED", "message":"Not permitted to view the error logs!"}';
    END IF;
END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

CREATE OR REPLACE FUNCTION get_access_logs(session SESSION_TYPE, page_limit INT, page_number INT) RETURNS TABLE(
    id INT,
    agent_id INT,
    resource_id INT,
    patron_email_address EMAIL_ADDRESS_TYPE,
    patron_hash PATRON_HASH_TYPE,
    date_logged TIMESTAMP,
    content TEXT
) AS $$
BEGIN

    WITH results as (SELECT id, agent_id, resource_id, patron_id, date_logged, content FROM get_access_logs(session, page_limit, page_number, NULL, NULL),

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

CREATE OR REPLACE FUNCTION get_access_logs(session SESSION_TYPE, in_agent_id INT, in_resource_id INT, email_address VARCHAR(128), searchTerm VARCHAR(128), page_limit INT, page_number INT, start_date TIMESTAMP, end_date TIMESTAMP) RETURNS TABLE(
    id INT,
    agent_id INT,
    resource_id INT,
    patron_hash VARCHAR,
    patron_email VARCHAR,
    date_logged TIMESTAMP,
    content TEXT
) AS $$
BEGIN

    /* Check if email is valid */
    IF email_address IS NOT NULL AND NOT is_email_address_valid(RTRIM(email_address)) THEN RAISE EXCEPTION '{"code":"ERR_INVALID_EMAIL_ADDRESS", "message":"email_address is invalid!"}'; END IF;

    DECLARE
        in_patron_id INT := NULL;
    BEGIN
        IF email_address IS NOT NULL THEN in_patron_id := (SELECT id FROM get_patron_internal(email_address::EMAIL_ADDRESS_TYPE)); END IF;

        RETURN QUERY
        (
            WITH result AS (SELECT * FROM get_access_logs(session, in_agent_id, in_resource_id, in_patron_id, searchTerm, page_limit, page_number, start_date, end_date))
            SELECT
                cleaned.id,
                cleaned.agent_id,
                cleaned.resource_id,
                cleaned.patron_hash,
                cleaned.patron_email,
                cleaned.date_logged,
                cleaned.content
                FROM (
                    SELECT
                        result.id,
                        result.agent_id,
                        result.resource_id,
                        (SELECT hash FROM get_patron_internal(result.patron_id)) AS patron_hash,
                        (SELECT address FROM get_patron_internal(result.patron_id)) AS patron_email,
                        result.date_logged,
                        result.content
                    FROM result WHERE result.patron_id IS NOT NULL
                    UNION ALL
                    SELECT
                        result.id,
                        result.agent_id,
                        result.resource_id,
                        NULL::VARCHAR AS patron_hash,
                        NULL::VARCHAR AS patron_email,
                        result.date_logged,
                        result.content
                     FROM result WHERE result.patron_id IS NOT NULL
                ) AS cleaned
        );
    END;

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

CREATE OR REPLACE FUNCTION get_resource_logs(session SESSION_TYPE, searchTerm VARCHAR(128), in_method VARCHAR(10), page_limit INT, page_number INT) RETURNS TABLE(
    id INT,
    url TEXT,
    method VARCHAR(10),
    count_accessed INT
) AS $$
BEGIN

    /* Validate session */
    IF NOT is_session_alive(session, TRUE) THEN RAISE EXCEPTION '{"code":"ERR_INVALID_SESSION", "message":"session is invalid!"}'; END IF;

    /* Validate page_limit */
    IF page_limit IS NULL OR page_limit < 1 THEN RAISE EXCEPTION '{"code":"ERR_INVALID_PAGE_LIMIT", "message":"page_limit is invalid!"}'; END IF;

    /* Validate page_number */
    IF page_number IS NULL OR page_number < 0  THEN RAISE EXCEPTION '{"code":"ERR_INVALID_PAGE_NUMBER", "message":"page_number is invalid!"}'; END IF;

    /* Check privileges */
    IF access_control_check_access(session, 'view_access_logs') THEN
        RETURN QUERY (
            SELECT
                Access_Resource.id,
                Access_Resource.url,
                Access_Resource.method,
                Access_Resource.count_accessed
            FROM Access_Resource
            WHERE
                (in_method IS NULL OR Access_Resource.method = in_method) AND /* Always true if in_method is NULL otherwise apply search terms */
                (searchTerm IS NULL OR Access_Resource.url LIKE CONCAT('%', CONCAT(searchTerm, '%')))
            LIMIT page_limit OFFSET (page_limit*page_number)
        );
    ELSE
        RAISE EXCEPTION '{"code":"ERR_NOT_PERMITTED", "message":"Not permitted to view the error logs!"}';
    END IF;
END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

CREATE OR REPLACE FUNCTION get_agent_logs(session SESSION_TYPE, ip_address_search VARCHAR(39), user_agent_search VARCHAR(128), page_limit INT, page_number INT) RETURNS TABLE(
    id INT,
    ip_address VARCHAR(39),
    user_agent TEXT,
    count_requests INT
) AS $$
BEGIN

    /* Validate session */
    IF NOT is_session_alive(session, TRUE) THEN RAISE EXCEPTION '{"code":"ERR_INVALID_SESSION", "message":"session is invalid!"}'; END IF;

    /* Validate page_limit */
    IF page_limit IS NULL OR page_limit < 1 THEN RAISE EXCEPTION '{"code":"ERR_INVALID_PAGE_LIMIT", "message":"page_limit is invalid!"}'; END IF;

    /* Validate page_number */
    IF page_number IS NULL OR page_number < 0  THEN RAISE EXCEPTION '{"code":"ERR_INVALID_PAGE_NUMBER", "message":"page_number is invalid!"}'; END IF;

    /* Check privileges */
    IF access_control_check_access(session, 'view_access_logs') THEN
        RETURN QUERY (
            SELECT
                Access_Agent.id,
                Access_Agent.ip_address,
                Access_Agent.user_agent,
                Access_Agent.count_requests
            FROM Access_Agent
            WHERE
                (ip_address_search IS NULL OR Access_Agent.ip_address LIKE CONCAT('%', CONCAT(ip_address_search, '%'))) AND /* Always true if ip_address_search is NULL otherwise apply search terms */
                (user_agent_search IS NULL OR Access_Agent.user_agent LIKE CONCAT('%', CONCAT(user_agent_search, '%')))
            LIMIT page_limit OFFSET (page_limit*page_number)
        );
    ELSE
        RAISE EXCEPTION '{"code":"ERR_NOT_PERMITTED", "message":"Not permitted to view the error logs!"}';
    END IF;
END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;


CREATE OR REPLACE FUNCTION get_known_associations_logs(session SESSION_TYPE, in_agent_id INT, email_address VARCHAR(128), page_limit INT, page_number INT) RETURNS TABLE(
    id INT,
    agent_id INT,
    patron_hash VARCHAR,
    patron_email VARCHAR
) AS $$
BEGIN

    /* Check if email is valid */
    IF email_address IS NOT NULL AND NOT is_email_address_valid(RTRIM(email_address)) THEN RAISE EXCEPTION '{"code":"ERR_INVALID_EMAIL_ADDRESS", "message":"email_address is invalid!"}'; END IF;

    DECLARE
        in_patron_id INT := NULL;
    BEGIN
        IF email_address IS NOT NULL THEN in_patron_id := (SELECT id FROM get_patron_internal(email_address::EMAIL_ADDRESS_TYPE)); END IF;

        RETURN QUERY
        (
            WITH result AS (SELECT * FROM get_known_associations_logs(session, in_agent_id, in_patron_id, page_limit, page_number))
            SELECT
                cleaned.id,
                cleaned.agent_id,
                cleaned.patron_hash,
                cleaned.patron_email
                FROM (
                    SELECT
                        result.id,
                        result.agent_id,
                        (SELECT hash FROM get_patron_internal(result.patron_id)) AS patron_hash,
                        (SELECT address FROM get_patron_internal(result.patron_id)) AS patron_email
                    FROM result WHERE result.patron_id IS NOT NULL
                    UNION ALL
                    SELECT
                        result.id,
                        result.agent_id,
                        NULL::VARCHAR AS patron_hash,
                        NULL::VARCHAR AS patron_email
                     FROM result WHERE result.patron_id IS NOT NULL
                ) AS cleaned
        );
    END;

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;
