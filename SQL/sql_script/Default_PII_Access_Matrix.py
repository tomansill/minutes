"""
Main method - to use, you need a spreadsheet with values filled out

Example:

|--------------------------------------------------------|
| From\To    | no_login | login  |  member |  more_users |
|--------------------------------------------------------|
| login      | never    | never  | opt     | always      |
|--------------------------------------------------------|
| member     | never    | opt    | always  | always      |
|--------------------------------------------------------|
| more_users | always   | always | always  | never       |
|--------------------------------------------------------|

Note: topleft-most cell will be ignored, you can type whatever in it and it will be ignored

Note: besides the very first cell, all elements on either top row or left column are roles

Note:   always - always permit access
        never - never permit access
        opt - optional (depends on user's privacy settings)

To understand this table, you start with left column, pick a user, for example "member".
Pretend that this "member" is you, role that you are trying to access is on top row,
the status of that access is located at where both you and other role intersects.
For example, with 'member', if you are trying to access an user with "login",
the access status will say 'opt'

More examples:
login to member is opt
member to no_login is never
more_users to more_users is never

Once you're satisifed with your spreadsheet, select all appropriate cells, copy it
Run this program, type in method name you want to use and press enter
Then paste what you just copied from spreadsheet and press enter
type 'q' and press enter again

It should print out SQL query. Select that and use that.

"""
def main():
    method = input("type method in here: ")
    matrix = []
    print("paste CSV in here! type q to end")
    while(True):
        csv_input = input("")
        if csv_input == "q":
            break
        row = csv_input.split()
        matrix.append(row)

    sql_query = "INSERT INTO Default_PII_Access_Matrix (accessor, target, method, access) VALUES\n"

    for x in range (1, len(matrix)):
        for y in range (1, len(matrix[x])):
            sql_query += "('" + matrix[0][y] + "','" + matrix[x][0] + "','" + method + "','" + matrix[x][y].upper() + "'),\n"

    print("here's your SQL query: \n" + sql_query);

main()
