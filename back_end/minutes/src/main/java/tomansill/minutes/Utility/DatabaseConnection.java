package tomansill.minutes;

import java.sql.*;
import java.util.Properties;

/** Database Connection object
 *  Object that handles the database initialization
 *  @author Thomas Ansill
 */
public class DatabaseConnection{

    /** Connection */
    public Connection connection;

    /** Flag to indicate if the connection is open */
    private boolean open = false;

    /** hostname */
    private String host;

    /** database name */
    private String database;

    /** port number */
    private int port;

    /** username for database */
    private String username;

    /** password for user */
    private String password;

    /** Constructor for DatabaseConnection without port
     *  Port will be default to Postgresql's default
     *  @param host hostname
     *  @param database database name
     *  @param username username of database user
     *  @param password password of database user
     */
    public DatabaseConnection(String host, String database, String username, String password){
        this(host, database, -1, username, password);
    }

    /** Full Constructor for DatabaseConnection
     *  @param host hostname
     *  @param database database name
     *  @param port port number for database
     *  @param username username of database user
     *  @param password password of database user
     */
    public DatabaseConnection(String host, String database, int port, String username, String password){
        this.host = host;
        this.database = database;
        this.port = port;
        this.username = username;
        this.password = password;
    }

    /** Opens the connection to the database
     *  @throws Exception thrown if there are any problems with opening a connection
     */
    public void open() throws Exception{
        if(!open){
            // Load the Driver
            Class.forName("org.postgresql.Driver");

            // Set up the URL
            String url = "jdbc:postgresql://" + this.host;
            if(port != -1) url += ":" + port;
            url += "/" + database;

            // Set the properties
            Properties properties = new Properties();
            properties.setProperty("user", username);
            properties.setProperty("password", password);
            //properties.setProperty("ssl", "true");

            // Connect
            connection = DriverManager.getConnection(url, properties);
            open = true;
        }
    }

    /** Closes the connection to the database
     *  @throws Exception thrown if there are any problems with closing a connection
     */
    public void close() throws Exception{
        if(open) connection.close();
        open = false;
    }
}
