package tomansill.minutes;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.file.Path;
import java.io.File;
import java.util.Map;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import java.io.IOException;
import java.util.regex.Pattern;
import javax.servlet.http.Cookie;
import java.util.Date;

/** Class that services or route requests that relates to an User
 *  @author Thomas Ansill
 */
public class UserOptionIdentificationAPI{

    /** Server config */
    private Config server_config;

    /** Database object */
    private Database db;

    /** Constuctor */
    public UserOptionIdentificationAPI(Config server_config) throws Exception{
        this.server_config = server_config;
        this.db = Database.getInstance();
    }

    /** Function that services the HTTP request and possibly route them further
     *  @param user_hash user hash
     *  @param path URL path
     *  @param request HTTP request
     *  @param response HTTP response to respond to the user
     */
    public void processRequest(PathList path, String user_hash, HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{

        // There must be no more left on the path
        if(path.size() == 0){
            // Check user_hash
            if(user_hash == null){
                // Get and check session
                String session_hash = this.db.session.getSessionHashFromCookies(request.getCookies());
                if(!this.db.session.isSessionAlive(session_hash)){
                    // Destroys the session hash in cookie
                    response.addCookie(SessionDB.destroyCookie());
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "You are not logged in!");
                    return;
                }

                // Get hash
                user_hash = this.db.session.getUserHashFromSession(session_hash);
            }

            // Check request method
            if(request.getMethod().equals("GET")){
                default_get(user_hash, request, response);
            }else if(request.getMethod().equals("OPTIONS")){
                default_options(user_hash, request, response);
            }else if(request.getMethod().equals("PUT")){
                default_put(user_hash, request, response);
            }else{
                response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "Only GET, OPTIONS, and PUT Request Methods are allowed on this endpoint.");
            }
        }
    }

    private void default_get(String user_hash, HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{

        // Check if user is logged in
        String session_hash = this.db.session.getSessionHashFromCookies(request.getCookies());

        // Get names
        JSONObject results = this.db.user.getUserNames(session_hash, user_hash);

        // Get birth date
        results.put("birth_date", this.db.user.getUserBirthDate(session_hash, user_hash));

        // Get privacy settings
        results.put("privacy", this.db.user.getUserIdentificationPrivacy(session_hash, user_hash));

        // OK
        response.setStatus(HttpServletResponse.SC_OK);
        response.setContentType("text/json; charset=utf-8");
        response.getWriter().print(Utility.encodeJSON(results));
    }

    private void default_options(String user_hash, HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{

        // Check if user is logged in
        String session_hash = this.db.session.getSessionHashFromCookies(request.getCookies());

        // Get names
        JSONObject results = this.db.user.getUserIdentificationPrivileges(session_hash, user_hash);

        // OK
        response.setStatus(HttpServletResponse.SC_OK);
        response.setContentType("text/json; charset=utf-8");
        response.getWriter().print(Utility.encodeJSON(results));
    }

    private void default_put(String user_hash, HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{

        // Check if user is logged in
        String session_hash = this.db.session.getSessionHashFromCookies(request.getCookies());

        // Counter to look out for invalid parameters
        int count = 0;

        // Verify inputs
        if(request.getParameter("title") != null) count++;
        if(request.getParameter("first_name") != null) count++;
        if(request.getParameter("middle_name") != null) count++;
        if(request.getParameter("last_name") != null) count++;
        if(request.getParameter("nick_name") != null) count++;
        if(request.getParameter("suffix") != null) count++;
        if(request.getParameter("birth_date") != null) count++;

        // Check if this input is truly boolean (null allowed)
        Boolean private_name = null;
        if(request.getParameter("private_name") != null){
            count++;
            try{
                private_name = Boolean.valueOf(request.getParameter("private_name"));
            }catch(Exception e){
                Utility.sendError(response, 422, "E8", "Invalid option on name privacy setting");
                return;
            }
        }

        // Check if this input is truly boolean (null allowed)
        Boolean private_middle_name = null;
        if(request.getParameter("private_middle_name") != null){
            count++;
            try{
                private_middle_name = Boolean.valueOf(request.getParameter("private_middle_name"));
            }catch(Exception e){
                Utility.sendError(response, 422, "E9", "Invalid option on middle name privacy setting");
                return;
            }
        }

        // Check if this input is truly boolean (null allowed)
        Boolean private_birth_date = null;
        if(request.getParameter("private_birth_date") != null){
            count++;
            try{
                private_birth_date = Boolean.valueOf(request.getParameter("private_birth_date"));
            }catch(Exception e){
                Utility.sendError(response, 422, "E10", "Invalid option on birth date privacy setting");
                return;
            }
        }

        // Check if there's no foreign parameter
        if(count != request.getParameterMap().size()){
            Utility.sendError(response, HttpServletResponse.SC_BAD_REQUEST, "E11", "Unrecognized parameter names in the request!");
            return;
        }

        // Go and update the identification info
        try{
            if(this.db.user.updateUserIdentification(
                session_hash,
                user_hash,
                request.getParameter("title"),
                request.getParameter("first_name"),
                request.getParameter("middle_name"),
                request.getParameter("last_name"),
                request.getParameter("nick_name"),
                request.getParameter("suffix"),
                Utility.convertToDate(request.getParameter("suffix")),
                private_name,
                private_middle_name,
                private_birth_date
            )){
                response.setStatus(HttpServletResponse.SC_ACCEPTED);
            }else{
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            }
        }catch(SQLException exception){

            // Attempt to interpret the exception
            SQLExceptionResponse res = SQLExceptionResponse.interpret(exception);

            // If cannot be interpreted, throw it upstream
            if(res == null) throw exception;

            // Otherwise, continue
            if(res.getCode().equals("ERR_INVALID_SESSION")){
                Utility.sendError(response, 422, "E1", "Login session is invalid.");
            }else if(res.getCode().equals("ERR_INVALID_EMAIL_ADDRESS")){
                Utility.sendError(response, 422, "E2", "Username is not a valid email address.");
            }else if(res.getCode().equals("ERR_INVALID_TITLE")){
                Utility.sendError(response, 422, "E3", "Value used for name title is invalid.");
            }else if(res.getCode().equals("ERR_INVALID_SUFFIX")){
                Utility.sendError(response, 422, "E4", "Value used for name suffix is invalid.");;
            }else if(res.getCode().equals("ERR_INVALID_FIRST_NAME")){
                Utility.sendError(response, 422, "E5", "First name is too short.");
            }else if(res.getCode().equals("ERR_INVALID_MIDDLE_NAME")){
                Utility.sendError(response, 422, "E6", "Middle name is too short.");
            }else if(res.getCode().equals("ERR_INVALID_LAST_NAME")){
                Utility.sendError(response, 422, "E7", "Last name is too short.");
            }else if(res.getCode().equals("ERR_INVALID_NICK_NAME")){
                Utility.sendError(response, 422, "E8", "Nick name is too short.");
            }else if(res.getCode().equals("ERR_BIRTH_TOO_YOUNG")){
                Utility.sendError(response, 422, "E9", "Birth date is invalid. User needs to be 18 years or older.");
            }else if(res.getCode().equals("ERR_EMPTY_PARAMETERS")){
                Utility.sendError(response, 422, "E10", "All parameters are NULL!");
            }else throw exception; // Throw it upstream
        }
    }
}
