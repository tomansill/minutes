CREATE OR REPLACE FUNCTION get_access_logs(session SESSION_TYPE, in_agent_id INT, in_resource_id INT, in_patron_id INT, searchTerm VARCHAR(128), page_limit INT, page_number INT, start_date TIMESTAMP, end_date TIMESTAMP) RETURNS TABLE(
    id INT,
    agent_id INT,
    resource_id INT,
    patron_id INT,
    date_logged TIMESTAMP,
    content TEXT
) AS $$
BEGIN

    /* Validate session */
    IF NOT is_session_alive(session, TRUE) THEN RAISE EXCEPTION '{"code":"ERR_INVALID_SESSION", "message":"session is invalid!"}'; END IF;

    /* Validate page_limit */
    IF page_limit IS NULL OR page_limit < 1 THEN RAISE EXCEPTION '{"code":"ERR_INVALID_PAGE_LIMIT", "message":"page_limit is invalid!"}'; END IF;

    /* Validate page_number */
    IF page_number IS NULL OR page_number < 0  THEN RAISE EXCEPTION '{"code":"ERR_INVALID_PAGE_NUMBER", "message":"page_number is invalid!"}'; END IF;

    /* Validate dates */
    IF start_date IS NOT NULL AND end_date IS NOT NULL AND start_date > end_date THEN
        RAISE EXCEPTION '{"code":"ERR_INVALID_DATES", "message":"The start date is older than end date!"}';
    END IF;

    /* If start date is null, set it to earliest possible */
    IF start_date IS NULL THEN start_date := DATE('1753-1-1'); END IF; /* Earliest allowed by SQL */resource

    /* If end date is null, set it to latest possible */
    IF end_date IS NULL THEN end_date := NOW() + INTERVAL '10 minutes'; END IF; /* why would there be any future logs? :) */

    /* Check privileges */
    IF access_control_check_access(session, 'view_access_logs') THEN
        RETURN QUERY (
            SELECT
                Access_Log.id,
                Access_Log.agent_id,
                Access_Log.resource_id,
                Access_Log.patron_id,
                Access_Log.date_logged,
                Access_Log.content
            FROM Access_Log
            WHERE
                start_date < Access_Log.date_logged AND
                end_date > Access_Log.date_logged AND
                (in_agent_id IS NULL OR Access_Log.agent_id = in_agent_id) AND /* Always true if in_agent_id is NULL otherwise apply search terms */
                (in_resource_id IS NULL OR Access_Log.resource_id = in_resource_id) AND
                (in_patron_id IS NULL OR (in_patron_id = -1 AND Access_Log.patron_id IS NULL) OR (NOT in_patron_id = -1 AND Access_Log.patron_id = in_patron_id)) AND
                (searchTerm IS NULL OR Access_Log.content LIKE CONCAT('%', CONCAT(searchTerm, '%')))
            ORDER BY Access_Log.date_logged DESC
            LIMIT page_limit OFFSET (page_limit*page_number)
        );
    ELSE
        RAISE EXCEPTION '{"code":"ERR_NOT_PERMITTED", "message":"Not permitted to view the error logs!"}';
    END IF;
END; $$
LANGUAGE PLPGSQL SECURITY INVOKER;

CREATE OR REPLACE FUNCTION get_known_associations_logs(session SESSION_TYPE, in_agent_id INT, in_patron_id INT, page_limit INT, page_number INT) RETURNS TABLE(
    id INT,
    agent_id INT,
    patron_id INT
) AS $$
BEGIN

    /* Validate session */
    IF NOT is_session_alive(session, TRUE) THEN RAISE EXCEPTION '{"code":"ERR_INVALID_SESSION", "message":"session is invalid!"}'; END IF;

    /* Validate page_limit */
    IF page_limit IS NULL OR page_limit < 1 THEN RAISE EXCEPTION '{"code":"ERR_INVALID_PAGE_LIMIT", "message":"page_limit is invalid!"}'; END IF;

    /* Validate page_number */
    IF page_number IS NULL OR page_number < 0  THEN RAISE EXCEPTION '{"code":"ERR_INVALID_PAGE_NUMBER", "message":"page_number is invalid!"}'; END IF;

    /* Check privileges */
    IF access_control_check_access(session, 'view_access_logs') THEN
        RETURN QUERY (
            SELECT
                Known_Association.id,
                Known_Association.agent_id,
                Known_Association.patron_id
            FROM Known_Association
            WHERE
                (in_agent_id IS NULL OR Known_Association.agent_id = in_agent_id) AND /* Always true if in_agent_id is NULL otherwise apply search terms */
                (in_patron_id IS NULL OR Known_Association.patron_id = in_patron_id)
            LIMIT page_limit OFFSET (page_limit*page_number)
        );
    ELSE
        RAISE EXCEPTION '{"code":"ERR_NOT_PERMITTED", "message":"Not permitted to view the error logs!"}';
    END IF;
END; $$
LANGUAGE PLPGSQL SECURITY INVOKER;
