#!/bin/bash

cd back_end/minutes

#check if dependencies is not pulled in yet
if [ ! -d "target/dependency" ]; then
	mvn dependency:copy-dependencies
fi

if test $? -eq 0
then
	mvn package
	if test $? -eq 0
	then
		clear
		echo "Compilation successful"
		#cd target
		#jarname=$(ls *.jar)
		cd ../..
		./run_backend.sh
	else
		echo "Compilation failed"
	fi
else
	echo "Could not load dependencies"
	echo "Compilation failed"
fi
