package tomansill.minutes;

import org.eclipse.jetty.servlet.DefaultServlet;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.logging.Level;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoggedDefaultServlet extends DefaultServlet{
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{

        // Log the request
        int access_log_id = LoggerUtil.logAccess(request);

        try{

            // Run the request to a proper servlet
            super.service(request, response);

            // Log the status code
            LoggerUtil.updateAccessLog(access_log_id, response.getStatus());

        }catch(ServletException exception){

            // Log the status code here if previous one has failed
            LoggerUtil.updateAccessLog(access_log_id, response.getStatus());

            // Log the failure
            LoggerUtil.logError(Level.SEVERE, exception, access_log_id);

             // Send it upstream - not our responsibility to fix stuff, we just log them
             throw exception;

        }catch(IOException exception){

            // Log the status code here if previous one has failed
            LoggerUtil.updateAccessLog(access_log_id, response.getStatus());

            // Log the failure
            LoggerUtil.logError(Level.SEVERE, exception, access_log_id);

             // Send it upstream - not our responsibility to fix stuff, we just log them
             throw exception;
        }
    }
}
