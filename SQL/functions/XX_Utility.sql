CREATE OR REPLACE FUNCTION is_password_valid(password TEXT) RETURNS BOOLEAN AS $$
BEGIN

    /* If null, return false */
    IF password = NULL THEN RETURN FALSE; END IF;

    /* Simple */
    RETURN LENGTH(RTRIM(password)) > 8;
END; $$
LANGUAGE PLPGSQL IMMUTABLE SECURITY DEFINER;

CREATE OR REPLACE FUNCTION is_birth_date_reasonable(birth_date DATE) RETURNS BOOLEAN AS $$
BEGIN

    /* If null, return false */
    IF birth_date = NULL THEN RETURN FALSE; END IF;

    /* Check if 18 years or older */
    RETURN AGE(NOW(), birth_date) > INTERVAL '18 years';
END; $$
LANGUAGE PLPGSQL IMMUTABLE SECURITY DEFINER;

CREATE OR REPLACE FUNCTION is_email_address_valid(email_address VARCHAR(128)) RETURNS BOOLEAN AS $$
BEGIN

    /* If null, return false */
    IF email_address = NULL THEN RETURN FALSE; END IF;

    /* Check if it matches the regex */
    RETURN email_address ~ '(^\S+@\S+\.\S+$)';
END; $$
LANGUAGE PLPGSQL IMMUTABLE SECURITY DEFINER;

CREATE OR REPLACE FUNCTION is_name_valid(name VARCHAR(128)) RETURNS BOOLEAN AS $$
BEGIN

    /* If null, return false */
    IF name = NULL THEN RETURN FALSE; END IF;

    /* Check if it matches the regex */
    RETURN name ~ '^([a-z]|[A-Z]|[^0-9]|\\s)+$';
END; $$
LANGUAGE PLPGSQL IMMUTABLE SECURITY DEFINER;

CREATE OR REPLACE FUNCTION is_name_valid_null(name VARCHAR(128)) RETURNS BOOLEAN AS $$
BEGIN

    /* If null, return false */
    IF name = NULL THEN RETURN TRUE; END IF;

    /* Check if it matches the regex */
    RETURN name ~ '^([a-z]|[A-Z]|[^0-9]|\\s)+$';
END; $$
LANGUAGE PLPGSQL IMMUTABLE SECURITY DEFINER;

/** Helper function to check if input title is conforming to database's enum
 *  @param title - title in text
 *  @return true if valid otherwise false
 */
CREATE OR REPLACE FUNCTION is_title_valid(title VARCHAR(10)) RETURNS BOOLEAN AS $$
BEGIN
    RETURN EXISTS (SELECT * FROM unnest(enum_range(NULL::TITLE)) WHERE unnest::text = title);
END; $$
LANGUAGE PLPGSQL IMMUTABLE SECURITY DEFINER;

/** Helper function to check if input suffix is conforming to database's enum
 *  @param suffix - suffix in text
 *  @return true if valid otherwise false
 */
CREATE OR REPLACE FUNCTION is_suffix_valid(suffix VARCHAR(10)) RETURNS BOOLEAN AS $$
BEGIN
    RETURN EXISTS (SELECT * FROM unnest(enum_range(NULL::SUFFIX)) WHERE unnest::text = suffix);
END; $$
LANGUAGE PLPGSQL IMMUTABLE SECURITY DEFINER;

/** Helper function to check if input email address status is conforming to database's enum
 *  @param suffix - suffix in text
 *  @return true if valid otherwise false
 */
CREATE OR REPLACE FUNCTION is_email_address_status_valid(status VARCHAR(10)) RETURNS BOOLEAN AS $$
BEGIN
    RETURN EXISTS (SELECT * FROM unnest(enum_range(NULL::EMAIL_ADDRESS_STATUS)) WHERE unnest::text = status);
END; $$
LANGUAGE PLPGSQL IMMUTABLE SECURITY DEFINER;

/** Helper function to get database space usage in bytes
 *  https://wiki-bsse.ethz.ch/display/ITDOC/Check+size+of+tables+and+objects+in+PostgreSQL+database
 *  @return table containing table name, size and external size
 */
CREATE OR REPLACE FUNCTION get_table_space_usage() RETURNS TABLE(
    table_name VARCHAR,
    size BIGINT,
    ext_size BIGINT
) AS $$
BEGIN
    RETURN QUERY (SELECT
        relname::VARCHAR as table_name,
        pg_total_relation_size(relid)::BIGINT As size,
        (pg_total_relation_size(relid) - pg_relation_size(relid))::BIGINT as ext_size
    FROM pg_catalog.pg_statio_user_tables);
END; $$
LANGUAGE PLPGSQL IMMUTABLE SECURITY DEFINER;
