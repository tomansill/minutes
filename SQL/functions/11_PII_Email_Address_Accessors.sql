/** Function to return email addresses of specified patron
 *  @param session session of current logged-in patron
 *  @param patron_hash hash of patron to access
 *  @return empty set if not permitted, otherwise return email addresses
 */
CREATE OR REPLACE FUNCTION get_patron_information_email_address(session SESSION_TYPE, email_address EMAIL_ADDRESS_TYPE) RETURNS TABLE(
    hash EMAIL_ADDRESS_HASH_TYPE,
    address EMAIL_ADDRESS_TYPE,
    status EMAIL_ADDRESS_STATUS,
    login BOOLEAN,
    private BOOLEAN
) AS $$

	RETURN QUERY (SELECT * FROM get_patron_information_primary_email_address(session, (SELECT hash FROM get_patron(email_address))));

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

/** Function to return email addresses of specified patron
 *  @param session session hash of current logged-in patron
 *  @param patron_hash hash of patron to access
 *  @return empty set if not permitted, otherwise return email addresses
 */
CREATE OR REPLACE FUNCTION get_patron_information_email_address(session SESSION_TYPE, patron_hash PATRON_HASH_TYPE) RETURNS TABLE(
    hash EMAIL_ADDRESS_HASH_TYPE,
    address EMAIL_ADDRESS_TYPE,
    status EMAIL_ADDRESS_STATUS,
    login BOOLEAN,
    private BOOLEAN
) AS $$
BEGIN

	DECLARE
		access PII_ACCESS_ENUM := (SELECT get_patron_information_access(session, patron_hash, 'PII_email_address'::VARCHAR));
		in_patron_id INT := (SELECT id FROM get_patron_internal(patron_hash));
        in_session_id INT := (SELECT id FROM identify_patron_from_session_internal(session));
	BEGIN

        IF in_session_id = in_patron_id OR access_control_check_access(session, 'PII_write') THEN

            RETURN QUERY (SELECT ea.hash, ea.address, ea.status, ea.login, ea.private FROM Email_Address ea WHERE ea.patron_id = in_patron_id AND ea.successor IS NULL AND ea.status != 'DELETED'); /*always fail - returns empty set */

		ELSIF access = 'NEVER' THEN/* If never, then return empty set */

            RETURN QUERY (SELECT ea.hash, ea.address, ea.status, NULL::BOOLEAN AS login, NULL::BOOLEAN AS private FROM Email_Address ea WHERE 1 = 0); /*always fail - returns empty set */

		ELSIF access = 'ALWAYS' THEN /* If always, return everything */

            RETURN QUERY (SELECT ea.hash, ea.address, ea.status, NULL::BOOLEAN AS login, NULL::BOOLEAN AS private FROM Email_Address ea WHERE ea.status != 'DELETED' AND ea.successor IS NULL AND ea.patron_id = in_patron_id);

        ELSE /* Return what we have */

    		RETURN QUERY (SELECT ea.hash, ea.address, ea.status, NULL::BOOLEAN AS login, NULL::BOOLEAN AS private FROM Email_Address WHERE ea.status != 'DELETED' AND ea.successor IS NULL AND ea.patron_id = in_patron_id AND NOT ea.private);

        END IF;

	END;

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;
