module.exports = function(grunt){
	grunt.initConfig({
		copy:{
			html:{
				expand: true,
				cwd: 'src/',
				src: '**/*.html',
				dest: "../../public/"
			},
			js:{
				expand: true,
				cwd: 'src/',
				src: '**/*.js',
				dest: "../../public/"
			}
		},
		sass:{
			scss:{
				expand: true,
				cwd: 'src/scss',
				src: '*.scss',
				dest: '../../public/css/',
				ext: '.css'
			}
		},
		watch:{
			html:{
				files: ['src/**/*.html'],
				tasks: ['copy:html']
			},
			scss:{
				files: ['src/scss/*.scss'],
				tasks: ['sass']
			},
			js:{
				files: ['src/js/*.js'],
				tasks:['copy:js']
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-sass');

	grunt.file.mkdir('build');
	grunt.registerTask('default', ['copy','sass','watch']);
}
