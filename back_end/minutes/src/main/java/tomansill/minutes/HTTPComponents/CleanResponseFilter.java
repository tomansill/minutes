package tomansill.minutes;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/** Class that serves as a filter to replace Jetty's HttpServletResponse with custom CleanHttpServletResponse
 *  @author Thomas Ansill
 */
public class CleanResponseFilter implements Filter {

    /** doFilter function
     *  @param req ServletRequest - We do not edit this at all
     *  @param res ServletResponse - We replace this response with one of our own
     *  @param chain FilterChain - We do not edit this at all
     *  @throws IOException thrown when ???
     *  @throws ServletException thrown when ???
     */
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        CleanHttpServletResponse response = new CleanHttpServletResponse((HttpServletResponse)res);
        chain.doFilter(req, response);
    }

    /** We have to include it here or java compiler will complain
     *  @param config FilterConfig - we don't use it at all
     *  @param ServletException never thrown?
     */
    public void init(FilterConfig config) throws ServletException{
        //empty
    }

    /** We have to include it here or java compiler will complain */
    public void destroy(){
        // empty
    }
}
