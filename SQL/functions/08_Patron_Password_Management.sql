/** Checks the patron's password with supplied password, if they match, return true otherwise FALSE
 *  @param email_address patron's email address
 *  @param in_password password
 *  @return boolean, true if match, otherwise FALSE
 *  @throws an exception if username or password are NULL
 */
CREATE OR REPLACE FUNCTION check_patron_password(email_address VARCHAR(128), in_password VARCHAR(128)) RETURNS BOOLEAN AS $$
BEGIN

    /* Check if email is not null */
    IF email_address IS NULL THEN RAISE EXCEPTION '{"code":"ERR_INVALID_EMAIL_ADDRESS", "message":"email_address is NULL!"}'; END IF;

    /* Check if email is not null */
    IF NOT is_email_address_valid(RTRIM(email_address)) THEN RAISE EXCEPTION '{"code":"ERR_INVALID_EMAIL_ADDRESS", "message":"email_address is invalid!"}'; END IF;

    RETURN (SELECT check_patron_password((SELECT hash FROM get_patron(email_address::EMAIL_ADDRESS_TYPE)), in_password));

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

/** Checks the patron's password with supplied password, if they match, return true otherwise FALSE
 *  @param email_address patron's email address
 *  @param in_password password
 *  @return boolean, true if match, otherwise FALSE
 *  @throws an exception if username or password are NULL
 */
CREATE OR REPLACE FUNCTION check_patron_password(email_address EMAIL_ADDRESS_TYPE, in_password VARCHAR(128)) RETURNS BOOLEAN AS $$
BEGIN

    RETURN (SELECT check_patron_password((SELECT hash FROM get_patron(email_address)), in_password));

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

/** Checks the patron's password with supplied password, if they match, return true otherwise FALSE
 *  @param hash patron's hash
 *  @param in_password password
 *  @return boolean, true if match, otherwise FALSE
 *  @throws an exception if username or password are NULL
 */
CREATE OR REPLACE FUNCTION check_patron_password(patron_hash PATRON_HASH_TYPE, in_password VARCHAR(128)) RETURNS BOOLEAN AS $$
BEGIN

    IF in_password IS NULL THEN RAISE EXCEPTION '{"code":"ERR_INVALID_PASSWORD", "message":"password is NULL!"}'; END IF;
    IF NOT is_password_valid(RTRIM(in_password)) THEN RAISE EXCEPTION '{"code":"ERR_PASSWORD_NOT_ACCEPTED", "message":"The password is too short!"}'; END IF;

    DECLARE
        in_patron_id INT = (SELECT id FROM get_patron_internal(patron_hash));
        user_salt VARCHAR(16) := (SELECT salt FROM Patron WHERE Patron.id = in_patron_id);
    BEGIN
        IF patron_exists(patron_hash) THEN
            RETURN (SELECT RTRIM(password_hash(in_password, user_salt)) = (SELECT Patron.password FROM Patron WHERE Patron.id = in_patron_id));
        ELSE
            RAISE EXCEPTION '{"code":"ERR_USER_HASH_NOT_FOUND", "message":"No Record : The patron_hash ''%'' does not exist in the system!"}', patron_hash;
        END IF;
    END;

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

/** Changes the patron's password - will drop any sessions associated to the user
 *  @param session Current login session
 *  @param email_address email address
 *  @param new_password new password
 *  @return true if password change is successful, otherwise return false
 *  Raises an exception if password are NULL
 *  Raises an exception if patron doesnt exist
 */
CREATE OR REPLACE FUNCTION change_patron_password(session SESSION_TYPE, email_address VARCHAR(128), new_password VARCHAR(128)) RETURNS BOOLEAN AS $$
BEGIN


    /* Check if email is not null */
    IF email_address IS NULL THEN RAISE EXCEPTION '{"code":"ERR_INVALID_EMAIL_ADDRESS", "message":"email_address is NULL!"}'; END IF;

    /* Check if email is not null */
    IF NOT is_email_address_valid(RTRIM(email_address)) THEN RAISE EXCEPTION '{"code":"ERR_INVALID_EMAIL_ADDRESS", "message":"email_address is invalid!"}'; END IF;


    RETURN (SELECT change_patron_password(session, (SELECT hash FROM get_patron(email_address::EMAIL_ADDRESS_TYPE)), new_password));

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

/** Changes the patron's password - will drop any sessions associated to the user
 *  @param session Current login session
 *  @param email_address email address
 *  @param new_password new password
 *  @return true if password change is successful, otherwise return false
 *  Raises an exception if password are NULL
 *  Raises an exception if patron doesnt exist
 */
CREATE OR REPLACE FUNCTION change_patron_password(session SESSION_TYPE, email_address EMAIL_ADDRESS_TYPE, new_password VARCHAR(128)) RETURNS BOOLEAN AS $$
BEGIN

    RETURN (SELECT change_patron_password(session, (SELECT hash FROM get_patron(email_address)), new_password));

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

/** Changes the patron's password - will drop any sessions associated to the user
 *  @param session current login session
 *  @param patron_hash patron's hash
 *  @param new_password new password
 *  @return true if password change is successful, otherwise return false
 *  Raises an exception if password are NULL
 *  Raises an exception if patron doesnt exist
 */
CREATE OR REPLACE FUNCTION change_patron_password(in_session SESSION_TYPE, patron_hash PATRON_HASH_TYPE, new_password VARCHAR(128)) RETURNS BOOLEAN AS $$
BEGIN

    /* verify input */
    IF new_password IS NULL THEN RAISE EXCEPTION '{"code":"ERR_INVALID_PASSWORD", "message":"new_password is NULL!"}'; END IF;
    IF NOT is_password_valid(RTRIM(new_password)) THEN RAISE EXCEPTION '{"code":"ERR_INVALID_PASSWORD", "message":"new_password is too short!"}'; END IF;

    /* Check session */
    IF NOT is_session_alive(in_session, FALSE) THEN RAISE EXCEPTION '{"code":"ERR_INVALID_SESSION", "message":"Session is not valid!"}'; END IF;

    /* Check if session is the patron */
    DECLARE
        patron_hash_from_session PATRON_HASH_TYPE := (SELECT hash FROM identify_patron_from_session(in_session));
        id_from_hash INT := (SELECT id FROM get_patron_internal(patron_hash));
        user_salt VARCHAR(16) := (SELECT salt FROM Patron WHERE Patron.id = id_from_hash);
    BEGIN
        /* Check if this user is allowed, could be admin or something */
        IF NOT patron_hash_from_session = patron_hash THEN
            IF NOT access_control_check_access(in_session, 'change_patron_password') THEN
                RETURN FALSE;
            END IF;
        END IF;

        IF patron_exists(patron_hash) THEN

            /* Update the passwords */
            UPDATE Patron SET password=RTRIM(password_hash(new_password, user_salt)) WHERE Patron.id = id_from_hash;
            UPDATE Patron SET password_date = now() WHERE Patron.id = id_from_hash;

            /* drop all sessions associated to patron */
            IF GLOBAL_password_change_renew_session() THEN
                DELETE FROM Session WHERE Session.patron_id = id_from_hash AND NOT Session.hash = in_session; /* Delete all other sessions except this one */
            ELSE
                DELETE FROM Session WHERE Session.patron_id = id_from_hash; /* Delete all sessions */
            END IF;

            /* Refresh the session expiration date */
            IF is_session_alive(in_session, TRUE) THEN END IF; /* do nothing */

            RETURN TRUE;
        ELSE
            RAISE EXCEPTION '{"code":"ERR_USER_HASH_NOT_FOUND", "message":"No Record : The patron_hash ''%'' does not exist in the system"}', patron_hash;
        END IF;
    END;

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

CREATE OR REPLACE FUNCTION is_2FA_enabled(session SESSION_TYPE, email_address EMAIL_ADDRESS_TYPE) RETURNS BOOLEAN AS  $$
BEGIN

    RETURN (SELECT is_2FA_enabled(session, (SELECT hash FROM get_patron(email_address))));

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

CREATE OR REPLACE FUNCTION is_2FA_enabled(session SESSION_TYPE, patron_hash PATRON_HASH_TYPE) RETURNS BOOLEAN AS  $$
BEGIN

    /* Check session */
    IF NOT is_session_alive(session, TRUE) THEN
        RAISE EXCEPTION '{"code":"ERR_NO_LOGIN", "message":"User is not logged in!"}';
    END IF;

    /* Check patron hash */
    IF NOT patron_exists(patron_hash) THEN
        RAISE EXCEPTION '{"code":"ERR_USER_HASH_NOT_FOUND", "message":"No Record : The patron_hash ''%'' does not exist in the system!"}', patron_hash;
    END IF;

    /* Extract ids */
    DECLARE
        session_id INT := (SELECT id FROM identify_patron_from_session_internal(session));
        in_patron_id INT := (SELECT ID FROM Patron WHERE hash = patron_hash);
    BEGIN
        IF  session_id = in_patron_id OR
            access_control_check_access(session, 'patron_modification')
        THEN
            RETURN (SELECT totp_secret FROM Patron WHERE id = in_patron_id) IS NOT NULL;
        END IF;
    END;

    RAISE EXCEPTION '{"code":"ERR_NOT_PERMITTED", "message":"Not privileged to view this information!"}';

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

CREATE OR REPLACE FUNCTION set_2FA(session SESSION_TYPE) RETURNS VARCHAR(64) AS $$
BEGIN

    /* Check session */
    IF is_session_alive(session, TRUE) THEN

        /* Get patron id and generate secret */
    	DECLARE
    		session_id INT := (SELECT id FROM identify_patron_from_session_internal(session));
    		secret VARCHAR(64) := (SELECT encode(gen_random_bytes(32), 'hex'));
    	BEGIN

            /* Delete any previous or old attempts if any */
            DELETE FROM Two_Factor_Authenication_Confirmation WHERE patron_id = session_id OR AGE(NOW(), date_created) > GLOBAL_2FA_timeout_interval();

            /* Add new 2fa code as unconfirmed */
            INSERT INTO Two_Factor_Authenication_Confirmation (patron_id, secret) VALUES (session_id, secret);

			RETURN secret;
    	END;
    ELSE
        RAISE EXCEPTION '{"code":"ERR_INVALID_SESSION", "message":"Invalid session!"}';
    END IF;
END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;

CREATE OR REPLACE FUNCTION confirm_2FA(session SESSION_TYPE, code VARCHAR(6)) RETURNS BOOLEAN AS $$
BEGIN

    /* Check if code is not null */
    IF code IS NULL THEN RAISE EXCEPTION '{"code":"ERR_INVALID_TWO_FACTOR_AUTH_CODE", "message":"code is NULL!"}'; END IF;

    /* Check session */
    IF is_session_alive(session, TRUE) THEN

        /* Delete any previous or old attempts if any */
        DELETE FROM Two_Factor_Authenication_Confirmation WHERE patron_id = session_id OR AGE(NOW(), date_created) > GLOBAL_2FA_timeout_interval();

        /* Get patron id and generate secret */
        DECLARE
            session_id INT := (SELECT id FROM identify_patron_from_session_internal(session));
            secret VARCHAR(64) := (SELECT secret FROM Two_Factor_Authenication_Confirmation WHERE patron_id = session_id);
        BEGIN
            /* If no TOTP was made, then return false */
            IF secret IS NULL THEN RETURN FALSE; END IF;

            /* Check if true */
            IF code = get_TOTP(secret) THEN

                /* Remove from confirmation list as the patron is confirmed to have 2fa set up */
                DELETE FROM Two_Factor_Authenication_Confirmation WHERE patron_id = session_id;

                /* Add secret to Patron's secret field */
                UPDATE Patron SET totp_secret = secret WHERE id = session_id;

                RETURN TRUE;

            ELSE RETURN FALSE; END IF;
        END;
    END IF;

    RAISE EXCEPTION '{"code":"ERR_TWO_FACTOR_AUTH_NOT_EXIST", "message":"Confirmation either expired or never existed!"}';

END; $$
LANGUAGE PLPGSQL SECURITY DEFINER;
